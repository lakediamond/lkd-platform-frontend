# LKD platform frontend

The front-end for the LKD platform, written in react.

## Running

### via docker-compose
* To build & run `var1=a var2=b docker-compose up -d --build --force-recreate`
* To tear down `docker-compose down -v`

## via npm
* `npm install`
* Modify src/actions/OrdersTypeTable.js to include the correct `BASE_URL` for the URL of
  the API, e.g. 'http://lakediamond-site.com/api'.
* `npm start`