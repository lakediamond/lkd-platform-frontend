FROM node:10-alpine as builder

ARG REACT_APP_API_URL
ARG REACT_APP_API_URL_SOCKET
ARG REACT_APP_TOKEN_CONTRACT
ARG REACT_APP_IO_CONTRACT
ARG REACT_APP_IO_STORAGE_CONTRACT
ARG REACT_APP_BLOCKCHAIN_EXPLORER_URL

ENV NODE_ENV production
ENV NODE_PATH src/

ENV REACT_APP_API_URL=$REACT_APP_API_URL
ENV REACT_APP_API_URL_SOCKET=$REACT_APP_API_URL_SOCKET
ENV REACT_APP_TOKEN_CONTRACT=$REACT_APP_TOKEN_CONTRACT
ENV REACT_APP_IO_CONTRACT=$REACT_APP_IO_CONTRACT
ENV REACT_APP_IO_STORAGE_CONTRACT=$REACT_APP_IO_STORAGE_CONTRACT
ENV REACT_APP_BLOCKCHAIN_EXPLORER_URL=$REACT_APP_BLOCKCHAIN_EXPLORER_URL

RUN apk --no-cache add build-base python
RUN npm config set unsafe-perm true
RUN npm install npm@6 --global --quiet
RUN npm set unsafe-perm true
RUN mkdir -p /opt/target
WORKDIR /opt
COPY . .
RUN npm ci
RUN mv nginx.conf /opt/target/
RUN npm run build
RUN mv build /opt/target/

FROM nginx:stable-alpine
EXPOSE 3000
COPY --from=builder /opt/target/nginx.conf /etc/nginx/nginx.conf
COPY --from=builder /opt/target/build /usr/share/nginx/html
RUN chown nginx.nginx /usr/share/nginx/html/ -R
CMD nginx -g 'daemon off;'
