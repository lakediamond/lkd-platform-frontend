export const disclaimer = "LakeDiamond SA is a company limited by shares having its\n" +
  "                registered office at Rue Galilée 7, 1400 Yverdon-les-Bains,\n" +
  "                Vaud, Switzerland, and registered as such with the Swiss\n" +
  "                Commercial Registry under reference CHE-141.951.583. LakeDiamond\n" +
  "                SA is a commercial and an operational entity whose sole activity\n" +
  "                is manufacturing, transforming, selling, marketing and\n" +
  "                distributing lab grown diamonds. In its Swiss-based labs,\n" +
  "                LakeDiamond SA grows diamonds plates and transforms them for\n" +
  "                high-tech applications thanks to its own developed proprietary\n" +
  "                technology. LakeDiamond SA especially sold diamond-production\n" +
  "                hours (Diamond Grown Production Hours) to customers, thus\n" +
  "                allowing customers to use labs and to grow diamond plates, and\n" +
  "                to transform such diamond plates into round brilliant\n" +
  "                respectively according to the customers’ needs. LakeDiamond SA\n" +
  "                contemplates making available Diamond Grown Production Minutes\n" +
  "                into tokens (LKD Tokens). Time-based machine token initiative\n" +
  "                based on LKD Token is likely to use the benefits of smart\n" +
  "                contracts to tokenize Time-based machine use in terms of\n" +
  "                minutes, by connecting LKD Tokens owners with LakeDiamond SA\n" +
  "                diamond production labs on Ethereum protocol basis. LKD Tokens\n" +
  "                are intended to provide a right of use to grow diamonds during a\n" +
  "                certain number of minutes on LakeDiamond SA labs. 1 LKD Token =\n" +
  "                1 Production Minute. LKD Tokens owners are likely to be given\n" +
  "                priority towards other order made to LakeDiamond SA by other\n" +
  "                customers, which would not own LKD Token. The smart contract is\n" +
  "                likely to guarantee the LKD Tokens owner priority. LakeDiamond\n" +
  "                SA, at its sole discretion, may decide to issue LKD Tokens in\n" +
  "                the context of an ICO. Actual owners of Diamond Grown Production\n" +
  "                Hours do not have any enforceable right against LakeDiamond SA\n" +
  "                to receive, acquire or convert Diamond Grown Production Hours\n" +
  "                into LKD Tokens. The launch of the ICO is made at the sole\n" +
  "                discretion of LakeDiamond. Potential purchasers of LKD Tokens\n" +
  "                (\"Potential Purchasers\") are aware, understand and accept that\n" +
  "                growing diamond plates, and transforming such diamond plates\n" +
  "                into round brilliant respectively is at an early stage and is a\n" +
  "                long process and that Potential Purchasers' needs and\n" +
  "                expectations with respect to delivery timing may not be\n" +
  "                guaranteed and may be subject to delay, in particular but not\n" +
  "                limited to high demand and/or outage and/or labs sizing and/or\n" +
  "                other production issues. LakeDiamond SA shall not be held liable\n" +
  "                for any delay and/or postponement and/or impossibility to use\n" +
  "                LKD Tokens. LKD Tokens shall give to Potential Purchasers owning\n" +
  "                LKD Tokens no other right than a right of use labs and to grow\n" +
  "                diamond plates, and to transform such diamond plates into round\n" +
  "                brilliant respectively according to Potential Purchasers’ needs.\n" +
  "                LKD Tokens are intended to be payment tokens that Potential\n" +
  "                Purchasers would use in order to make use of the Diamond Grown\n" +
  "                Production Minutes only. LKD Tokens are not intended to give\n" +
  "                Potential Purchasers any other right like, for instance, equity,\n" +
  "                assets, bond, security, interest, yield, debt, right to\n" +
  "                repayment, or intellectual property right. LKD TOKENS WILL NOT\n" +
  "                QUALIFY AS PURE CRYPTOCURRENCIES LIKE FOR INSTANCE BITCOIN OR\n" +
  "                ETHEREUM AND MAY NOT BE USED AS SUCH IN ANY WAY. THEY WILL ALSO\n" +
  "                NOT AND SHALL NOT BE USED IN ANY WAY AS EQUITY, ASSETS, BONDS,\n" +
  "                SECURITIES, DERIVATIVES OR ANY OTHER FINANCIAL INSTRUMENTS.\n" +
  "                LakeDiamond SA is neither a bank, a private bank, an exchange, a\n" +
  "                securities dealer, a fund, a collective scheme investment\n" +
  "                manager or distributor, a financial intermediary, an asset\n" +
  "                manager or otherwise a financial institution and is neither\n" +
  "                authorized to act as such nor monitored by any financial market\n" +
  "                supervisory authority, including the Swiss Financial Market\n" +
  "                Supervisory Authority FINMA. Neither this document nor any other\n" +
  "                information material relating to LakeDiamond SA and/or the LKD\n" +
  "                Tokens, including but not limited to the Terms and Conditions of\n" +
  "                the LKD Tokens, have been or will be filed with or approved by\n" +
  "                any Swiss regulatory authority. In particular, this document\n" +
  "                will not be filed with the Swiss Financial Market Supervisory\n" +
  "                Authority FINMA (FINMA). The LKD Tokens and the ICO are not and\n" +
  "                will not be subject to any supervision and/or authorization by\n" +
  "                the FINMA and Potential Purchasers will not benefit from\n" +
  "                protection or supervision by such authority. Any ICO involves\n" +
  "                risks, which would be described in the documentation prepared in\n" +
  "                due course, in particular but not limited to the Terms and\n" +
  "                Conditions of the LKD Tokens. Furthermore, each Customer is\n" +
  "                aware, understands and accept the inherent risks associated with\n" +
  "                the Blockchain technology and cryptocurrencies in general and\n" +
  "                the LKD Tokens in particular, including, but not limited to,\n" +
  "                those listed hereinafter. More comprehensive risk factors\n" +
  "                describing risks associated with LKD Tokens will be set out in\n" +
  "                the documentation prepared in due course related to the LKD\n" +
  "                Tokens, in particular but not limited to the Terms and\n" +
  "                Conditions of the LKD Tokens. Potential Purchasers are aware,\n" +
  "                understand and accept that any smart contract, and/or any\n" +
  "                underlying software application and/or any blockchain are part\n" +
  "                of a new technology that is still in an early stage. Potential\n" +
  "                Purchasers are aware, understand and accept that the blockchain\n" +
  "                may be interrupted or may contain errors. Potential Purchasers\n" +
  "                are aware, understand and accept that there is an inherent risk\n" +
  "                that the smart contract, and/or the software and/or the\n" +
  "                blockchain could contain weaknesses, vulnerabilities, virus or\n" +
  "                bugs causing, inter alia, the complete loss of ETH, other\n" +
  "                (financial) means and/or support and/or LKD Tokens. Potential\n" +
  "                Purchasers are aware, understand and accept that any smart\n" +
  "                contract and/or underlying protocols and/or any other software,\n" +
  "                and/or any blockchain may either delay and/or not execute and/or\n" +
  "                not execute properly an order due to various factors, including,\n" +
  "                but not limited to the overall traffic volume, mining attacks,\n" +
  "                virus and/or similar events. LakeDiamond SA shall not be held\n" +
  "                liable for any suspension, theft, fraud, loss of LKD Tokens.\n" +
  "                LakeDiamond SA shall not be liable for the technical risks\n" +
  "                related to, among others, power outage, disconnection, time-out\n" +
  "                or system failure, delays, transmission errors, disturbance or\n" +
  "                the overloading or locking-up of the systems or networks\n" +
  "                involved therewith. If LakeDiamond SA detects any security\n" +
  "                risks, it reserves the right to interrupt the blockchain for the\n" +
  "                protection of the Potential Purchasers at any time until the\n" +
  "                risk is removed. LakeDiamond SA shall not be liable for any\n" +
  "                damages incurred as a result of such interruption. LakeDiamond\n" +
  "                cannot guarantee the availability of the internet. The\n" +
  "                information thereof are exclusively made available to interested\n" +
  "                persons visiting the website upon their own request. This\n" +
  "                website does not purport to contain all information relating to\n" +
  "                LKD Tokens. Information on this website are based on facts\n" +
  "                available on the date hereof. No representation or warranty,\n" +
  "                express or implied, is made as to the fairness, accuracy,\n" +
  "                completeness or correctness of the information or opinions\n" +
  "                contained herein by LakeDiamond SA and its advisors. The\n" +
  "                information set out herein may be subject to updating,\n" +
  "                completion, revision, verification and amendment, without\n" +
  "                notice, all of which may impact this presentation. It should be\n" +
  "                understood that subsequent developments may affect the\n" +
  "                information contained in this document, which neither\n" +
  "                LakeDiamond SA nor any of their advisors are under an obligation\n" +
  "                to update, revise or affirm. This website contains general\n" +
  "                information on LakeDiamond SA and is intended for general\n" +
  "                informational purposes only, upon request of interested persons.\n" +
  "                With respect to the sale of any LKD Tokens, Potential Purchasers\n" +
  "                must rely on the relevant offering documentation to be prepared\n" +
  "                in this respect in due course, in particular but not limited to\n" +
  "                the Terms and Conditions. In particular, the information set out\n" +
  "                is of a preliminary nature. LakeDiamond SA does not warrant,\n" +
  "                guarantee or otherwise assume any and does not assume any\n" +
  "                liability with respect with such information. This website\n" +
  "                contains information concerning the actual business and expected\n" +
  "                future business of LakeDiamond SA, potential growth prospects\n" +
  "                and other future events or developments of the existing\n" +
  "                industrial equipment. These forward-looking information are\n" +
  "                based on the current expectations, estimates and projections of\n" +
  "                LakeDiamond SA management. They are subject to assumptions and\n" +
  "                involve known and unknown risks, uncertainties and other factors\n" +
  "                that may cause actual results and developments to differ\n" +
  "                materially from any future results and developments expressed or\n" +
  "                implied by such forward-looking statements. Therefore,\n" +
  "                LakeDiamond SA cannot provide any assurance with respect to the\n" +
  "                correctness of such forward-looking statements and their effects\n" +
  "                on the situation of LakeDiamond SA or the LKD Tokens issued.\n" +
  "                LakeDiamond SA has no obligation to update or release any\n" +
  "                revisions to the forward-looking statements contained in this\n" +
  "                website to reflect events or circumstances after the date of\n" +
  "                this website. the content of this website does not constitute an\n" +
  "                investment advice or a recommendation or invitation for\n" +
  "                purchasing, holding or selling any LKD TOKENS. In particular, it\n" +
  "                are not deemed to provide (and must not be held as such) any\n" +
  "                advice relating to any decision whether or not to purchase LKD\n" +
  "                TOKENS and must not be considered as providing complete\n" +
  "                information in relation to such a decision. IF YOU ARE IN ANY\n" +
  "                DOUBT AS TO THE ACTION YOU SHOULD TAKE, YOU SHOULD CONSULT YOUR\n" +
  "                LEGAL, FINANCIAL, TAX OR OTHER PROFESSIONAL ADVISOR(S). the\n" +
  "                content of this website DOES NOT CONSTITUTE AN OFFER OR\n" +
  "                INVITATION TO SUBSCRIBE FOR OR PURCHASE ANY LKD TOKENS. the\n" +
  "                content of this website IS NOT INTENDED TO BE READ IN THE\n" +
  "                \"PROHIBITED COUNTRIES\" (AS LISTED IN SCHEDULE 1 AS AMENDED FROM\n" +
  "                TIME TO TIME). IF YOU ARE LOCATED IN THE PROHIBITED COUNTRIES,\n" +
  "                PLEASE LEAVE THIS WEBSITE IMMEDIATELY. the content of this\n" +
  "                website IS NOT BEING ISSUED IN THE UNITED STATES OF AMERICA AND\n" +
  "                SHOULD NOT BE DISTRIBUTED TO U.S. PERSONS OR PUBLICATIONS WITH A\n" +
  "                GENERAL CIRCULATION IN THE UNITED STATES. THIS DOCUMENT DOES NOT\n" +
  "                CONSTITUTE A PROSPECTUS ACCORDING TO ART. 652a AND 1156 OF THE\n" +
  "                SWISS CODE OF OBLIGATIONS OR A PROPSECTUS WITHIN THE MEANING OF\n" +
  "                DIRECTIVE 2003/71/EC, AS AMENDED FROM TIME TO TIME, INCLUDING BY\n" +
  "                DIRECTIVE 2010/73/EU, \"THE PROSPECTUS DIRECTIVE\"). Potential\n" +
  "                Purchasers are aware and understand that certain jurisdictions\n" +
  "                restrict or may restrict in future their residents or citizens\n" +
  "                from participating in any ICO, token sales, use of\n" +
  "                cryptocurrencies, or use of any cryptocurrency exchanges for any\n" +
  "                reason. LakeDiamond SA does not bear any liability for any\n" +
  "                possible current or future impossibility to convert, hold, use\n" +
  "                or otherwise keep LKD Tokens because of the aforementioned or\n" +
  "                any other possible restrictions. LKD Tokens may not be\n" +
  "                converted, held, used or otherwise kept if legal restrictions\n" +
  "                apply, in particular but non-exclusively, to residents or\n" +
  "                citizens from Prohibited Countries. It is the responsibility of\n" +
  "                Potential Purchasers to seek legal advice in their jurisdiction\n" +
  "                to identify any such legal restrictions. LakeDiamond SA shall\n" +
  "                have the right at any anytime, at its sole discretion and by any\n" +
  "                means, to exclude, ban or otherwise restrict the participation\n" +
  "                in the Token Sale or otherwise restrict the Potential\n" +
  "                Purchasers’ possibility of converting, holding, using or in any\n" +
  "                other way keeping LKD Tokens if the respective Customer does not\n" +
  "                meet eligibility requirements set forth by LakeDiamond SA for\n" +
  "                the purpose of the LKD Tokens Sale or on other grounds. All\n" +
  "                information contained in this website are made available upon\n" +
  "                request of interested persons with the understanding that the\n" +
  "                authors, publishers, distributors and advisors are not rendering\n" +
  "                legal, accounting, tax or other professional advice or opinions\n" +
  "                on specific facts or matters and accordingly assume no liability\n" +
  "                whatsoever in connection with its use. In no event shall\n" +
  "                LakeDiamond SA and/or its advisors be liable for any direct,\n" +
  "                indirect, special, incidental or consequential damages arising\n" +
  "                out of the use of any opinion or information expressly or\n" +
  "                implicitly contained in this publication. SCHEDULE 1 The United\n" +
  "                States (including its territories and dependencies, any state of\n" +
  "                the United States and the District of Columbia), Afghanistan,\n" +
  "                Angola, Anguilla (Isle), Antigua and Barbuda (Isle), Aruba,\n" +
  "                Australia, Bangladesh, Belarus, Benin, Bhutan, Bolivia,\n" +
  "                Botswana, Brunei Darussalam, British Indian Ocean Territory,\n" +
  "                Burundi, Burkina Faso, Bosnia, Burundi, Cambodia, Cameroon,\n" +
  "                Canada, Cape Verde, Central Africa republic, Chad, People's\n" +
  "                Republic of China, Comoros, Congo, Congo Democratic republic,\n" +
  "                Cuba, Djibouti, Dominica, Ecuador, El Salvador, Equatorial\n" +
  "                Guinea, Eritrea, Ethiopia, Fiji (Isle), French Guiana (Isle),\n" +
  "                French Polynesia (Isle), Gabon, Gambia, Ghana, Guam (Isle),\n" +
  "                Guatemala, Guyana, Guinea, Guinea Bissau, Haiti, Honduras, Iran,\n" +
  "                Iraq, Ivory Coast, Jordan, Kenya, Kiribati, Kosovo, Kyrgyz\n" +
  "                Republic, Laos People’s Republic, Lebanon, Lesotho, Liberia,\n" +
  "                Libya, Madagascar, Malawi, Mali, Maldives, Mauritania, Mayotte\n" +
  "                (Isle), Micronesia, Moldova, Mongolia, Montenegro, Montserrat,\n" +
  "                Mozambique, Myanmar, Namibia, Nauru, Nepal, New Caledonia,\n" +
  "                Nicaragua, Niger, Nigeria, Niue, Northern Mariana Islands, North\n" +
  "                Korea, Oman, Pakistan, Palau (Isle), Palestinian Areas, Papua\n" +
  "                New Guinea, Paraguay, Reunion, Rwanda, Samoa, Sao Tome and\n" +
  "                Principe, Senegal, Sierra Leone, Somalia, South Georgia, Serbia,\n" +
  "                South Korea, Sudan, South Sudan, Sri Lanka, Suriname, Syria,\n" +
  "                Swaziland, Tajikistan, Tanzania, Timor, Togo, Tonga, Trinidad\n" +
  "                and Tobago, Tunisia, Turkmenistan, Tuvalu (Isle), Uganda,\n" +
  "                Ukraine, Uzbekistan, Vanuatu, Venezuela, Western Sahara, Yemen,\n" +
  "                Zambia, Zimbabwe, all countries and territories listed by the\n" +
  "                Financial Action Task Force as a \"non-cooperative country or\n" +
  "                territory\" and all countries, persons or entities listed on the\n" +
  "                State Secretariat for Economic Affairs' website (SECO), as\n" +
  "                amended from time to time (sanktionsmassnahmen)."