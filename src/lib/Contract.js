import io_contract_abi from "./io_contract_abi";
import token_abi from "./token_abi";

import {
  TOKEN_CONTRACT,
  STORAGE_CONTRACT,
  IO_CONTRACT,
  TOKEN_CONTRACT_ADDRESS,
  IO_CONTRACT_ADDRESS
} from "redux/constants";

class Contract {
  constructor(account, contract) {
    /*global web3 Web3:true*/

    const web3Instance = new Web3(web3.currentProvider);

    web3Instance.eth.defaultAccount = account;

    this.myAddress = account;

    this.createContractApi = contract => {
      switch (contract) {
        case TOKEN_CONTRACT:
          return web3Instance.eth.contract(token_abi);
        case IO_CONTRACT:
          return web3Instance.eth.contract(io_contract_abi);
        default:
          return false;
      }
    };

    const contractApi = this.createContractApi(contract);

    this.createContractInstance = contract => {
      switch (contract) {
        case TOKEN_CONTRACT:
          return contractApi.at(TOKEN_CONTRACT_ADDRESS);
        case IO_CONTRACT:
          return contractApi.at(IO_CONTRACT_ADDRESS);
        default:
          return false;
      }
    };

    this.contractInstance = this.createContractInstance(contract);
    return this.contractInstance;
  }
}

export default Contract;
