import React from "react";
import { connect } from "react-redux";
import { SHA3 } from 'sha3';
import {
  Card,
  Button,
  Input,
  Row,
  Col,
  message,
  Form,
  Spin,
  Popover,
  Icon,
  Skeleton
} from "antd";
import { checkChallenge, restorePassword } from "redux/actions";
import { Title, PasswordHint, Spinner, FinishMessage } from "components";
import { startAuth } from "services";

const FormItem = Form.Item;

class Recovery extends React.Component {
  state = {
    token: null
  };
  
  handleSubmit = e => {
    const { restorePassword } = this.props;
    
    e.preventDefault();
    
    this.props.form.validateFields(async (err, values) => {
      if (!err) {
        const hashNewPassword = new SHA3(256);
        
        const formData = {
          token: this.state.token,
          password: hashNewPassword.update(values["password"]).digest('hex')
        };
        
        restorePassword(formData);
      }
    });
  };
  
  componentDidMount() {
    const {
      history: {
        location: { search }
      },
      checkChallenge
    } = this.props;
    
    search
      ? checkChallenge({ token: search.split("=")[1] })
      : checkChallenge({ token: "" });
  }
  
  componentDidUpdate(prevProps, prevState) {
    const {
      auth: { challenge_is_valid },
      history: {
        location: { search }
      }
    } = this.props;
    
    if (prevProps.auth.challenge_is_valid === null && challenge_is_valid) {
      this.setState({ token: search.split("=")[1] });
    }
  }
  
  compareToFirstPassword = (rule, value, callback) => {
    const form = this.props.form;
    if (value && value !== form.getFieldValue("password")) {
      callback("Two passwords that you enter is inconsistent!");
    } else {
      callback();
    }
  };
  
  validateToNextPassword = (rule, value, callback) => {
    const form = this.props.form;
    if (value && this.state.confirmDirty) {
      form.validateFields(["confirm"], { force: true });
    }
    callback();
  };
  
  renderForm = () => {
    const {
      form: { getFieldDecorator },
      fetch: { state }
    } = this.props;
    
    return (
      <Form onSubmit={e => this.handleSubmit(e)}>
        <FormItem label={"New password"} >
          {getFieldDecorator("password", {
            validateTrigger: "onBlur",
            rules: [
              {
                required: true,
                message: "Please input your password!"
                // pattern: new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[ -/:-@\\[-`{-~]).{10}$")
              },
              {
                validator: this.validateToNextPassword
              }
            ]
          })(
            <Popover content={<PasswordHint />}>
              <Input type="password" />
            </Popover>
          )}
        </FormItem>
        
        <FormItem className="card_row" label={"Confirm password"}>
          {getFieldDecorator("confirm", {
            validateTrigger: "onBlur",
            rules: [
              {
                required: true,
                message: "Please confirm your password!"
                // pattern: new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[ -/:-@\\[-`{-~]).{10}$")
              },
              {
                validator: this.compareToFirstPassword
              }
            ]
          })(<Input type="password" />)}
        </FormItem>
        <Button type="primary" htmlType="submit" block>
          {!state ? (
            "Create new password"
          ) : (
            <Icon type="sync" spin style={{ color: "fff" }} />
          )}
        </Button>
      </Form>
    );
  };
  
  render() {
    const {
      history: {
        location: { search }
      },
      auth: { challenge_is_valid, password_changed }
    } = this.props;
    
    if (!!!search || challenge_is_valid === null) {
      return (
        <Spinner />
      );
    }
    
    const renderSwitchPasswordStatus = password_changed => {
      switch (password_changed) {
        case null:
          return <Card>{this.renderForm()}</Card>;
        case false:
          return <FinishMessage success={false} message={"update your password"} method={startAuth}/>
        case true:
          return <FinishMessage success={true} message={"update your password"} method={startAuth}/>
      }
    };
    
    return (
      <div className={"auth_view"}>
        <Row className="card_row">
          <Col span={8} offset={8}>
            <Title />
            {renderSwitchPasswordStatus(password_changed)}
          </Col>
        </Row>
      </div>
    );
  }
}
const mapStateToProps = (state, ownProps) => {
  return {
    auth: state.auth,
    fetch: state.fetch
  };
};

const mapDispatchToProps = dispatch => {
  return {
    checkChallenge: value => {
      dispatch(checkChallenge(value));
    },
    restorePassword: value => {
      dispatch(restorePassword(value));
    }
  };
};

const RecoveryComponent = Form.create()(Recovery);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RecoveryComponent);
