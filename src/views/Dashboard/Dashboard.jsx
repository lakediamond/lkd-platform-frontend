import React from "react";
import { connect } from "react-redux";
import { Row, Col, message } from "antd";
import {
  StatsCard,
  ChartCard,
  OrdersTable,
  CreateProposalDataWidget,
  TablesContainer,
  Chart
} from "components";

import { get, showAlert, webSocketData } from "redux/actions";

import { ORDERS_SOCKET, GET_CHART_DATA, GET_TIERS_INFO } from "redux/constants";

class Dashboard extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {};
  }

  fetchProposalTables = () => {
    const {
      user: { eth_address }
    } = this.props;

    this.props.get(
      "GET_PROPOSALS_USER",
      null,
      `page=1&limit=5&owner_address=${eth_address}&status=active`
    );
    this.props.get(
      "GET_PROPOSALS_ALL",
      null,
      "page=1&limit=5&status=completed"
    );
    // this.props.webSocketData(ORDERS_SOCKET, "start");
    // this.timeouts.push(setTimeout(this.fetchOrders, 1000));
    // this.props.get("GET_PROPOSALS_USER");
    // this.props.webSocketData(ORDERS_SOCKET, "start");

    // this.timeouts.push(setTimeout(this.fetchTables, 3000));
  };

  fetchOrdersTable = () => {
    this.props.get("GET_ORDERS", null, "page=1&limit=5");
  };

  componentWillMount() {
    this.timeouts = [];
  }

  componentWillUnmount() {
    // this.props.webSocketData(ORDERS_SOCKET, "finish");
  }

  componentDidMount() {
    this.fetchProposalTables();
    this.fetchOrdersTable();
    this.props.get(GET_CHART_DATA, null, 90);
    this.props.get(GET_TIERS_INFO);
  }

  componentDidUpdate(prevProps, prevState) {
    const {
      proposalsList: { error }
    } = this.props;

    if (error) {
      message.error(error);
    }
  }

  render() {
    const {
      ordersList: {
        orders: { records }
      },
      proposalsList,
      user,
      blockchain: { ioContractInstance },
      chartData: { rows }
    } = this.props;

    return (
      <React.Fragment>
        <Row className="container_row" gutter={12}>
          {!!rows && rows.length > 0 && (
              <React.Fragment>
                <div className={"tables-container__title"}>
                  ACTIVE PROPOSALS
                </div>
                <Col span={16}>
                  <Chart />
                </Col>
              </React.Fragment>
            )}
          <Col span={4}>
            <CreateProposalDataWidget contractInstance={ioContractInstance} />
          </Col>
        </Row>
        <Row className={"container_row"}>
          <Col span={24}>
            <TablesContainer
              title={"PROPOSALS LIST"}
              type={"short"}
              table={"Proposals"}
              action={this.fetchProposalTables}
            />
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <OrdersTable
              collection={records}
              title={"LAST COMPLETED ORDERS"}
              action={this.fetchOrdersTable}
              type={"short"}
            />
          </Col>
        </Row>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    ordersList: state.ordersList,
    proposalsList: state.proposalsList,
    blockchain: state.blockchain,
    alertMessage: state.alertMessage,
    user: state.user,
    chartData: state.chartData
  };
};

const mapDispatchToProps = dispatch => {
  return {
    get: (type, body, param) => {
      dispatch(get(type, body, param));
    },
    // webSocketData: (type, action) => {
    //   dispatch(webSocketData(type, action));
    // },
    showAlert: message => {
      dispatch(showAlert(message));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Dashboard);
