import React from "react";
import { connect } from "react-redux";
import { Row, Col, message, Card, Skeleton } from "antd";
import {
  ProposalsTable,
  CreateProposalDataWidget,
  MetamaskNotExistCard,
  Spinner,
  TablesContainer
} from "components";

import {
  checkAllowance,
  get,
  isMetamaskInstalled,
  webSocketData
} from "redux/actions";

import {
  PROPOSALS_USER_SOCKET,
  PROPOSALS_ALL_SOCKET,
  GET_PROPOSALS_ALL,
  GET_PROPOSALS_USER,
  GET_PROPOSALS_OWNERS
} from "redux/constants";

class Proposals extends React.Component {
  state = {
    allowanceRequest: 0
  };

  componentWillMount() {
    // this.timeouts = [];
  }

  componentDidMount() {
    const {
      user: { kyc_summary, eth_address },
      history,
      blockchain: { metaMaskAccount, isInstalled },
      isMetamaskInstalled,
      webSocketData,
      get
    } = this.props;

    // if (!!!kyc_summary) {
    //   history.push("/profile/overview");
    // }

    if (!!metaMaskAccount) {
      // get("GET_PROPOSALS_ALL");
      // get("GET_PROPOSALS_USER");
      // webSocketData(PROPOSALS_ALL_SOCKET, "start");
      // webSocketData(PROPOSALS_USER_SOCKET, "start");
    }

    get(GET_PROPOSALS_ALL);
    get(GET_PROPOSALS_USER, null, `owner_address=${eth_address}`);
    get(GET_PROPOSALS_OWNERS);
  }

  componentWillUnmount() {
    // const { webSocketData } = this.props;
    // webSocketData(PROPOSALS_ALL_SOCKET, "finish");
    // webSocketData(PROPOSALS_USER_SOCKET, "finish");
  }

  componentDidUpdate(prevProps, prevState, prevContext) {
    const {
      blockchain: { metaMaskAccount, allowance },
      checkAllowance,
      proposalsList: { error },
      webSocketData,
      get
    } = this.props;

    const { allowanceRequest } = this.state;

    if (
      prevProps.blockchain.metaMaskAccount !== metaMaskAccount &&
      !!metaMaskAccount
    ) {
      // get("GET_PROPOSALS_ALL", null, null);
      // get("GET_PROPOSALS_USER", null, null);
      // this.fetchProposals();
      // webSocketData(PROPOSALS_ALL_SOCKET, "start");
      // webSocketData(PROPOSALS_USER_SOCKET, "start");
    }

    // if (
    //   !!metaMaskAccount &&
    //   prevProps.blockchain.allowance === null &&
    //   allowance === null &&
    //   allowanceRequest === 0
    // ) {
    //   this.setState({ allowanceRequest: 1 });
    //   checkAllowance(metaMaskAccount);
    // }

    //TODO CHECK THIS
    if (error) {
      message.error(error);
    }
  }

  fetchProposals = (type, param = "") => {
    // this.props.fetchOrdersList();
    this.props.get(type, null, param);
    // this.props.get("GET_PROPOSALS_ALL", null, "page=1&limit=10");

    // this.props.get(type, null, param);

    // this.props.get("GET_PROPOSALS_USER", null, param);
    // this.props.webSocketData(ORDERS_SOCKET, "start");
    // this.timeouts.push(setTimeout(this.fetchOrders, 1000));
  };

  render() {
    return <React.Fragment>{this.renderContent()}</React.Fragment>;
  }

  renderContent = () => {
    const {
      blockchain: { ioContractInstance, isLoggedIn },
      proposalsList: {owners}
    } = this.props;

    return (
      <React.Fragment>
        <Row>
          <Col span={24}>
            {/*<ProposalsTable*/}
            {/*collection={proposalsList["all"]}*/}
            {/*title={"All Proposals"}*/}
            {/*/>*/}
            <TablesContainer
              type={"full"}
              table={"Proposals"}
              title={"PROPOSALS LIST"}
              owners={owners}
            />
          </Col>
        </Row>
      </React.Fragment>
    );
  };
}

const mapStateToProps = (state, ownProps) => {
  return {
    proposalsList: state.proposalsList,
    blockchain: state.blockchain,
    user: state.user
  };
};

const mapDispatchToProps = dispatch => {
  return {
    isMetamaskInstalled: () => {
      dispatch(isMetamaskInstalled());
    },
    get: (type, body, param) => {
      dispatch(get(type, body, param));
    },
    checkAllowance: account => {
      dispatch(checkAllowance(account));
    }
    // webSocketData: (type, action) => {
    //   dispatch(webSocketData(type, action));
    // }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Proposals);
