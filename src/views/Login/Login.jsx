import React from "react";
import { connect } from "react-redux";
import { NavLink } from "react-router-dom";
import { Card, Button, Input, Row, Col, message, Form, Icon } from "antd";
import { SHA3 } from 'sha3';
import { Request, startAuth } from "services";
import { GET_OAUTH_LOGIN_REQUEST, POST_SIGN_IN } from "redux/constants";
import { Title, Spinner } from "components";
import { sendEmailForRestore } from "redux/actions";

const FormItem = Form.Item;

class Login extends React.Component {
  state = {
    login_challenge: "",
    request: false,
    show2FACode: false,
    forgotPassword: false,
    showForm: null
  };

  handleSubmitLoginForm = e => {
    e.preventDefault();
    this.setState({ request: true });
    this.props.form.validateFields(async (err, values) => {
      if (!err) {
        const hash = new SHA3(256);
        
        const formData = {
          ...values,
          login_challenge: this.state.login_challenge,
          password: hash.update(values["password"]).digest('hex').toString()
        };

        const signInUser = await Request(POST_SIGN_IN, formData);

        if (!signInUser) {
          // message.error("Looks like it's a problem with network");
          this.setState({ request: false });
          return false;
        }

        if (signInUser.status === 403) {
          this.setState({ request: false });
          message.error("Your email or password isn't correct");
          return false;
        }

        if (signInUser.status === 401) {
          signInUser
            .text()
            .then(text => (text.length ? JSON.parse(text) : false))
            .then(text =>
              text ? message.error(text.error_message.join()) : null
            );
          this.setState({ show2FACode: true, request: false });
          return false;
        }

        const headers = signInUser.headers;
        const location = headers.get("location");
        window.location.href = location;
      }
      this.setState({ request: false });
    });
  };

  handleSubmitForgotPasswordForm = e => {
    e.preventDefault();
    const { sendEmailForRestore } = this.props;
    this.props.form.validateFields(async (err, values) => {
      if (!err) {
        const formData = { email: values["email_to_restore"] };
        sendEmailForRestore(formData);
      }
    });
  };

  CheckOauthLoginRequest = async type => {
    const data = await Request(GET_OAUTH_LOGIN_REQUEST, null, type);

    if (!data) {
      return false;
    }

    if (data.status === 200) {
      const headers = data.headers;
      const location = headers.get("Location");
      window.location.href = location;
    }

    if (data.status === 204) {
      this.setState({ showForm: true });
    }
  };

  componentDidMount() {
    const {
      history: {
        location: { search }
      }
    } = this.props;

    if (!!!search) {
      startAuth();
    }

    if (search.includes("email_verified=true")) {
      message.success(
        "Congratulations! Your email has been verified successfully, you may proceed with sign-in"
      );
    }

    if (search.includes("email_verified=false")) {
      message.info(
        "Apologies, during your email address verification procedure we faced unprecedented case. Please, contact our support at support@lakediamond.ch"
      );
    }

    if (search.includes("login_challenge")) {
      let urlParams = search.slice(1).split("&");
      urlParams.map(keyValue => {
        if (keyValue.split("=")[0] === "login_challenge") {
          this.setState({ login_challenge: keyValue.split("=")[1] });
        }
      });

      this.setState({ challenge_url: this.props.history.location.search });
      const type = `${this.props.history.location.pathname}${
        this.props.history.location.search
      }`;
      this.CheckOauthLoginRequest(type);
    }
  }

  renderLoginForm = () => {
    const {
      form: { getFieldDecorator }
    } = this.props;

    const { show2FACode, request, forgotPassword } = this.state;

    if (forgotPassword) {
      return null;
    }

    return (
      <Form onSubmit={e => this.handleSubmitLoginForm(e)}>
        <FormItem label={"Email"}>
          {getFieldDecorator("email", {
            validateTrigger: "onBlur",
            rules: [
              { type: "email", message: "Please input your email!" },
              { required: true, message: "Please input your email!" }
            ]
          })(<Input disabled={show2FACode} />)}
        </FormItem>
        <FormItem className={!show2FACode ? "card_row" : null} label={"Password"}>
          {getFieldDecorator("password", {
            validateTrigger: "onBlur",
            rules: [
              {
                required: true,
                message: "Please input your password!"
              }
            ]
          })(
            <Input
              type="password"
              disabled={show2FACode}
            />
          )}
        </FormItem>
        {show2FACode ? (
          <FormItem  className="card_row" label={"2FA code"}>
            {getFieldDecorator("google_2_fa_pass", {
              validateTrigger: "onBlur",
              rules: [
                {
                  required: true,
                  message: "Please input your 2FA code!"
                }
              ]
            })(<Input type="password" />)}
          </FormItem>
        ) : null}

        <Button type="primary" block htmlType="submit" disabled={request}>
          {request ? (
            <Icon type="sync" spin style={{ color: "fff" }} />
          ) : (
            "Login"
          )}
        </Button>
      </Form>
    );
  };

  renderForgotPasswordForm = () => {
    const {
      form: { getFieldDecorator },
      fetch: { state }
    } = this.props;
    const { forgotPassword, request } = this.state;

    if (!forgotPassword) {
      return null;
    }

    return (
      <Form onSubmit={e => this.handleSubmitForgotPasswordForm(e)}>
        <FormItem className="card_row" label={"Email"}>
          {getFieldDecorator("email_to_restore", {
            validateTrigger: "onBlur",
            rules: [
              { type: "email", message: "Please input your email!" },
              { required: true, message: "Please input your email!" }
            ]
          })(<Input />)}
        </FormItem>

        <Button type="primary" block htmlType="submit">
          {state ? (
            <Icon type="sync" spin style={{ color: "fff" }} />
          ) : (
            "Send me the password recovery link"
          )}
        </Button>
      </Form>
    );
  };

  handleForgotPassword = e => {
    e.preventDefault();
    this.setState({ forgotPassword: true });
  };

  handleRememberedPassword = e => {
    e.preventDefault();
    this.setState({ forgotPassword: false });
  };

  renderContent = () => {
    const {
      auth: { email_for_restore_send }
    } = this.props;

    return !email_for_restore_send ? (
      <React.Fragment>
        {this.renderLoginForm()}
        {this.renderForgotPasswordForm()}
        {this.renderForgotPasswordButton()}
      </React.Fragment>
    ) : (
      <React.Fragment>
        <div>Please check you email for restore link</div>
      </React.Fragment>
    );
  };

  renderLinkToRegister = () => {
    const {
      auth: { email_for_restore_send }
    } = this.props;

    return !email_for_restore_send ? (
      <Row>
        <Col className="register_message">
          <span>
            Don't have an account?{" "}
            <NavLink to={`/register${this.state.challenge_url}`}>
              Sign up
            </NavLink>
          </span>
        </Col>
      </Row>
    ) : null;
  };

  renderForgotPasswordButton = () => {
    const { forgotPassword } = this.state;

    return (
      <Row className={"forgot_password_button"}>
        {!forgotPassword ? (
          <a onClick={e => this.handleForgotPassword(e)}>Forgot your password?</a>
        ) : (
          <a onClick={e => this.handleRememberedPassword(e)}>
            I remembered a password
          </a>
        )}
      </Row>
    );
  };

  render() {
    const {
      history: {
        location: { search }
      }
    } = this.props;

    const { showForm } = this.state;

    if (!!!search || showForm === null) {
      return <Spinner />;
    }

    if (!!showForm) {
      return (
        <Row className={"auth_view"}>
          <Col span={8} offset={8}>
            <Title />
            <Card>{this.renderContent()}</Card>
            {this.renderLinkToRegister()}
          </Col>
        </Row>
      );
    }
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    auth: state.auth,
    fetch: state.fetch
  };
};

const mapDispatchToProps = dispatch => {
  return {
    sendEmailForRestore: value => {
      dispatch(sendEmailForRestore(value));
    }
  };
};

const loginComponent = Form.create()(Login);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(loginComponent);
