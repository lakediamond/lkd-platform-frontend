import Proposals from "./Proposals/Proposals";
import Consent from "./Consent/Consent";
import Dashboard from "./Dashboard/Dashboard";
import Login from "./Login/Login";
import Management from "./Management/Management";
import Orders from "./Orders/Orders";
import Register from "./Register/Register";
// import Wallet from "./Wallet/Wallet";
import Auth from "./Auth/Auth";
import Final from "./Final/Final";
import Overview from "./Profile/Overview/Overview";
import Kyc from "./Profile/KYC/Kyc";
import TwoFA from "./Profile/TwoFA/TwoFA";
import Password from "./Profile/Password/Password";
import Recovery from "./Recovery/Recovery";

export {
  Proposals,
  Consent,
  Dashboard,
  Login,
  Management,
  Orders,
  Register,
  Auth,
  Final,
  Overview,
  Kyc,
  TwoFA,
  Recovery,
  Password
};