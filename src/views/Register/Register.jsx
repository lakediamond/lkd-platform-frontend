import React from "react";
import { Request, startAuth, checkPasswordOWASP } from "services";
import { SHA3 } from "sha3";

import {
  POST_REGISTRATION,
  ETH_ADDRESS_EXP,
  LETTERS_EXP_WITH_SPECSYMBOLS
} from "redux/constants";

import {
  Card,
  Button,
  Input,
  Row,
  Col,
  message,
  Form,
  DatePicker,
  Icon,
  Popover,
  Select
} from "antd";

import moment from "moment";
import { NavLink } from "react-router-dom";
import { Title, Spinner, PasswordHint } from "components";

const FormItem = Form.Item;
const Option = Select.Option;

class Register extends React.Component {
  state = {
    login_challenge: "",
    userCreated: null,
    request: false
  };

  handleSubmit = e => {
    e.preventDefault();
    this.setState({ request: true });
    this.props.form.validateFields(async (err, values) => {
      if (!err) {
        delete values["confirm"];
        const dateFormat = "YYYY-MM-DD";
        
        values["birth_date"] = moment(values["birth_date"]).format(dateFormat);

        const hash = new SHA3(256);

        const formData = {
          ...values,
          password: hash
            .update(values["password"])
            .digest("hex")
            .toString(),
          login_challenge: this.state.login_challenge,
          investment_plan: 0
        };

        const registerUser = await Request(POST_REGISTRATION, formData);

        if (!registerUser) {
          message.error("Looks like it's problem with network");
          this.setState({ request: false });
          return false;
        }

        if (registerUser.status !== 201) {
          registerUser
            .json()
            .then(data => data)
            .then(res => message.error(res[Object.keys(res)[0]].join(",")));
          this.setState({ userCreated: false });
          this.setState({ request: false });
          return false;
        }

        this.setState({ request: false });
        this.setState({ userCreated: true });
      }
      this.setState({ request: false });
    });
  };

  disabledDate = current => {
    const yearForCheck = moment()
      .subtract(18, "years")
      .year();
    return current && current > moment().year(yearForCheck);
  };

  componentDidMount() {
    const {
      history: {
        location: { search }
      }
    } = this.props;

    if (!!!search) {
      startAuth();
    }

    if (search.includes("login_challenge")) {
      const loginChallenge = search.slice(1).split("=")[1];
      this.setState({ login_challenge: loginChallenge });
    }
  }

  compareToFirstPassword = (rule, value, callback) => {
    const form = this.props.form;
    if (value && value !== form.getFieldValue("password")) {
      callback("Two passwords that you enter is inconsistent!");
    } else {
      callback();
    }
  };

  validateToNextPassword = (rule, value, callback) => {
    const form = this.props.form;
    if (value) {
      if (checkPasswordOWASP(value) !== "valid") {
        callback(checkPasswordOWASP(value));
      }
      form.validateFields(["confirm"], { force: true });
    }
    callback();
  };

  render() {
    const { userCreated } = this.state;
    const { getFieldDecorator } = this.props.form;
    const {
      history: {
        location: { search }
      }
    } = this.props;

    if (!!!search) {
      return <Spinner />;
    }

    const DatePickerconfig = {
      initialValue: moment().subtract(18, "years"),
      rules: [
        {
          type: "object",
          validateTrigger: "onBlur",
          required: true,
          message: "Please select your birthday"
        }
      ]
    };

    const showUI = () => {
      return !!!userCreated ? (
        <React.Fragment>
          <Card>
            <Form onSubmit={e => this.handleSubmit(e)}>
              <FormItem label={"First Name"}>
                {getFieldDecorator("first_name", {
                  validateTrigger: "onBlur",
                  rules: [
                    {
                      required: true,
                      pattern: LETTERS_EXP_WITH_SPECSYMBOLS,
                      message: "Please input your first name!"
                    }
                  ]
                })(<Input/>)}
              </FormItem>
              <FormItem label={"Last Name"}>
                {getFieldDecorator("last_name", {
                  validateTrigger: "onBlur",
                  rules: [
                    {
                      required: true,
                      pattern: LETTERS_EXP_WITH_SPECSYMBOLS,
                      message: "Please input your last name!"
                    }
                  ]
                })(<Input/>)}
              </FormItem>
              <FormItem label={"Email"}>
                {getFieldDecorator("email", {
                  validateTrigger: "onBlur",
                  rules: [
                    { type: "email", message: "Please input your email!" },
                    { required: true, message: "Please input your email!" }
                  ]
                })(<Input />)}
              </FormItem>
              <FormItem label={"Date of birth"}>
                {getFieldDecorator("birth_date", DatePickerconfig)(
                  <DatePicker
                    format={"YYYY-MM-DD"}
                    placeholder={"YYYY-MM-DD"}
                    disabledDate={this.disabledDate}
                  />
                )}
              </FormItem>
              <FormItem label={"Ethereum wallet (only Metamask address is supported!)"}>
                {getFieldDecorator("hash", {
                  validateTrigger: "onBlur",
                  rules: [
                    {
                      required: true,
                      pattern: ETH_ADDRESS_EXP,
                      message: "Please input your ETH account!"
                    }
                  ]
                })(
                  <Input
                    placeholder="0x0000000000000000000000000000000000000000"
                    autoComplete="no-there-is-no-need"
                  />
                )}
              </FormItem>
              <FormItem label={"Password"}>
                {getFieldDecorator("password", {
                  validateTrigger: "onBlur",
                  rules: [
                    {
                      required: true,
                      message: "Please input your password!"
                    },
                    {
                      validator: this.validateToNextPassword
                    }
                  ]
                })(
                  <Popover content={<PasswordHint />}>
                    <Input type="password" />
                  </Popover>
                )}
              </FormItem>
              <FormItem className="card_row" label={"Confirm password"}>
                {getFieldDecorator("confirm", {
                  validateTrigger: "onBlur",
                  rules: [
                    {
                      required: true,
                      message: "Please confirm your password!"
                    },
                    {
                      validator: this.compareToFirstPassword
                    }
                  ]
                })(<Input type="password" />)}
              </FormItem>

              <Button
                type="primary"
                htmlType="submit"
                block
                disabled={this.state.request}
              >
                {this.state.request ? (
                  <Icon type="sync" spin style={{ color: "fff" }} />
                ) : (
                  "Register"
                )}
              </Button>
            </Form>
          </Card>
          <Row>
            <Col span={24} className="register_message">
              <span>
                Already have an account?{" "}
                <NavLink
                  to={`/login?login_challenge=${this.state.login_challenge}`}
                >
                  Login here
                </NavLink>
              </span>
            </Col>
          </Row>
        </React.Fragment>
      ) : (
        <div>
          You have successfully registered on the LKD-Platform.
          Please go to your mailbox to find the confirmation link.
        </div>
      );
    };

    return (
      <div className={"auth_view"}>
        <Row>
          <Col span={8} offset={8}>
            <Title />
            {showUI()}
          </Col>
        </Row>
      </div>
    );
  }
}

export default Form.create()(Register);
