import React from "react";
import { connect } from "react-redux";
import { Row, Col, Alert, message, Skeleton, Card } from "antd";
import {
  SubOwnersTable,
  OrdersTypeTable,
  OrderTypeDataWidget,
  SubownerDataWidget,
  MetamaskNotExistCard,
  SetupBlockchainProvider,
  ChooseBlockchainProvider
} from "components";

import { Request } from "services";
import { GET_ORDER_TYPES, GET_SUB_OWNERS } from "redux/constants";
import { checkContractOwner, get } from "../../redux/actions";

class Management extends React.Component {
  state = {};
  
  addSubOwnersToContract = addresses => {
    const {
      blockchain: { ioContractInstance }
    } = this.props;

    ioContractInstance.addSubOwners(addresses, (err, res) => {
      if (!err) {
        message.success(
          "You successfully added new sub owners to the contract"
        );
        return true;
      }
      message.error("Ooops, something went wrong");
      return false;
    });
  };

  removeSubOwnersFromContract = addresses => {
    const {
      blockchain: { ioContractInstance }
    } = this.props;

    ioContractInstance.removeSubOwners(addresses, (err, res) => {
      if (!err) {
        message.success("You successfully removed sub owners to the contract");
        return true;
      }
      message.error("Ooops, something went wrong");
      return false;
    });
  };

  addOrderTypesToContract = (id, orderType) => {
    const {
      blockchain: { ioContractInstance }
    } = this.props;

    ioContractInstance.setOrderType(Number(id), orderType, (err, res) => {
      if (!err) {
        message.success(
          "You successfully added new order type to the contract"
        );
        return true;
      }
      message.error("Ooops, something went wrong");
      return false;
    });
  };

  componentDidMount() {
    const {
      user: { role },
      history,
      blockchain: {
        metaMaskAccount,
        isInstalled,
        ioContractInstance,
        isContractOwner
      },
      checkContractOwner,
      get
    } = this.props;

    if (!!role && role !== "admin") {
      history.push("/profile/overview");
    }

    if (!!metaMaskAccount) {
      get(GET_SUB_OWNERS);
      get(GET_ORDER_TYPES);
    }

    if (!!ioContractInstance && isContractOwner === null) {
      checkContractOwner(ioContractInstance, metaMaskAccount);
    }
  }

  componentDidUpdate(prevProps, prevState, prevContext) {
    const {
      blockchain: { metaMaskAccount, ioContractInstance, isContractOwner },
      checkContractOwner,
      get
    } = this.props;

    if (
      prevProps.blockchain.metaMaskAccount !== metaMaskAccount &&
      !!metaMaskAccount
    ) {
      get(GET_SUB_OWNERS);
      get(GET_ORDER_TYPES);
    }

    if (
      prevProps.blockchain.ioContractInstance !== ioContractInstance &&
      !!ioContractInstance &&
      isContractOwner === null
    ) {
      checkContractOwner(ioContractInstance, metaMaskAccount);
    }
  }

  render() {
    return <React.Fragment>{this.renderContent()}</React.Fragment>;
  }

  renderAlert = () => {
    return this.state.open_alert ? (
      <Row gutter={16} className="container_row">
        <Col span={12}>
          <Alert
            message={this.state.alert_text}
            type={this.state.alert_type}
            showIcon
            closeText="X"
          />
        </Col>
      </Row>
    ) : null;
  };

  renderContent = () => {
    const {
      blockchain: {
        storageContractInstance,
        isLoggedIn,
        isContractOwner,
        isInstalled,
        allowance,
        isEnabled
      },
      subOwnersList,
      proposalsList,
      user: { blockchain_connection_type }
    } = this.props;

    const blockChainNotSetuped =
      !isInstalled ||
      !isLoggedIn ||
      !allowance ||
      (!isEnabled && blockchain_connection_type);

    return (
      <div
        className={
          !!!blockchain_connection_type || blockChainNotSetuped
            ? "data-widget_blocked"
            : null
        }
      >
        {blockChainNotSetuped ? <SetupBlockchainProvider type={"full"}/> : null}
        {!!!blockchain_connection_type ? <ChooseBlockchainProvider /> : null}
        {this.renderAlert()}
        <Row
          gutter={16}
          type="flex"
          justify="space-between"
          className="container_row"
        >
          <Col lg={14} xl={12}>
            <SubownerDataWidget
              action={this.addSubOwnersToContract}
              blocked={!!!blockchain_connection_type || blockChainNotSetuped}
            />
          </Col>
          <Col lg={10} xl={12}>
            <OrderTypeDataWidget
              action={this.addOrderTypesToContract}
              blocked={!!!blockchain_connection_type || blockChainNotSetuped}
            />
          </Col>
        </Row>
        <Row gutter={16}>
          <Col lg={24} xl={12} className="container_row">
            <SubOwnersTable
              collection={subOwnersList}
              action={this.removeSubOwnersFromContract}
              isContractOwner={isContractOwner}
              blocked={!!!blockchain_connection_type || blockChainNotSetuped}
            />
          </Col>
          <Col lg={24} xl={12}>
            <OrdersTypeTable
              collection={this.props.ordersList.types}
              action={this.addOrderTypesToContract}
              isContractOwner={isContractOwner}
              blocked={!!!blockchain_connection_type || blockChainNotSetuped}
            />
          </Col>
        </Row>
      </div>
    );
  };
}

const mapStateToProps = (state, ownProps) => {
  return {
    user: state.user,
    blockchain: state.blockchain,
    subOwnersList: state.subOwnersList,
    ordersList: state.ordersList
  };
};

const mapDispatchToProps = dispatch => {
  return {
    checkContractOwner: (contractInstance, account) => {
      dispatch(checkContractOwner(contractInstance, account));
    },
    get: type => {
      dispatch(get(type));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Management);
