import React from 'react';
import renderer from 'react-test-renderer';
import Management from '../Management';

test('Management', () => {
  const component = renderer.create(<Management/>);
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});