import React from "react";
import { connect } from "react-redux";
import { StatsCard, ChartCard, MetamaskNotExistCard } from "components";
import ChartistGraph from "react-chartist";
import { dailySalesChart } from "variables/charts";
import {
  getETHBalance,
  getTokenBalance,
  isMetamaskInstalled,
  enableMetamask
} from "../../redux/actions";
import { Row, Col, Card, Skeleton } from "antd";

class Wallet extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      EthTmtTrade: 0.0027,
      EurTmtTrade: 0.48,
      balance: 0
    };
  }

  // getTMTPriceData = () => {
  //   const currentDate = new Date();
  //
  //   if (!("dayLabels" in this.state)) {
  //     const days = ["M", "T", "W", "T", "F", "S", "S"];
  //     const currentDay = currentDate.getDay();
  //     let labels = [];
  //     for (let i = currentDay; i < currentDay + 7; i++) {
  //       labels.push(days[i % 7]);
  //     }
  //     this.setState({ dayLabels: labels });
  //   }
  //
  //   let weeklyPrices = [1.5, 1.9, 2, 1.7, 2.1, 2.5, 2.98];
  //
  //   let options = dailySalesChart.options;
  //   const minPrice = Math.min.apply(Math, weeklyPrices);
  //   const maxPrice = Math.max.apply(Math, weeklyPrices);
  //   options.low = minPrice - 0.1 * minPrice;
  //   options.high = maxPrice + 0.1 * maxPrice;
  //
  //   return {
  //     data: {
  //       labels: this.state.dayLabels,
  //       series: [weeklyPrices]
  //     },
  //     options: options
  //   };
  // };

  componentDidMount() {
    const {
      isMetamaskInstalled,
      blockchain: { metaMaskAccount, isInstalled },
      getETHBalance,
      getTokenBalance
    } = this.props;
  
    if (isInstalled === null) {
      isMetamaskInstalled();
    }

    if (!!metaMaskAccount) {
      getETHBalance(metaMaskAccount);
      getTokenBalance(metaMaskAccount);
    }
  }

  componentDidUpdate(prevProps, prevState, prevContext) {
    const {
      blockchain: { metaMaskAccount, isInstalled },
      getETHBalance,
      getTokenBalance,
      enableMetamask
    } = this.props;

    // if (
    //   prevProps.metaMask.isInstalled !== isInstalled ||
    //   (isInstalled && !!!metaMaskAccount)
    // ) {
    //   enableMetamask();
    // }

    if (
      prevProps.blockchain.metaMaskAccount !== metaMaskAccount &&
      !!metaMaskAccount
    ) {
      getETHBalance(metaMaskAccount);
      getTokenBalance(metaMaskAccount);
    }
    
  }

  render() {
    const {
      blockchain: { ethBalance, tokenBalance, isInstalled }
    } = this.props;

    switch (isInstalled) {
      case null:
        return <Skeleton active />;
      case true:
        return (
          <React.Fragment>
            <Row className="container_row" gutter={48}>
              <Col span={8}>
                {!!tokenBalance ? (
                  <StatsCard
                    title="Token Balance"
                    description={`${tokenBalance} LKD`}
                  />
                ) : (
                  <Card className={"card_stats_skeleton"}>
                    <Skeleton active />
                  </Card>
                )}
              </Col>
              <Col span={8}>
                {!!ethBalance ? (
                  <StatsCard
                    title="Ethereum Balance"
                    description={`${ethBalance} LKD`}
                  />
                ) : (
                  <Card className={"card_stats_skeleton"}>
                    <Skeleton active />
                  </Card>
                )}
              </Col>
              <Col span={8}>
                {!!tokenBalance ? (
                  <StatsCard
                    iconColor="purple"
                    title="EUR Balance"
                    description={
                      (tokenBalance
                        ? (
                            Number(tokenBalance) * this.state.EurTmtTrade
                          ).toFixed(3)
                        : 0) + " EUR"
                    }
                    statText={"1 LKD = " + this.state.EurTmtTrade + " EUR"}
                  />
                ) : (
                  <Card className={"card_stats_skeleton"}>
                    <Skeleton active />
                  </Card>
                )}
              </Col>
            </Row>
            
          </React.Fragment>
        );
      case false:
        return <MetamaskNotExistCard />;
      default:
        return <Skeleton active />;
    }
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    blockchain: state.blockchain,
    user: state.user
  };
};

const mapDispatchToProps = dispatch => {
  return {
    enableMetamask: () => {
      dispatch(enableMetamask());
    },
    isMetamaskInstalled: () => {
      dispatch(isMetamaskInstalled());
    },
    getETHBalance: account => {
      return dispatch(getETHBalance(account));
    },
    getTokenBalance: account => {
      return dispatch(getTokenBalance(account));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Wallet);

{/*<Row>*/}
  {/*<Col span={24}>*/}
    {/*{!!tokenBalance ? (*/}
      {/*<a*/}
        {/*href="https://coinmarketcap.com"*/}
        {/*target="_blank"*/}
        {/*rel="noopener noreferrer"*/}
      {/*>*/}
        {/*<ChartCard*/}
          {/*chart={*/}
            {/*<ChartistGraph*/}
              {/*className="ct-chart"*/}
              {/*data={this.getTMTPriceData()["data"]}*/}
              {/*type="Line"*/}
              {/*options={this.getTMTPriceData()["options"]}*/}
              {/*listener={dailySalesChart.animation}*/}
            {/*/>*/}
          {/*}*/}
          {/*chartColor="blue"*/}
          {/*title="LKD weekly price (EUR)"*/}
          {/*text=""*/}
          {/*statText=""*/}
        {/*/>*/}
      {/*</a>) : (*/}
      {/*<Card className={"card_stats_skeleton"}>*/}
        {/*<Skeleton active />*/}
      {/*</Card>*/}
    {/*)}*/}
  {/*</Col>*/}
{/*</Row>*/}
