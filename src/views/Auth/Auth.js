import React from "react";
import { message } from "antd";
import { Request } from "services";
import { GET_USER_REQUEST_LOGIN } from "redux/constants";
import { Spinner } from "components";

class Auth extends React.Component {
  checkUserAuthenticateChallenge = async () => {
    const data = await Request(GET_USER_REQUEST_LOGIN);
    if (!data) {
      message.error("Looks like it's a network error");
      return false;
    }
    if (data.status === 200) {
      const header = data.headers;
      const location = header.get("location");
      window.location.href = location;
    } else {
      message.error("Oooops, something went wrong");
    }
  };

  componentDidMount() {
    this.checkUserAuthenticateChallenge();
  }
  
  render() {
    return(<Spinner />)
  }
}

export default Auth;
