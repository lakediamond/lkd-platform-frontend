import React from "react";
import { connect } from "react-redux";
import { Col, Row } from "antd";
import { TierStepDataWidget, TargetTierDataWidget } from "components";
import { get, setInvestmentPlan } from "redux/actions";

class Kyc extends React.Component {
  componentDidMount() {
    const {
      get,
      user: { target_tier },
      setInvestmentPlan,
      location,
      history
    } = this.props;

    if (!!!target_tier) {
      setInvestmentPlan(0);
    }

    if (target_tier) {
      get("GET_TIERS_INFO");
    }

    //TODO improved it
    if (location.pathname == "/profile/kyc") {
      history.push("/profile/kyc/tier/0");
    }
  }

  componentDidUpdate(prevProps, prevState, prevContext) {
    const {
      get,
      user: { target_tier }
    } = this.props;

    if (prevProps.user.target_tier !== target_tier) {
      get("GET_TIERS_INFO");
    }
  }

  navigationOpenedKey = () => {
    const arr = this.props.location.pathname.split("/");
    return arr[arr.length - 1];
  };

  render() {
    return (
      <React.Fragment>
        <Row className="container_row">
          <Col span={24}>
            <TierStepDataWidget defaultOpenKey={this.navigationOpenedKey()} />
          </Col>
        </Row>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    user: state.user,
    tierInfo: state.tierInfo
  };
};

const mapDispatchToProps = dispatch => {
  return {
    get: type => {
      dispatch(get(type));
    },
    setInvestmentPlan: data => {
      dispatch(setInvestmentPlan(data));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Kyc);
