import React from "react";
import { connect } from "react-redux";
import { NavLink } from "react-router-dom";
import { Card, Col, Row, Button, Icon } from "antd";
import { setInvestmentPlan, get, openSelectProvider } from "redux/actions";
import introJs from "intro.js";
import "intro.js/introjs.css";
import { SelectBlockchainProvider } from "components";
import style from "./index.css";

class Overview extends React.Component {
  componentDidMount() {}

  componentDidUpdate(prevProps, prevState, snapshot) {
    const {
      user: { target_tier },
      history
    } = this.props;

    // if (prevProps.user.target_tier === null && target_tier === null) {
    //   introJs().start();
    // }

    if (
      prevProps.user.target_tier === null &&
      prevProps.user.target_tier !== target_tier
    ) {
      this.props.get("GET_TIERS_INFO");
      history.push("/profile/kyc");
    }
  }

  componentWillUnmount() {
    // introJs().exit();
  }

  renderKYCSummary = () => {
    const {
      user: { kyc_summary: kyc, target_tier },
      setInvestmentPlan
    } = this.props;

    return (
      <Card title={"KYC progress"} className={"personal-info"}>
        {target_tier !== null && kyc ? (
          <React.Fragment>
            <Row gutter={8}>
              <Col span={10}>Tier achieved</Col>
              <Col span={10}>
                {kyc["achieved"] ? kyc["achieved"]["name"] : "None"}
              </Col>
            </Row>
            <Row gutter={8}>
              <Col span={10}>Tier which requires input</Col>
              <Col span={10}>
                {kyc.ongoing ? (
                  <NavLink to={`/profile/kyc/tier/${kyc.ongoing["id"]}`}>
                    {kyc.ongoing["name"]}
                  </NavLink>
                ) : (
                  "None"
                )}
              </Col>
            </Row>
            {kyc.upcoming ? (
              <Row gutter={8}>
                <Col span={10}>Upcoming tier</Col>
                <Col span={10}>{kyc.upcoming["name"]}</Col>
              </Row>
            ) : null}
          </React.Fragment>
        ) : (
          <React.Fragment>
            {" "}
            <Button
              className={"ant-btn ant-btn-primary ant-btn-block"}
              data-intro="Button!"
              data-step="2"
              onClick={() => setInvestmentPlan(0)}
            >
              Please, proceed to create your KYC application
            </Button>
          </React.Fragment>
        )}
      </Card>
    );
  };

  renderBlockchainProvider = () => {
    const {
      user: { blockchain_connection_type },
      openSelectProvider
    } = this.props;

    return !!blockchain_connection_type ? (
      <Row gutter={2}>
        <Col span={9}>
          <span className={"personal-info__title"}>Blockchain provider:</span>{" "}
          {blockchain_connection_type}
        </Col>
        {!!blockchain_connection_type ? (
          <Col span={8}>
            <a><Icon type="edit" onClick={openSelectProvider}/></a>
          </Col>
        ) : null}
      </Row>
    ) : null;
  };

  render() {
    const {
      user: {
        first_name,
        last_name,
        email,
        eth_address,
        birth_date,
        blockchain_connection_type
      }
    } = this.props;

    return (
      <React.Fragment>
        <Row className="container_row" gutter={48}>
          <Col span={12}>
            <Card title={"Personal information"} className={"personal-info"}>
              <Row>
                <Col>
                  <span className={"personal-info__title"}>Name:</span>{" "}
                  {first_name} {last_name}
                </Col>
                <Col>
                  <span className={"personal-info__title"}>Email:</span> {email}
                </Col>
                <Col>
                  <span className={"personal-info__title"}>
                    Ethereum address:
                  </span>{" "}
                  {eth_address}
                </Col>
                <Col>
                  <span className={"personal-info__title"}>Birth date:</span>{" "}
                  {birth_date}
                </Col>
                {this.renderBlockchainProvider()}
              </Row>
            </Card>
          </Col>
          <Col span={12}>{this.renderKYCSummary()}</Col>
        </Row>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    user: state.user
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setInvestmentPlan: data => {
      dispatch(setInvestmentPlan(data));
    },
    get: type => {
      dispatch(get(type));
    },
    openSelectProvider: () => {
      return dispatch(openSelectProvider());
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Overview);
