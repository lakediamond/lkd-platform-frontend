import React from "react";
import { connect } from "react-redux";
import { Row, Col, Skeleton, Card } from "antd";
import {
  OrdersTable,
  CreateOrderDataWidget,
  MetamaskNotExistCard,
  OrderMatchingSimulation,
  Spinner
} from "components";
import { get, isMetamaskInstalled, webSocketData } from "redux/actions";
import { startAuth } from "services";
import { ORDERS_SOCKET, GET_ORDERS } from "redux/constants";

class Orders extends React.Component {
  componentWillMount() {
    this.timeouts = [];
  }

  componentDidMount() {
    this.fetchOrders();

    // const {
    //   user: { role },
    //   history,
    //   blockchain: {
    //     metaMaskAccount,
    //     isInstalled,
    //     ioContractInstance,
    //     isContractOwner,
    //     isLoggedIn
    //   },
    //   isMetamaskInstalled
    // } = this.props;
    
  }
  

  componentWillUnmount() {
    // this.props.webSocketData(ORDERS_SOCKET, "finish");
  }

  fetchOrders = (param = null) => {
    // this.props.fetchOrdersList();
    this.props.get(GET_ORDERS, null, param);
    // this.props.webSocketData(ORDERS_SOCKET, "start");
    // this.timeouts.push(setTimeout(this.fetchOrders, 1000));
  };

  renderContent = () => {
    const {
      blockchain: { metaMaskAccount },
      ordersList: {
        orders: { records, total_record }
      },
      user: { role }
    } = this.props;

    return (
      <React.Fragment>
        {!!role && role === "admin" && (
          <Row className="container_row" gutter={48}>
            <Col span={12}>
              <CreateOrderDataWidget title={"CREATE AN ORDER"}/>
            </Col>
          </Row>
        )}
        <Row>
          <Col span={24}>
            <OrdersTable
              collection={records}
              account={metaMaskAccount}
              totalRecord={total_record}
              title={"ORDERS LIST"}
              type={"full"}
              action={this.fetchOrders}
            />
          </Col>
        </Row>
      </React.Fragment>
    );
  };

  render() {
    return <React.Fragment>{this.renderContent()}</React.Fragment>;
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    ordersList: state.ordersList,
    user: state.user,
    blockchain: state.blockchain
  };
};

const mapDispatchToProps = dispatch => {
  return {
    isMetamaskInstalled: () => {
      dispatch(isMetamaskInstalled());
    },
    get: (type, body, param) => {
      dispatch(get(type, body, param));
    }
    // webSocketData: (type, action) => {
    //   dispatch(webSocketData(type, action));
    // }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Orders);
