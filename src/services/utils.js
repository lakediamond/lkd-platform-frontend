import moment from "moment";
import { BLOCKCHAIN_EXPLORER_URL } from "redux/constants";
const OWASP = require("owasp-password-strength-test");
const BN = require('bn.js');

OWASP.config({
  allowPassphrases: false,
  maxLength: 128,
  minLength: 10,
  minPhraseLength: 0,
  minOptionalTestsToPass: 4
});

export const startAuth = () => (window.location.href = "/auth");

export const increaseValueForBlockchain = value =>
  Number(value) * Math.pow(10, 18);

export const decreaseValueForBlockchain = value => {
  return Number(value) / Math.pow(10, 18);
};

export const decreaseValueForBlockchainUI = value => {
  let res = Number(value) / Math.pow(10, 18);
  return res % 1 ? res.toFixed(4) : res;
};

export const multiplyBy100 = (value) => {
  return Math.round(value * 100);
};

export const divideValueFor100 = value => {
  return Math.round(Number(value * 10000))/1000000;
};

// const integerExchangeRate = Math.round(exchangeRate * 100);
// const integerPrice = Math.round(price * 100);

// sendConfig.value = new BN(10).pow(new BN(18)).mul(new BN(integerPrice)).mul(new BN(tknAmount)).div(new BN(integerExchangeRate));

export const getEtherAmount = (price, amount, price_rate) => {
  const bPrice =  Math.round(price * 100);
  const bPriceRate = Math.round(price_rate * 100);
  const res = new BN(10).pow(new BN(18)).mul(new BN(bPrice)).mul(new BN(amount)).div(new BN(bPriceRate));
  return res.toString(10);
  // return (Math.round(price * 100) * amount) / Math.round(price_rate * 100);
};

export const formatData = value =>
  moment(value)
    .utc()
    .format("YYYY/MM/DD  HH:mm:ss");

export const createAddressLink = value =>
  `${BLOCKCHAIN_EXPLORER_URL}/address/${value}`;

export const createTXLink = value => `${BLOCKCHAIN_EXPLORER_URL}${value}`;

// export const createTXLink = value => `${BLOCKCHAIN_EXPLORER_URL}/tx/${value}`;

export const checkPasswordOWASP = value => {
  const result = OWASP.test(value);
  return !result["errors"].length
    ? "valid"
    : result["errors"].map(item => item.padStart(item.length + 1)).join(",");
};
