import {
  GET_TOKEN_EXCHANGE_PATH,
  GET_GENERIC_UPLOAD_IMAGE_PATH,
  TIERS_INFO_PATH,
  GET_VERIFICATION_PHONE_INFO_PATH,
  GET_COMMON_INFO_PATH,
  GET_INVESTMENT_PLAN_PATH,
  GET_2FA_CODE_PATH,
  POST_SMS_CODE_PATH,
  POST_COMMON_INFO_PATH,
  POST_PHONE_NUMBER_PATH,
  ENABLE_2FA_PATH,
  DISABLE_2FA_PATH,
  ORDER_TYPES_PATH,
  ORDERS_PATH,
  PROPOSALS_ALL_PATH,
  PROPOSALS_USER_PATH,
  USER_REQUEST_LOGIN_PATH,
  CHECK_OAUTH_REQUEST_PATH,
  OAUTH_TOKEN_EXCHANGE_PATH,
  AUTHENTICATE_USER_PATH,
  LOGOUT_USER_PATH,
  REGISTRATION_PATH,
  SUB_OWNERS_PATH,
  USER_PROFILE_PATH,
  COMPLIANCE_REVIEW_PATH,
  RESEND_PHONE_NUMBER_PATH,
  RESTORE_PASSWORD_PATH,
  CHECK_CHALLENGE_PATH,
  CHANGE_PASSWORD_PATH,
  SEND_EMAIL_FOR_RESTORE_PATH,
  GET_IDENTITY_SCANS_PATH,
  CHECK_POSSIBILITY_TO_UPLOAD_PHOTO,
  // GET_ETHER_AMOUNT_PATH,
  GET_SUB_OWNERS,
  GET_ORDER_TYPES,
  GET_ORDERS,
  GET_PROPOSALS_ALL,
  GET_PROPOSALS_USER,
  GET_PROPOSALS_OWNERS,
  GET_USER_REQUEST_LOGIN,
  GET_CHECK_OAUTH_LOGIN_REQUEST,
  GET_CHECK_OAUTH_CONSENT_REQUES,
  // GET_ETHER_AMOUNT,
  GET_INVESTMENT_PLAN,
  GET_OAUTH_TOKEN_EXCHANGE,
  GET_OAUTH_LOGIN_REQUEST,
  GET_OAUTH_CONSENT_REQUEST,
  GET_USER_PROFILE_DATA,
  GET_GENERIC_UPLOAD_IMAGE_URL,
  GET_TIERS_INFO,
  GET_PHONE_INFO,
  GET_COMMON_INFO,
  GET_IDENTITY_SCANS,
  GET_COMPLIANCE_REVIEW,
  GET_2FA_CODE,
  POST_CONSENT,
  POST_AUTHENTICATE_USER,
  POST_REGISTRATION,
  POST_SIGN_IN,
  POST_PHONE_NUMBER,
  DELETE_PHONE_NUMBER,
  POST_SMS_CODE,
  POST_INVESTMENT_PLAN,
  POST_COMMON_INFO,
  POST_COMPLIANCE_REVIEW,
  POST_EMAIL_FOR_RESTORE,
  RESEND_PHONE_NUMBER,
  ENABLE_2FA,
  DISABLE_2FA,
  RESTORE_PASSWORD,
  CHECK_CHALLENGE,
  CHANGE_PASSWORD,
  LOGOUT_USER,
  POST_PROVIDER_PATH,
  POST_PROVIDER,
  GET_TX_DATA_PATH,
  GET_TX_DATA,
  GET_PROPOSALS_QUEUE_INFO,
  GET_PROPOSALS_QUEUE_INFO_PATH,
  GET_PROPOSALS_OWNERS_PATH,
  GET_ETH_EXCHANGE_RATE,
  GET_ETH_EXCHANGE_RATE_PATH,
  GET_ETH_EXCHANGE_RATE_WALLET,
  GET_CHART_DATA_PATH,
  GET_CHART_DATA
} from "redux/constants";

const default_config = (type, body = null) => {
  const config = {
    method: `${type}`,
    credentials: "include",
    headers: new Headers({ "content-type": "application/json" }),
    mode: "cors"
  };

  if (body) config["body"] = JSON.stringify(body);
  return config;
};

const upload_config = (type, body) => {
  const config = {
    method: `${type}`,
    credentials: "include",
    headers: new Headers({
      "response-content-disposition": `attachment; filename=${body}`
    }),
    mode: "cors"
  };
  return config;
};

export const Request = async (type, body, param) => {
  switch (type) {
    case POST_REGISTRATION:
      try {
        return await fetch(REGISTRATION_PATH, default_config("POST", body));
      } catch (error) {
        return false;
      }
    case POST_SIGN_IN:
      try {
        return await fetch(
          AUTHENTICATE_USER_PATH,
          default_config("POST", body)
        );
      } catch (error) {
        return false;
      }
    case GET_SUB_OWNERS:
      try {
        return await fetch(SUB_OWNERS_PATH, default_config("GET"));
      } catch (error) {
        return false;
      }
    case GET_ORDER_TYPES:
      try {
        return await fetch(ORDER_TYPES_PATH, default_config("GET"));
      } catch (error) {
        return false;
      }
    case GET_ORDERS:
      try {
        return await fetch(`${ORDERS_PATH}${param}`, default_config("GET"));
      } catch (error) {
        return false;
      }
    case GET_PROPOSALS_ALL:
      try {
        return await fetch(
          `${PROPOSALS_USER_PATH}${param}`,
          default_config("GET")
        );
      } catch (error) {
        return false;
      }
    case GET_PROPOSALS_USER:
      try {
        return await fetch(
          `${PROPOSALS_USER_PATH}${param}`,
          default_config("GET")
        );
      } catch (error) {
        return false;
      }
    case GET_USER_REQUEST_LOGIN:
      try {
        return await fetch(USER_REQUEST_LOGIN_PATH, default_config("GET"));
      } catch (error) {
        return false;
      }
    case POST_AUTHENTICATE_USER:
      try {
        return await fetch(AUTHENTICATE_USER_PATH, default_config("POST"));
      } catch (error) {
        return false;
      }
    case GET_OAUTH_LOGIN_REQUEST:
      try {
        return await fetch(
          `${CHECK_OAUTH_REQUEST_PATH}${param}`,
          default_config("GET")
        );
      } catch (error) {
        return false;
      }
    case GET_OAUTH_CONSENT_REQUEST:
      try {
        return await fetch(
          `${CHECK_OAUTH_REQUEST_PATH}${param}`,
          default_config("GET")
        );
      } catch (error) {
        return false;
      }
    case POST_CONSENT:
      try {
        return await fetch(`${POST_CONSENT}`, default_config("POST", body));
      } catch (error) {
        return false;
      }
    case GET_OAUTH_TOKEN_EXCHANGE:
      try {
        return await fetch(
          `${OAUTH_TOKEN_EXCHANGE_PATH}${param}`,
          default_config("GET")
        );
      } catch (error) {
        return false;
      }
    // case GET_ETHER_AMOUNT:
    //   try {
    //     return await fetch(
    //       `${GET_ETHER_AMOUNT_PATH}${param}`,
    //       default_config("GET")
    //     );
    //   } catch (error) {
    //     return false;
    //   }
    case GET_USER_PROFILE_DATA:
      try {
        return await fetch(`${USER_PROFILE_PATH}`, default_config("GET"));
      } catch (error) {
        return false;
      }
    case CHECK_POSSIBILITY_TO_UPLOAD_PHOTO:
      try {
        return await fetch(
          `${GET_GENERIC_UPLOAD_IMAGE_PATH}${param}`,
          default_config("GET")
        );
      } catch (error) {
        return false;
      }
    case GET_IDENTITY_SCANS:
      try {
        return await fetch(`${GET_IDENTITY_SCANS_PATH}`, upload_config("GET"));
      } catch (error) {
        return false;
      }
    case GET_GENERIC_UPLOAD_IMAGE_URL:
      try {
        return await fetch(
          `${GET_GENERIC_UPLOAD_IMAGE_PATH}${param}`,
          upload_config("GET", body)
        );
      } catch (error) {
        return false;
      }
    case POST_PHONE_NUMBER:
      try {
        return await fetch(
          `${POST_PHONE_NUMBER_PATH}`,
          default_config("POST", body)
        );
      } catch (error) {
        return false;
      }
    case DELETE_PHONE_NUMBER:
      try {
        return await fetch(
          `${POST_PHONE_NUMBER_PATH}`,
          default_config("DELETE")
        );
      } catch (error) {
        return false;
      }
    case RESEND_PHONE_NUMBER:
      try {
        return await fetch(
          `${RESEND_PHONE_NUMBER_PATH}`,
          default_config("POST", body)
        );
      } catch (error) {
        return false;
      }
    case POST_SMS_CODE:
      try {
        return await fetch(
          `${POST_SMS_CODE_PATH}`,
          default_config("POST", body)
        );
      } catch (error) {
        return false;
      }
    case POST_INVESTMENT_PLAN:
      try {
        return await fetch(`${TIERS_INFO_PATH}`, default_config("POST", body));
      } catch (error) {
        return false;
      }
    case GET_TIERS_INFO:
      try {
        return await fetch(`${TIERS_INFO_PATH}`, default_config("GET"));
      } catch (error) {
        return false;
      }
    case GET_PHONE_INFO:
      try {
        return await fetch(
          `${GET_VERIFICATION_PHONE_INFO_PATH}`,
          default_config("GET")
        );
      } catch (error) {
        return false;
      }
    case GET_COMMON_INFO:
      try {
        return await fetch(`${GET_COMMON_INFO_PATH}`, default_config("GET"));
      } catch (error) {
        return false;
      }
    case POST_COMMON_INFO:
      try {
        return await fetch(
          `${POST_COMMON_INFO_PATH}`,
          default_config("POST", body)
        );
      } catch (error) {
        return false;
      }
    case GET_INVESTMENT_PLAN:
      try {
        return await fetch(
          `${GET_INVESTMENT_PLAN_PATH}`,
          default_config("GET")
        );
      } catch (error) {
        return false;
      }
    case GET_COMPLIANCE_REVIEW:
      try {
        return await fetch(`${COMPLIANCE_REVIEW_PATH}`, default_config("GET"));
      } catch (error) {
        return false;
      }
    case POST_COMPLIANCE_REVIEW:
      try {
        return await fetch(
          `${COMPLIANCE_REVIEW_PATH}`,
          default_config("POST", body)
        );
      } catch (error) {
        return false;
      }
    case ENABLE_2FA:
      try {
        return await fetch(`${ENABLE_2FA_PATH}`, default_config("POST", body));
      } catch (error) {
        return false;
      }
    case GET_2FA_CODE:
      try {
        return await fetch(`${GET_2FA_CODE_PATH}`, default_config("POST"));
      } catch (error) {
        return false;
      }
    case DISABLE_2FA:
      try {
        return await fetch(`${DISABLE_2FA_PATH}`, default_config("POST", body));
      } catch (error) {
        return false;
      }
    case LOGOUT_USER:
      try {
        return await fetch(`${LOGOUT_USER_PATH}`, default_config("POST"));
      } catch (error) {
        return false;
      }
    case POST_EMAIL_FOR_RESTORE:
      try {
        return await fetch(
          SEND_EMAIL_FOR_RESTORE_PATH,
          default_config("POST", body)
        );
      } catch (error) {
        return false;
      }
    case CHECK_CHALLENGE:
      try {
        return await fetch(
          `${CHECK_CHALLENGE_PATH}`,
          default_config("POST", body)
        );
      } catch (error) {
        return false;
      }
    case RESTORE_PASSWORD:
      try {
        return await fetch(
          `${RESTORE_PASSWORD_PATH}`,
          default_config("POST", body)
        );
      } catch (error) {
        return false;
      }
    case CHANGE_PASSWORD:
      try {
        return await fetch(
          `${CHANGE_PASSWORD_PATH}`,
          default_config("POST", body)
        );
      } catch (error) {
        return false;
      }
    case POST_PROVIDER:
      try {
        return await fetch(
          `${POST_PROVIDER_PATH}`,
          default_config("PATCH", body)
        );
      } catch (error) {
        return false;
      }
    case GET_TX_DATA:
      try {
        return await fetch(
          `${GET_TX_DATA_PATH}${param}`,
          upload_config("GET", body)
        );
      } catch (error) {
        return false;
      }
    case GET_PROPOSALS_QUEUE_INFO:
      try {
        return await fetch(
          `${GET_PROPOSALS_QUEUE_INFO_PATH}${param}`,
          upload_config("GET")
        );
      } catch (error) {
        return false;
      }
    case GET_PROPOSALS_OWNERS:
      try {
        return await fetch(
          `${GET_PROPOSALS_OWNERS_PATH}`,
          upload_config("GET")
        );
      } catch (error) {
        return false;
      }
    case GET_ETH_EXCHANGE_RATE:
      try {
        return await fetch(
          `${GET_ETH_EXCHANGE_RATE_PATH}${param}`,
          upload_config("GET")
        );
      } catch (error) {
        return false;
      }
    case GET_ETH_EXCHANGE_RATE_WALLET:
      try {
        return await fetch(
          `${GET_ETH_EXCHANGE_RATE_PATH}${param}`,
          upload_config("GET")
        );
      } catch (error) {
        return false;
      }
    case GET_CHART_DATA:
      try {
        return await fetch(
          `${GET_CHART_DATA_PATH}${param}`,
          upload_config("GET")
        );
      } catch (error) {
        return false;
      }
    default:
      return false;
  }
};
