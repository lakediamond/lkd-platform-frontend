import { Request } from "./fetch";
import {
  startAuth,
  decreaseValueForBlockchain,
  increaseValueForBlockchain,
  formatData,
  createAddressLink,
  createTXLink,
  checkPasswordOWASP,
  getEtherAmount,
  multiplyBy100,
  divideValueFor100,
  decreaseValueForBlockchainUI
} from "./utils";

export {
  startAuth,
  Request,
  decreaseValueForBlockchain,
  increaseValueForBlockchain,
  formatData,
  createAddressLink,
  createTXLink,
  checkPasswordOWASP,
  getEtherAmount,
  multiplyBy100,
  divideValueFor100,
  decreaseValueForBlockchainUI
};
