import React from "react";
import ReactDOM from "react-dom";
import { createBrowserHistory } from "history";
import { Router, Route, Switch } from "react-router-dom";
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk'
import { createLogger } from 'redux-logger'

import tokensiteApp from './redux/reducers';
import "assets/css/material-dashboard-react.css";
import "assets/css/App.css";
import indexRoutes from "routes/index.jsx";


const loggerMiddleware = createLogger();

export const store = createStore(
  tokensiteApp,
  applyMiddleware(
    thunkMiddleware,
    loggerMiddleware,
  ),
  // window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

const hist = createBrowserHistory();

ReactDOM.render(
  <Router history={hist}>
    <Provider store={store}>
      <Switch>
        {indexRoutes.map((prop, key) => {
          return <Route path={prop.path} component={prop.component} key={key} />;
        })}
      </Switch>
    </Provider>
  </Router>,
  document.getElementById("root")
);
