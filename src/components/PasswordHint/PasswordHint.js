import React from "react";

const PasswordHint = () => {
  return (
    <React.Fragment>
      Password must meet at least 3 out of the following 4 complexity rules:
      <ul>
        <ul>
          <li>at least 1 uppercase character (A-Z)</li>
          <li>at least 1 lowercase character (a-z)</li>
          <li>at least 1 digit (0-9)</li>
        </ul>
      </ul>
      <ul>
        <li>at least 10 characters</li>
        <li>at most 128 characters</li>
        <li>
          not more than 2 identical characters in a row (e.g., 111 not
          allowed)
        </li>
      </ul>
    </React.Fragment>
  )
}

export default PasswordHint;