import React from "react";
import { Button, Icon } from "antd";
import { connect } from "react-redux";
import { openSetupProvider } from "redux/actions";

const SetupBlockchainProvider = props => {
  const { openSetupProvider, type } = props;

  if (type === "full") {
    return (
      <Button
        className={"setup-provider button_secondary"}
        onClick={() => openSetupProvider()}
      >
        <Icon type="setting" /> Setup wallet
      </Button>
    );
  } else if (type === "middle") {
    return (
      <Button
        className={"setup-provider button_secondary setup-provider_middle"}
        onClick={() => openSetupProvider()}
      >
        <Icon type="setting" /> Setup
      </Button>
    );
  } else {
    return (
      <span onClick={() => openSetupProvider()} className={"ant-table__link"}>
        Setup provider
      </span>
    );
  }
};
const mapDispatchToProps = dispatch => {
  return {
    openSetupProvider: () => {
      return dispatch(openSetupProvider());
    }
  };
};

export default connect(
  null,
  mapDispatchToProps
)(SetupBlockchainProvider);

{
  /*<Icon*/
}
{
  /*type="setting"*/
}
{
  /*className={"setupBlockchainProvider_short"}*/
}

{
  /*/>*/
}
