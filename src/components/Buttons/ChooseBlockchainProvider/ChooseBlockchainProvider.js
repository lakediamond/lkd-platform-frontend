import React from "react";
import { Button } from "antd";
import { connect } from "react-redux";
import { openSelectProvider } from "redux/actions";

const ChooseBlockchainProvider = props => {
  const { openSelectProvider } = props;
  return (
    <Button
      className={"setup-provider button_secondary"}
      onClick={openSelectProvider}
    >
      Select blockchain provider
    </Button>
  );
};

const mapDispatchToProps = dispatch => {
  return {
    openSelectProvider: () => {
      return dispatch(openSelectProvider());
    }
  };
};

export default connect(
  null,
  mapDispatchToProps
)(ChooseBlockchainProvider);
