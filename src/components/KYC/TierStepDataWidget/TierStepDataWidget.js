import React from "react";
import { Card, Col, Input, Row, Button, Menu, Icon } from "antd";
import {
  Navigation,
  PhoneDataWidget,
  DocumentDataWidget,
  CommonInfoDataWidget,
  // ComplianceReviewDataWidget
} from "components";
import { connect } from "react-redux";

class TierStepDataWidget extends React.Component {
  state = {
    selectedStep: null
  };
  
  
  componentDidUpdate(prevProps, prevState, snapshot) {
    const { tierInfo } = this.props;
    if(prevProps.tierInfo !== tierInfo){
      this.openTier();
    }
  }
  
  renderStep = () => {
    const { selectedStep } = this.state;

    switch (selectedStep) {
      case null:
        return <div>You have to choose on‌e of the required steps</div>;
      case "phone_verification":
        return <PhoneDataWidget />;
      case "identity_scans":
        return <DocumentDataWidget />;
      case "common_info":
        return <CommonInfoDataWidget />;
      // case "compliance_review":
      //   return <ComplianceReviewDataWidget />;
      default:
        return null;
    }
  };
  
  changeStep = value => {
    return this.setState({ selectedStep: value });
  };
  
  openTier = () => {
    const { tierInfo } = this.props;
    
    let steps;
    
    if (tierInfo) {
      if (tierInfo.tierInfo) {
        steps = tierInfo.tierInfo[0]['steps'].filter(item => item['status'] === "OPENED");
        if(steps && steps.length){
          return this.changeStep(steps[0]['code']);
        }
      }
    }
    
    return "";
  };
  
  render() {
    const { defaultOpenKey, tierInfo } = this.props;
    const { selectedStep } = this.state;
    
    if (tierInfo) {
      if (tierInfo.tierInfo) {
        if (tierInfo.tierInfo[0]["status"] === "SUBMITTED") {
          return (
            <React.Fragment>
              <Card>
                <Row gutter={48}>
                  <Col lg={24} xl={8} className={"responsive_row"}>
                    <Navigation
                      defaultOpenKey={defaultOpenKey}
                      changeStep={this.changeStep}
                    />
                  </Col>
                  <Col lg={{ offset: 0, span: 24 }} xl={{ span:15, offset: 1 }} className={"responsive_tier_step"}>
                    <div>
                      Your KYC application has been accepted for review. Please,
                      check your mailbox regularly to be on track for any
                      further communications
                    </div>
                  </Col>
                </Row>
              </Card>
            </React.Fragment>
          );
        }
      }
    }
    
    return (
      <React.Fragment>
        <Card>
          <Row gutter={48}>
            <Col lg={24} xl={8} className={"responsive_row"}>
              <Navigation
                defaultOpenKey={defaultOpenKey}
                defaultSelectedStep={selectedStep}
                changeStep={this.changeStep}
              />
            </Col>
            <Col lg={{ offset: 0, span: 24 }} xl={{ span:15, offset: 1 }} className={"responsive_tier_step"}>
              {defaultOpenKey === "kyc" ? null : this.renderStep()}
            </Col>
          </Row>
        </Card>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    tierInfo: state.tierInfo
  };
};

export default connect(
  mapStateToProps,
  null
)(TierStepDataWidget);
