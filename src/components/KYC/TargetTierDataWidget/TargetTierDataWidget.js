import React from "react";
import { connect } from "react-redux";
import {Form, Select, Card, Row, Col, Button, Icon} from "antd";
import { get } from "redux/actions";

const FormItem = Form.Item;
const Option = Select.Option;

class TargetTierDataWidget extends React.Component {
  state = {
    change_target_tier: null
  };

  componentDidMount() {
    this.props.get("GET_INVESTMENT_PLAN");
  }
  
  componentDidUpdate(prevProps, prevState, snapshot) {
    // const {
    //   fetch: { state }
    // } = this.props;
    //
    // if(prevProps.fetch.state === true && state === false){
    //   this.setState({ change_target_tier: false });
    // }
  }
  
  // handleSubmit = e => {
  //   e.preventDefault();
  //   this.props.form.validateFields(async (err, values) => {
  //     if (!err) {
  //       this.props.setInvestmentPlan(values["target_tier"]);
  //     }
  //   });
  // };

  renderFormChoseInvestmentPlan = () => {
    const {
      form: { getFieldDecorator },
      user: { investment_plans, target_tier },
      fetch: { state }
    } = this.props;

    const children = [];

    investment_plans
      ? investment_plans.map((value, index) => {
          children.push(
            <Option
              key={`${index}` + Math.random()}
              value={`${value["code"]}|${value["description"]}`}
              key={value["code"]}
            >
              {value["description"]}
            </Option>
          );
        })
      : null;

    return (
      <Form onSubmit={e => this.handleSubmit(e)}>
        <Row>
          <Col span={8}>
            <FormItem>
              {getFieldDecorator("target_tier", {
                validateTrigger: "onBlur",
                rules: [
                  {
                    required: true,
                    message: "Please select your investment plan"
                  }
                ]
              })(<Select showSearch>{children}</Select>)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={4}>
            <Button type="primary" block htmlType="submit" disabled={state}>
              {state ? (
                <Icon type="sync" spin style={{ color: "fff" }} />
              ) : (
                "Confirm"
              )}
            </Button>
          </Col>
        </Row>
      </Form>
    );
  };

  renderChangeInvestmentPlan = () => {
    return (
      <Button
        type="primary"
        onClick={() => this.setState({ change_target_tier: true })}
      >
        Change your investment plan
      </Button>
    );
  };

  renderTitle = () => {
    const {
      user: { target_tier }
    } = this.props;

    const { change_target_tier } = this.state;

    if (target_tier === null || (!!target_tier && change_target_tier)) {
      return "Please choose your investment plan";
    }

    if (!!target_tier && !change_target_tier) {
      return `Your investment plan: ${
        target_tier ? target_tier["description"] : ""
      }`;
    }
  };

  render() {
    const {
      user: { target_tier }
    } = this.props;

    const { change_target_tier } = this.state;

    return (
      <Card title={this.renderTitle()}>
        {!!target_tier && !change_target_tier
          ? this.renderChangeInvestmentPlan()
          : this.renderFormChoseInvestmentPlan()}
      </Card>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    user: state.user,
    fetch: state.fetch
  };
};

const mapDispatchToProps = dispatch => {
  return {
    get: type => {
      dispatch(get(type));
    }
  };
};

const TargetTierDataWidgetComponent = Form.create()(TargetTierDataWidget);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TargetTierDataWidgetComponent);
