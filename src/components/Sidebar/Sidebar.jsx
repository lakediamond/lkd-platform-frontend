import React from "react";
import { NavLink } from "react-router-dom";
import lkd_logo from "assets/img/lkd_logo.png";
import style from "./index.css";
import { Menu, Icon, Layout, Divider } from "antd";
const { Sider } = Layout;

const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;

const Sidebar = ({ ...props }) => {
  const {
    routes,
    history: {
      location: { pathname }
    },
    user: { role }
  } = props;

  //TODO change this ASAP
  const achieved = props.user.kyc_summary
    ? props.user.kyc_summary["achieved"]
    : null;

  const links = () => {
    return routes.map((prop, key) => {
      if (
        prop.redirect ||
        (prop.path === "/management" && role !== "admin") ||
        prop.path === "/consent" ||
        prop.path === "/auth" ||
        prop.path === "/" ||
        prop.path.includes("profile") ||
        prop.path === "/user/access_recovery_activation"
        // ||
        // (prop.path === "/proposals" && !!!achieved)
      )
        return null;
      return (
        <Menu.Item key={prop.path}>
          <NavLink to={prop.path} key={key} onClick={handleClickLink}>
            <Icon type={prop.icon} />
            {prop.sidebarName}
          </NavLink>
        </Menu.Item>
      );
    });
  };

  const handleClickLink = e => {
    const { linksDisabled } = props;
    if (linksDisabled) e.preventDefault();
  };
  
  const brand = (
    <React.Fragment>
      <NavLink to="/profile/overview">
        <img className="logo_lake" src={lkd_logo} alt="logo" />
      </NavLink>
      <Divider className={"logo_divider"} />
    </React.Fragment>
  );

  return (
    <Sider theme={"dark"}>
      <Menu
        theme={"dark"}
        mode="inline"
        selectable={true}
        selectedKeys={[`${pathname}`]}
        defaultOpenKeys={["overview", "security"]}
      >
        {brand}
        {links()}
        <SubMenu
          key="overview"
          title={
            <span>
              <Icon type="setting" />
              <span>Profile</span>
            </span>
          }
        >
          <Menu.Item
            key="to_profile"
            className={
              pathname == "/profile/overview" ? "ant-menu-item-selected" : null
            }
          >
            <NavLink
              exact
              to="/profile/overview"
              key={"21"}
              onClick={handleClickLink}
            >
              <Icon type="desktop" />
              Overview
            </NavLink>
          </Menu.Item>
          <Menu.Item
            key="to_kyc"
            className={
              pathname.includes("/profile/kyc")
                ? "ant-menu-item-selected"
                : null
            }
          >
            <NavLink to="/profile/kyc" key={"21"} onClick={handleClickLink}>
              <Icon type="solution" />
              KYC area
            </NavLink>
          </Menu.Item>
          <SubMenu
            key="security"
            title={
              <span>
                <Icon type="key" />
                <span>Security</span>
              </span>
            }
            className={"ant-menu-submenu-open"}
          >
            <Menu.Item
              key="to_security_password"
              className={
                pathname == "/profile/security/password"
                  ? "ant-menu-item-selected"
                  : null
              }
            >
              <NavLink
                to="/profile/security/password"
                key={"21"}
                onClick={handleClickLink}
              >
                <Icon type="lock" />
                Password
              </NavLink>
            </Menu.Item>
            <Menu.Item
              key="to_2fa"
              className={
                pathname == "/profile/security/2FA"
                  ? "ant-menu-item-selected"
                  : null
              }
            >
              <NavLink
                to="/profile/security/2FA"
                key={"21"}
                onClick={handleClickLink}
              >
                <Icon type="barcode" />
                2FA settings
              </NavLink>
            </Menu.Item>
          </SubMenu>
        </SubMenu>
      </Menu>
    </Sider>
  );
};

export default Sidebar;
