import React from "react";
import { Card, Row, Col, Button, Progress, Steps, Modal } from "antd";
import { connect } from "react-redux";
import {
  isMetamaskInstalled,
  isMetamaskLoggedIn,
  enableMetamask,
  checkAllowance,
  closeSetupProvider
} from "redux/actions";

import style from "./index.css";

const Step = Steps.Step;

class BlockchainProviderSettings extends React.Component {
  state = {
    provider: {
      installed: false,
      loggedIn: false,
      connection: false,
      allowanceReceived: false
    },
    current: 0
  };

  componentWillMount() {
    this.intervals = [];
  }

  componentWillUnmount() {
    this.intervals.map(item => clearInterval(item));
    this.intervals = [];
  }

  getPercent = () => {
    const { provider } = this.state;

    let percent = Object.keys(provider).filter(item => provider[item]).length
      ? 25 * Object.keys(provider).filter(item => !!provider[item]).length -
        100 +
        100
      : 0;

    return percent;
  };

  componentDidUpdate(prevProps, prevState, snapshot) {
    const {
      blockchain: {
        isInstalled,
        isLoggedIn,
        metaMaskAccount,
        isEnabled,
        allowance,
        setupProviderOpened
      },
      isMetamaskInstalled,
      isMetamaskLoggedIn,
      enableMetamask,
      checkAllowance,
      user,
      closeSetupProvider
    } = this.props;

    const {
      provider: { loggedIn, connection, allowanceReceived },
      current
    } = this.state;

    if (
      prevProps.blockchain.setupProviderOpened === false &&
      prevProps.blockchain.setupProviderOpened !== setupProviderOpened &&
      !!!isInstalled
    ) {
      isMetamaskInstalled();
    }

    if (!!!isLoggedIn && setupProviderOpened && isInstalled) {
      if (this.intervals.length < 2) {
        this.intervals.push(
          setInterval(() => {
            isMetamaskLoggedIn();
          }, 1000)
        );
      }

      if (current !== 1) {
        const provider = { ...this.state.provider };
        provider.installed = true;
        this.setState({ provider, current: 1 });
      }
    }

    if (isLoggedIn && metaMaskAccount === null && !loggedIn) {
      this.intervals.map(item => clearInterval(item));
      const provider = { ...this.state.provider };
      provider.loggedIn = true;
      this.setState({ provider, current: 2 });
      enableMetamask();
      this.intervals.map(item => clearInterval(item));
      this.intervals = [];
    }

    if (!!isEnabled && !connection) {
      const provider = { ...this.state.provider };
      provider.connection = true;
      this.setState({ provider, current: 3 });
      checkAllowance(metaMaskAccount);
    }

    if (
      prevProps.blockchain.allowance === null &&
      !!allowance &&
      !allowanceReceived
    ) {
      const provider = { ...this.state.provider };
      provider.allowanceReceived = true;
      this.setState({ provider });
    }

    // if(isInstalled,
    //   isLoggedIn,
    //   metaMaskAccount,
    //   isEnabled,
    //   allowance){
    //     setTimeout(()=> {closeSetupProvider()}, 1500)
    // }
  }

  renderStateMessage = () => {
    const { current } = this.state;

    switch (current) {
      case 0:
        return (
          <Row>
            Install metamask plugin for Google Chrome or Firefox{" "}
            <a
              target="_blank"
              rel="noopener noreferrer"
              href="https://metamask.io/"
              className="provider__link"
            >
              (https://metamask.io/)
            </a>
          </Row>
        );
      case 1:
        return (
          <React.Fragment>
            Login to your existing Metamask wallet or setup one
          </React.Fragment>
        );
      case 2:
        return (
          <React.Fragment>
            Verify your wallet ownership by approving the Metamask popup
          </React.Fragment>
        );
      // case 3:
      //   return <React.Fragment>You can work with our system</React.Fragment>;
      default:
        return null;
    }
  };

  closeModal = () => {
    const { closeSetupProvider } = this.props;
    this.intervals.map(item => clearInterval(item));
    this.intervals = [];
    closeSetupProvider();
  };

  render() {
    const {
      current,
      provider: { installed, loggedIn, allowanceReceived, connection }
    } = this.state;

    const {
      blockchain: { setupProviderOpened },
      closeSetupProvider
    } = this.props;

    return (
      <Modal
        visible={setupProviderOpened}
        onCancel={() => this.closeModal()}
        closable={false}
        footer={null}
        className={
          (installed, loggedIn, allowanceReceived, connection)
            ? "modal modal_blockchain-provider modal_success"
            : "modal modal_blockchain-provider"
        }
      >
        <React.Fragment>
          <Row className={"card_row"}>
            <Col>
              <Steps size="small" current={current}>
                <Step
                  title={"Wallet installation"}
                  status={installed ? "finish" : "process"}
                />
                <Step
                  title="Connection"
                  status={loggedIn ? "finish" : "wait"}
                />
                <Step
                  title="Verification"
                  status={connection ? "finish" : "wait"}
                />
                <Step
                  title={"Approved"}
                  status={allowanceReceived ? "finish" : "wait"}
                />
              </Steps>
            </Col>
          </Row>
          <Row className={"card_row"}>{this.renderStateMessage()}</Row>
          <Row>
            <Col span={"12"} offset={"11"}>
              <Progress type="circle" percent={this.getPercent()} />
            </Col>
          </Row>
        </React.Fragment>
      </Modal>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    user: state.user,
    blockchain: state.blockchain
  };
};

const mapDispatchToProps = dispatch => {
  return {
    isMetamaskInstalled: () => {
      dispatch(isMetamaskInstalled());
    },
    isMetamaskLoggedIn: () => {
      dispatch(isMetamaskLoggedIn());
    },
    enableMetamask: () => {
      dispatch(enableMetamask());
    },
    checkAllowance: account => {
      dispatch(checkAllowance(account));
    },
    closeSetupProvider: () => {
      return dispatch(closeSetupProvider());
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BlockchainProviderSettings);
