import React from "react";
import { Card } from "antd";

function ChartCard({ ...props }) {
  const {
    chart,
    title,
    text,
  } = props;

  const cardStyle = {
    backgroundColor: "#0074a6"
  };

  const headStyle = {
    backgroundColor: "#fff"
  };

  return (
    <Card
      title={title}
      style={cardStyle}
      headStyle={headStyle}
    >
      {chart}
      <span>
        {text}
      </span>
    </Card>
  );
}

ChartCard.defaultProps = {
  statIconColor: "gray",
  chartColor: "purple"
};

export default ChartCard;
