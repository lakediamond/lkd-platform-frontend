import React from "react";
import { Card } from "antd";

const StatsCard = ({ ...props }) => {
  const { title, description } = props;
  return (
    <Card title={title} bordered={false}>
      <span className={"card_description"}>{description}</span>
    </Card>
  );
};

export default StatsCard;
