import React from "react";
import { Card, Col, Row } from "antd";

const MetamaskNotExistCard = () => {
  return (
    <Row className="container_row" gutter={48}>
      <Col span={8}>
        <Card>
          <Row>
            Please install metamask app to be able to work with functionality
            our platform offers:{" "}
            <a href="https://chrome.google.com/webstore/detail/metamask/nkbihfbeogaeaoehlefnkodbefgpgknn">
              Chrome
            </a>,
            {" "}
            <a href="https://addons.mozilla.org/ru/firefox/addon/ether-metamask/">
              Firefox
            </a>
            {" "}
            or
            {" "}
            <a href="https://addons.opera.com/en/extensions/details/metamask/">
              Opera
            </a>
          </Row>
        </Card>
      </Col>
    </Row>
  );
};

export default MetamaskNotExistCard;
