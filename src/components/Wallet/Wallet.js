import React from "react";
import { getETHBalance, getTokenBalance, get } from "redux/actions";
import { connect } from "react-redux";
import { SetupBlockchainProvider, ChooseBlockchainProvider } from "components";
import { GET_ETH_EXCHANGE_RATE_WALLET } from "redux/constants";
import style from "./index.css";

class Wallet extends React.Component {
  componentWillUnmount() {
    this.interval.map(index => clearInterval(index));
  }

  componentDidMount() {
    this.interval = [];
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    const {
      blockchain: { tokenBalance, metaMaskAccount, ethBalance },
      getETHBalance,
      getTokenBalance,
      open_alert,
      get
    } = this.props;

    //TODO quick fix to get balance
    if (prevProps.blockchain.metaMaskAccount === null && metaMaskAccount) {
      this.getBalances(metaMaskAccount);
      if (this.interval.length <= 2) {
        this.interval.push(
          setInterval(() => {
            this.getBalances(metaMaskAccount);
          }, 7500)
        );
      }
    }

    if (prevProps.blockchain.ethBalance != ethBalance) {
      this.props.get(
        GET_ETH_EXCHANGE_RATE_WALLET,
        null,
        `ethAmount=${ethBalance}`
      );
    }

    if (prevProps.open_alert !== open_alert && open_alert) {
      this.interval.map(index => clearInterval(index));
    }
  }

  getBalances = metaMaskAccount => {
    const { getETHBalance, getTokenBalance } = this.props;
    getETHBalance(metaMaskAccount);
    getTokenBalance(metaMaskAccount);
  };

  renderContent = () => {
    const {
      blockchain: {
        tokenBalance,
        ethBalance,
        metaMaskAccount,
        isInstalled,
        isLoggedIn,
        isEnabled,
        allowance
      },
      user: { blockchain_connection_type },
      exchangeRate: { wallet_converted_amount },
    } = this.props;

    if (
      (!!!isInstalled || !!!isLoggedIn || !!!isEnabled || !!!allowance) &&
      !!blockchain_connection_type
    ) {
      return (
        <div className="wallet">
          <div className="wallet__title">
            <SetupBlockchainProvider type={"full"} />
          </div>
        </div>
      );
    }

    if (!!!blockchain_connection_type) {
      return (
        <div className="wallet">
          <div className="wallet__title">
            <ChooseBlockchainProvider />
          </div>
        </div>
      );
    }

    return (
      <div className="wallet">
        <div className="wallet__title">MY BALANCE</div>
        <div className="wallet__balance-container">
          <span className={"wallet__token-ballance"}>{tokenBalance} LKD </span>
          <div className="wallet__currencies">
            <div className={"wallet__currency"}>
              {Number(ethBalance).toFixed(3)} ETH
            </div>
            {wallet_converted_amount && (
              <div className={"wallet__currency"}>
                {wallet_converted_amount} CHF
              </div>
            )}
          </div>
        </div>
      </div>
    );
  };

  render() {
    return <React.Fragment>{this.renderContent()}</React.Fragment>;
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    blockchain: state.blockchain,
    user: state.user,
    open_alert: state.open_alert,
    exchangeRate: state.exchangeRate
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getETHBalance: account => {
      return dispatch(getETHBalance(account));
    },
    getTokenBalance: account => {
      return dispatch(getTokenBalance(account));
    },
    get: (type, body, param) => {
      dispatch(get(type, body, param));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Wallet);
