import React from "react";

import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
  Label,
  ReferenceLine,
  Brush
} from "recharts";

import { Card, Row, Col, Select } from "antd";

import style from "./index.css";
import { get, setEstimationEthereumPrice } from "redux/actions";
import { GET_CHART_DATA } from "redux/constants";
import { connect } from "react-redux";
import { divideValueFor100 } from "services";

const Option = Select.Option;

const CustomTooltip = ({ active, payload, label }) => {
  if (active) {
    return (
      <div className="custom-tooltip">
        <p className="label">{`Price range: ${label}`}</p>
        <p className="label">{`Past completed orders: ${
          payload[0].value
        } LKD`}</p>
        <p className="label">{`Open proposals: ${payload[1].value} LKD`}</p>
      </div>
    );
  }

  return null;
};

class Chart extends React.Component {
  
  // componentDidMount() {
    // this.props.get(GET_CHART_DATA, null, 90);
  // }

  handleSelectChange = value => {
    this.props.get(GET_CHART_DATA, null, value);
  };

  formatLable = (value, entry) => {
    return (
      <span>
        {value === "proposalsTokenAmount"
          ? "Open proposals"
          : "Past completed orders"}
      </span>
    );
  };

  handleBarClick = e => {
    const { setEstimationEthereumPrice } = this.props;

    if (!!!e) {
      return false;
    }
    
    return setEstimationEthereumPrice(
      e.activePayload[0].payload.name.split("-")[1]
    );
  };

  render() {
    const {
      chartData: { rows, average_price_conversion }
    } = this.props;

    const data = rows
      ? rows.map(item => {
          return {
            name: `${item["minValue"]}-${item["maxValue"]}`,
            proposalsTokenAmount: `${item["proposalsTokenAmount"]}`,
            ordersTokenAmount: `${item["ordersTokenAmount"]}`
          };
        })
      : [];

    // data.push(
    //   {
    //     name: `0.3-0.4`,
    //     proposalsTokenAmount: `150`,
    //     ordersTokenAmount: `102`,
    //   },
    //   {
    //     name: `0.5-0.6`,
    //     proposalsTokenAmount: `130`,
    //     ordersTokenAmount: `99`,
    //   },
    //   {
    //     name: `0.6-0.8`,
    //     proposalsTokenAmount: `80`,
    //     ordersTokenAmount: `10`,
    //   },
    //   {
    //     name: `0.9-1`,
    //     proposalsTokenAmount: `2000`,
    //     ordersTokenAmount: `1000`,
    //   },
    //   {
    //     name: `1.1-1.2`,
    //     proposalsTokenAmount: `520`,
    //     ordersTokenAmount: `500`,
    //   },
    //   {
    //     name: `1.3-1.4`,
    //     proposalsTokenAmount: `185`,
    //     ordersTokenAmount: `102`,
    //   },
    //   {
    //     name: `1.5-1.6`,
    //     proposalsTokenAmount: `185`,
    //     ordersTokenAmount: `99`,
    //   },
    //   {
    //     name: `1.7-1.8`,
    //     proposalsTokenAmount: `85`,
    //     ordersTokenAmount: `10`,
    //   },
    //   {
    //     name: `1.9-2.0`,
    //     proposalsTokenAmount: `20000`,
    //     ordersTokenAmount: `10000`,
    //   },
    //   {
    //     name: `2.1-2.2`,
    //     proposalsTokenAmount: `1200`,
    //     ordersTokenAmount: `500`,
    //   },
    //   {
    //     name: `2.3-2.4`,
    //     proposalsTokenAmount: `850`,
    //     ordersTokenAmount: `102`,
    //   },
    //   {
    //     name: `2.5-2.6`,
    //     proposalsTokenAmount: `320`,
    //     ordersTokenAmount: `99`,
    //   },
    //   {
    //     name: `2.6-2.7`,
    //     proposalsTokenAmount: `80`,
    //     ordersTokenAmount: `10`,
    //   },
    //   {
    //     name: `2.9-3`,
    //     proposalsTokenAmount: `20000`,
    //     ordersTokenAmount: `10000`,
    //   },
    //   {
    //     name: `3.1-3.2`,
    //     proposalsTokenAmount: `720`,
    //     ordersTokenAmount: `500`,
    //   },
    //   {
    //     name: `3.3-3.4`,
    //     proposalsTokenAmount: `85`,
    //     ordersTokenAmount: `102`,
    //   },
    //   {
    //     name: `3.5-3.6`,
    //     proposalsTokenAmount: `1085`,
    //     ordersTokenAmount: `99`,
    //   },
    //   {
    //     name: `3.6-3.8`,
    //     proposalsTokenAmount: `80`,
    //     ordersTokenAmount: `10`,
    //   },
    //   {
    //     name: `3.9-4`,
    //     proposalsTokenAmount: `20000`,
    //     ordersTokenAmount: `10000`,
    //   },
    //   {
    //     name: `4.1-4.2`,
    //     proposalsTokenAmount: `620`,
    //     ordersTokenAmount: `500`,
    //   },
    //   {
    //     name: `4.3-4.4`,
    //     proposalsTokenAmount: `850`,
    //     ordersTokenAmount: `102`,
    //   },
    //   {
    //     name: `4.5-4.6`,
    //     proposalsTokenAmount: `850`,
    //     ordersTokenAmount: `99`,
    //   },
    //   {
    //     name: `4.6-4.8`,
    //     proposalsTokenAmount: `88`,
    //     ordersTokenAmount: `10`,
    //   },
    //   {
    //     name: `4.9-5`,
    //     proposalsTokenAmount: `3222`,
    //     ordersTokenAmount: `1000`,
    //   },
    //   {
    //     name: `5.1-5.2`,
    //     proposalsTokenAmount: `660`,
    //     ordersTokenAmount: `500`,
    //   }
    // );

    // if (!!!rows || rows.length < 1) {
    //   return null;
    // }

    return (
      <Card bordered={false} className={"chart"}>
        <Row
          gutter={16}
          type="flex"
          justify="space-between"
          className="container_row"
        >
          <Col>
            <div className={"chart__title"}>
              Average token price conversion:{" "}
              {divideValueFor100(average_price_conversion)} CHF
            </div>
          </Col>
          <Col>
            <div className={"chart__title"}>
              <Select defaultValue="90" onChange={this.handleSelectChange}>
                <Option value="30">30 days</Option>
                <Option value="60">60 days</Option>
                <Option value="90">90 days</Option>
              </Select>
            </div>
          </Col>
        </Row>{" "}
        <div className={"chart__label chart__label_top"}>Volume (LKD)</div>
        <ResponsiveContainer width="90%" height={450}>
          <BarChart
            data={data}
            margin={{ top: 0, right: 0, left: 0, bottom: 5 }}
            onClick={this.handleBarClick}
          >
            <CartesianGrid strokeDasharray="3 3" />
            {data.length > 5 && (
              <Brush
                dataKey="name"
                height={15}
                stroke="#1a455b"
                y={425}
                startIndex={0}
                endIndex={4}
              />
            )}
            <XAxis dataKey="name" />
            <YAxis />
            <Tooltip content={CustomTooltip} />
            <Legend formatter={this.formatLable} />
            <Bar
              dataKey="ordersTokenAmount"
              stackId="a"
              fill="#ef7d2f"
              barSize={80}
            />
            <Bar
              dataKey="proposalsTokenAmount"
              stackId="a"
              fill="#00c7b3"
              barSize={80}
            />
          </BarChart>
        </ResponsiveContainer>
        <div className={"chart__label chart__label_bottom"}>
          Token price (CHF)
        </div>
      </Card>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    chartData: state.chartData
  };
};

const mapDispatchToProps = dispatch => {
  return {
    get: (type, body, param) => {
      dispatch(get(type, body, param));
    },
    setEstimationEthereumPrice: value => {
      dispatch(setEstimationEthereumPrice(value));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Chart);
