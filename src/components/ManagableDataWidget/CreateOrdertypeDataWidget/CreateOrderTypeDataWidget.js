import React from "react";
import PropTypes from "prop-types";
import { DIGIT_REG_EXP, LETTERS_EXP_WITH_SPECSYMBOLS } from "redux/constants";
import { Button, Col, Form, Input, Row, Card } from "antd";

const FormItem = Form.Item;

class CreateOrderTypeDataWidget extends React.Component {
  static propTypes = {
    action: PropTypes.func.isRequired,
    blocked: PropTypes.bool.isRequired
  };

  handleClickReset = () => {
    this.props.form.resetFields();
  };

  handleSubmitForm = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.props.action(
          values["orderTypeIdId"],
          values["orderTypeDescription"]
        );
        this.props.form.resetFields();
      }
    });
  };

  render() {
    const { form: { getFieldDecorator }, blocked } = this.props;

    return (
      <Card title={"Create new order type"}>
        <Form onSubmit={e => this.handleSubmitForm(e)}>
          <Row gutter={8} className="card_row">
            <Col span={24}>
              <FormItem label={"ID"}>
                {getFieldDecorator("orderTypeIdId", {
                  validateTrigger: "onBlur",
                  rules: [
                    {
                      required: true,
                      message: "Order type ID must be a number",
                      pattern: DIGIT_REG_EXP
                    }
                  ]
                })(<Input placeholder="0" disabled={blocked}/>)}
              </FormItem>
              <FormItem label={"Description"}>
                {getFieldDecorator("orderTypeDescription", {
                  validateTrigger: "onBlur",
                  rules: [
                    {
                      required: true,
                      message: "Order type name must be a string",
                      pattern: LETTERS_EXP_WITH_SPECSYMBOLS
                    }
                  ]
                })(<Input placeholder="Some value" disabled={blocked}/>)}
              </FormItem>
            </Col>
          </Row>
          <Row gutter={8} type="flex" justify="start">
            <Col lg={12} xl={8}>
              <Button type="primary" htmlType="submit" block disabled={blocked}>
                Confirm
              </Button>
            </Col>
            <Col lg={12} xl={8}>
              <Button
                type="danger"
                onClick={e => this.handleClickReset(e)}
                block
                disabled={blocked}
              >
                Decline
              </Button>
            </Col>
          </Row>
        </Form>
      </Card>
    );
  }
}

export default Form.create()(CreateOrderTypeDataWidget);
