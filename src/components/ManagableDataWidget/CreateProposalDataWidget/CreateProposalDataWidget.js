import React from "react";
import { Button, Col, Form, Input, Row, Card, message } from "antd";
import { DIGIT_REG_EXP_WITH_DECIMALS, DIGIT_REG_EXP } from "redux/constants";
import { multiplyBy100 } from "services";
import {
  SetupBlockchainProvider,
  ChooseBlockchainProvider,
  ManageInputValues
} from "components";
import { connect } from "react-redux";
import {
  user_token_expired,
  setInvestmentPlan,
  get,
  getAllowedTokensAmount
} from "redux/actions";
import { Request, startAuth, divideValueFor100 } from "services";
import {
  GET_TX_DATA,
  GET_PROPOSALS_QUEUE_INFO,
  GET_TIERS_INFO
} from "redux/constants";
import { NavLink } from "react-router-dom";
import style from "./index.css";

const FormItem = Form.Item;

class CreateProposalDataWidget extends React.Component {
  state = {
    upfront_queue_tokens_volume: null,
    upfront_queue_proposals_nb: null
  };

  initialState = JSON.parse(JSON.stringify(this.state));

  render() {
    const {
      form: { getFieldDecorator },
      user: {
        blockchain_connection_type,
        kyc_summary: { achieved }
      },
      blockchain: {
        isInstalled,
        isLoggedIn,
        allowance,
        isEnabled,
        estimationQueueTokensAmount,
        allowedTokensAmount
      },
      chartData: { last_price_conversion },
      setInvestmentPlan
    } = this.props;

    const { upfront_queue_proposals_nb } = this.state;

    const blockChainNotSetuped =
      !isInstalled ||
      !isLoggedIn ||
      !allowance ||
      (!isEnabled && blockchain_connection_type);

    return (
      <div
        className={
          !!!blockchain_connection_type ||
          blockChainNotSetuped ||
          !!!achieved ||
          allowedTokensAmount === 0
            ? "create-proposal data-widget_blocked"
            : "create-proposal"
        }
      >
        {blockChainNotSetuped ? (
          <SetupBlockchainProvider type={"middle"} />
        ) : null}
        {!!!blockchain_connection_type ? <ChooseBlockchainProvider /> : null}
        {this.renderErrorMessageKYC()}
        {this.renderAllowedTokensErrorMessage()}
        <Card className={"card_row"} bordered={false}>
          <Form
            onSubmit={e => {
              upfront_queue_proposals_nb === null
                ? this.handleEstimationForm(e)
                : this.resetState(e);
            }}
          >
            <FormItem label={"Amount"} className={"card_row"}>
              {getFieldDecorator("tokens_amount", {
                validateTrigger: "onBlur",
                initialValue: 100,
                rules: [
                  { message: "Please type tokens amount", required: true },
                  {
                    validator: this.validateTokensAmount,
                    validateTrigger: "onBlur"
                  }
                ]
              })(
                <Input
                  placeholder="0"
                  disabled={upfront_queue_proposals_nb !== null || !allowance}
                  addonAfter={
                    <ManageInputValues
                      actions={[
                        this.increaseTokenAmount,
                        this.decreaseTokenAmount
                      ]}
                      disabled={
                        upfront_queue_proposals_nb !== null || !allowance
                      }
                      currency={"LKD"}
                    />
                  }
                  className={"ant-input_with-manage-input"}
                />
              )}
            </FormItem>
            <FormItem className="card_row" label={"Token price"}>
              {getFieldDecorator("tokens_price", {
                validateTrigger: "onBlur",
                initialValue: divideValueFor100(last_price_conversion),
                rules: [
                  {
                    required: true,
                    message: "Please input token price",
                    pattern: DIGIT_REG_EXP_WITH_DECIMALS
                  }
                ]
              })(
                <Input
                  placeholder="0"
                  disabled={upfront_queue_proposals_nb !== null || !allowance}
                  addonAfter={
                    <ManageInputValues
                      actions={[
                        this.increaseTokenPrice,
                        this.decreaseTokenPrice
                      ]}
                      disabled={
                        upfront_queue_proposals_nb !== null || !allowance
                      }
                      currency={"CHF"}
                    />
                  }
                  className={"ant-input_with-manage-input"}
                />
              )}
            </FormItem>
            <Row>
              <Col span={24}>
                <Button
                  type={
                    upfront_queue_proposals_nb === null ? "primary" : "danger"
                  }
                  htmlType="submit"
                  block
                  disabled={
                    !allowance || !!!achieved || allowedTokensAmount === 0
                  }
                >
                  {upfront_queue_proposals_nb === null ? "Estimate" : "Reset"}
                </Button>
              </Col>
            </Row>
          </Form>
        </Card>

        {upfront_queue_proposals_nb && (
          <Card bordered={false}>
            <Row className={"card_row create-proposal__content"}>
              <div className={"create-proposal__title create-proposal__info"}>
                At this proposal price
              </div>
              <div className={"create-proposal__title"}>
                <div>A volume of</div>
                <div className={"create-proposal__info"}>
                  {" "}
                  {this.state.upfront_queue_tokens_volume} LKD
                </div>
                <div>is queuing before you</div>
              </div>

              <div className={"create-proposal__title"}>
                <div>Represented by</div>
                <div className={"create-proposal__info"}>
                  {this.state.upfront_queue_proposals_nb}
                </div>
                <div>{`proposal${
                  this.state.upfront_queue_proposals_nb > 1 ? "s" : ""
                }`}</div>
              </div>
            </Row>
            <Row>
              <Col>
                <Form onSubmit={e => this.handleSubmit(e)}>
                  <Button type={"primary"} htmlType="submit" block>
                    Create Proposal
                  </Button>
                </Form>
              </Col>
            </Row>
          </Card>
        )}
      </div>
    );
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    const {
      blockchain: { estimationEthereumPrice, metaMaskAccount, allowedTokensAmount },
      form: { setFieldsValue },
      get,
      history,
      user: {
        target_tier,
        kyc_summary: { achieved }
      },
      getAllowedTokensAmount
    } = this.props;

    //TODO Could to be much more cases?
    if (
      prevProps.blockchain.metaMaskAccount === null &&
      metaMaskAccount && !!achieved
    ) {
      getAllowedTokensAmount(metaMaskAccount);
    }
    
    if(prevProps.blockchain.allowedTokensAmount !== allowedTokensAmount){
      console.log("!!!allowedTokensAmount!!!!");
      console.log(allowedTokensAmount);
      console.log("!!!allowedTokensAmount!!!!");
    }

    if (
      prevProps.blockchain.estimationEthereumPrice != estimationEthereumPrice
    ) {
      setFieldsValue({ tokens_price: estimationEthereumPrice });
    }
    
    if (
      prevProps.user.target_tier === null &&
      prevProps.user.target_tier !== target_tier
    ) {
      get(GET_TIERS_INFO);
      history.push("/profile/kyc");
    }
  }
  
  componentDidMount() {
    const {
      blockchain: { metaMaskAccount },
      user: {
        kyc_summary: { achieved }
      },
      getAllowedTokensAmount
    } = this.props;
  
    if (metaMaskAccount && achieved) {
      getAllowedTokensAmount(metaMaskAccount);
    }
  }
  
  renderErrorMessageKYC = () => {
    const {
      user: {
        blockchain_connection_type,
        kyc_summary: { achieved },
        target_tier
      },
      blockchain: { isInstalled, isLoggedIn, allowance, isEnabled },
      setInvestmentPlan
    } = this.props;

    const blockChainNotSetuped =
      !isInstalled ||
      !isLoggedIn ||
      !allowance ||
      (!isEnabled && blockchain_connection_type);

    if (!!!achieved && !blockChainNotSetuped) {
      if (target_tier === null) {
        return (
          <span
            className={"kyc-error-message"}
            onClick={() => setInvestmentPlan(0)}
          >
            Proposals placement is not available until your KYC application is
            approved
          </span>
        );
      } else {
        return (
          <NavLink to={`/profile/kyc`} className={"kyc-error-message"}>
            Proposals placement is not available until your KYC application is
            approved
          </NavLink>
        );
      }
    }

    return null;
  };

  renderAllowedTokensErrorMessage = () => {
    const {
      user: {
        blockchain_connection_type,
        kyc_summary: { achieved },
        target_tier
      },
      blockchain: {
        isInstalled,
        isLoggedIn,
        allowance,
        isEnabled,
        allowedTokensAmount
      },
      setInvestmentPlan
    } = this.props;

    const blockChainNotSetuped =
      !isInstalled ||
      !isLoggedIn ||
      !allowance ||
      (!isEnabled && blockchain_connection_type);
    
    if (!!achieved && !blockChainNotSetuped && allowedTokensAmount === 0) {
      return (
        <span className={"kyc-error-message"}>
          Allowed tokens limit left is not enough to create proposal
        </span>
      );
    }

    return null;
  };

  validateTokensAmount = (rule, value, callback) => {
    const {
      blockchain: { tokenBalance, allowedTokensAmount }
    } = this.props;

    if (value) {
      if (Number(value) > Number(tokenBalance)) {
        callback(
          "Tokens balance is not enough to create proposal of given amount"
        );
      }

      if (!DIGIT_REG_EXP.test(value)) {
        callback("Tokens value must be without decimals");
      }

      if (Number(value) < 100) {
        callback("Proposal tokens amount should be at least 100 LKD");
      }

      if (Number(allowedTokensAmount) < Number(value)) {
        callback(
          "Allowed tokens limit left is not enough to create proposal of given amount"
        );
      }
    }
    callback();
  };

  handleEstimationForm = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll(async (err, values) => {
      if (!err) {
        const data = await Request(
          GET_PROPOSALS_QUEUE_INFO,
          null,
          multiplyBy100(values["tokens_price"])
        );

        if (!data || data.status === 400) {
          return message.error("Something went wrong");
        }

        if (data.status === 401) {
          return user_token_expired();
        }

        if (data.status === 200) {
          data.json().then(res => {
            this.setState({
              upfront_queue_tokens_volume: res["upfront_queue_tokens_volume"],
              upfront_queue_proposals_nb: res["upfront_queue_proposals_nb"]
            });
          });
        }
      }
    });
  };

  resetState = e => {
    e.preventDefault();
    this.setState(this.initialState);
  };

  handleSubmit = e => {
    e.preventDefault();

    const { user_token_expired } = this.props;

    this.props.form.validateFieldsAndScroll(async (err, values) => {
      if (!err) {
        const price = multiplyBy100(values["tokens_price"]);
        const data = await Request(GET_TX_DATA, null, price);

        if (!data || data.status === 400) {
          return message.error("Something went wrong");
        }

        if (data.status === 401) {
          return user_token_expired();
        }

        if (data.status === 200) {
          data.json().then(res => {
            this.createProposalInContract(
              price,
              values["tokens_amount"],
              res["startPosition"]
            );
          });
        }
      }
    });
  };

  // handleClickReset = e => {
  //   e.preventDefault();
  //   this.props.form.resetFields();
  // };

  createProposalInContract = (price_value, amount_value, startPosition) => {
    const { contractInstance } = this.props;

    contractInstance.createProposal(
      price_value,
      amount_value,
      startPosition,
      (err, res) => {
        if (!err) {
          message.success("You successfully created a proposal");
          this.props.form.resetFields();
          return this.setState(this.initialState);
        }
        message.error("Ooops, something went wrong");
        return;
      }
    );
  };

  increaseTokenAmount = () => {
    const {
      form: { setFieldsValue, getFieldsValue },
      blockchain: { enabled }
    } = this.props;

    const { upfront_queue_proposals_nb } = this.state;

    if (upfront_queue_proposals_nb !== null || !enabled) {
      return false;
    }

    const value = getFieldsValue(["tokens_amount"]);
    setFieldsValue({ tokens_amount: Number(value["tokens_amount"]) + 10 });
  };

  decreaseTokenAmount = () => {
    const {
      form: { setFieldsValue, getFieldsValue },
      blockchain: { enabled }
    } = this.props;

    const { upfront_queue_proposals_nb } = this.state;

    if (upfront_queue_proposals_nb !== null || !enabled) {
      return false;
    }

    const value = getFieldsValue(["tokens_amount"]);
    if (value["tokens_amount"] >= 110) {
      setFieldsValue({ tokens_amount: Number(value["tokens_amount"]) - 10 });
    }
  };

  increaseTokenPrice = () => {
    const {
      form: { setFieldsValue, getFieldsValue },
      blockchain: { enabled }
    } = this.props;

    const { upfront_queue_proposals_nb } = this.state;

    if (upfront_queue_proposals_nb !== null || !enabled) {
      return false;
    }

    const value = getFieldsValue(["tokens_price"]);
    setFieldsValue({
      tokens_price: (Number(value["tokens_price"]) + 0.5).toPrecision(3)
    });
  };

  decreaseTokenPrice = () => {
    const {
      form: { setFieldsValue, getFieldsValue },
      blockchain: { enabled }
    } = this.props;

    const { upfront_queue_proposals_nb } = this.state;

    if (upfront_queue_proposals_nb !== null || !enabled) {
      return false;
    }

    const value = getFieldsValue(["tokens_price"]);

    if (value["tokens_price"] >= 0.5) {
      setFieldsValue({
        tokens_price: (Number(value["tokens_price"]) - 0.5).toPrecision(3)
      });
    }
  };
}

const mapStateToProps = (state, ownProps) => {
  return {
    user: state.user,
    blockchain: state.blockchain,
    chartData: state.chartData
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setInvestmentPlan: data => {
      dispatch(setInvestmentPlan(data));
    },
    user_token_expired: () => {
      dispatch(user_token_expired());
    },
    getAllowedTokensAmount: metaMaskAccount => {
      dispatch(getAllowedTokensAmount(metaMaskAccount));
    },
    get: (type, body, param) => {
      dispatch(get(type, body, param));
    }
  };
};

const CreateProposalDataWidgetComponent = Form.create()(
  CreateProposalDataWidget
);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CreateProposalDataWidgetComponent);
