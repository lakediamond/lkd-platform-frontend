import React from "react";
import PropTypes from "prop-types";
import { Row, Col, Input, Button, Form, Tag, Card } from "antd";
import { ETH_ADDRESS_EXP } from "redux/constants";

const FormItem = Form.Item;

class SubownerDataWidget extends React.Component {
  state = {
    dataCollection: []
  };

  static propTypes = {
    action: PropTypes.func.isRequired,
    blocked: PropTypes.bool.isRequired
  };

  showAddedCollection = () => {
    const { dataCollection } = this.state;
    return dataCollection.map((item, index) => {
      return <Tag key={index}>{item}</Tag>;
    });
  };

  handleClickAddDataToCollection = e => {
    e.preventDefault();

    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.setState(prevState => ({
          dataCollection: [...prevState.dataCollection, values["hash"]]
        }));
        this.props.form.resetFields();
      }
    });
  };

  handleClickReset = e => {
    e.preventDefault();
    this.setState(prevState => ({
      dataCollection: []
    }));
    this.props.form.resetFields();
  };

  handleSubmitForm = e => {
    e.preventDefault();
    this.props.action(this.state.dataCollection);
    this.props.form.resetFields();
    return this.setState(prevState => ({
      dataCollection: []
    }));
  };

  render() {
    const { form: { getFieldDecorator }, blocked } = this.props;

    return (
      <Card title={"Add new admin"}>
        <Form onSubmit={e => this.handleSubmitForm(e)}>
          <Row
            gutter={8}
            className="card_row"
            type="flex"
            justify="start"
            align="bottom"
          >
            <Col lg={22} xl={18}>
              <FormItem label={"Ethereum wallet"}>
                {getFieldDecorator("hash", {
                  rules: [
                    {
                      required: true,
                      pattern: ETH_ADDRESS_EXP,
                      message: "Please input your ETH account!"
                    }
                  ]
                })(
                  <Input placeholder="0x0000000000000000000000000000000000000000" disabled={blocked}/>
                )}
              </FormItem>
            </Col>
            <Col lg={2} xl={6}>
              <Button
                type="primary"
                shape="circle"
                icon="plus"
                className={"button_addSubownerDataCollection"}
                onClick={e => this.handleClickAddDataToCollection(e)}
                disabled={this.state.dataCollection.length === 5 || blocked}
              />
            </Col>
          </Row>
          <Row className="card_row">{this.showAddedCollection()}</Row>
          <Row gutter={8} type="flex" justify="start">
            <Col lg={12} xl={6}>
              <Button
                type="primary"
                htmlType="submit"
                block
                disabled={
                  this.state.dataCollection.length < 1 ||
                  this.state.dataCollection.length > 5 || blocked
                }
              >
                Confirm
              </Button>
            </Col>
            <Col lg={12} xl={6}>
              <Button
                type="danger"
                onClick={e => this.handleClickReset(e)}
                block
                disabled={blocked}
              >
                Decline
              </Button>
            </Col>
          </Row>
        </Form>
      </Card>
    );
  }
}

export default Form.create()(SubownerDataWidget);
