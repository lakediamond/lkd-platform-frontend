import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import {
  Request,
  decreaseValueForBlockchain,
  increaseValueForBlockchain,
  getEtherAmount,
  multiplyBy100
} from "services";

import { Button, Col, Form, Input, Row, Card, Select, message } from "antd";

import {
  GET_ORDER_TYPES,
  GET_ETHER_AMOUNT,
  DIGIT_REG_EXP,
  LETTERS_EXP,
  SPECSYMBOLS_NOT_ALLOWED,
  GET_ETH_EXCHANGE_RATE,
  DIGIT_REG_EXP_WITH_DECIMALS
} from "redux/constants";

import { get } from "redux/actions";

import { SetupBlockchainProvider, ChooseBlockchainProvider } from "components";

const Option = Select.Option;
const FormItem = Form.Item;

class CreateOrderDataWidget extends React.Component {
  state = {
    ordersTypeData: null
  };
  
  intervals = [];
  
  componentDidMount() {
    const { get } = this.props;
    get(GET_ORDER_TYPES);
    get(GET_ETH_EXCHANGE_RATE, null, "ethAmount=1&riskIncluded=true");
    
    if(this.intervals.length < 2){
      this.intervals.push(
        setInterval(() => {
          this.props.get(GET_ETH_EXCHANGE_RATE, null, "ethAmount=1&riskIncluded=true");
        }, 30000)
      );
    }
  }
  
  
  componentWillUnmount() {
    this.intervals.map(item => clearInterval(item));
    this.intervals = [];
  }
  
  handleSubmit = e => {
    const { form } = this.props;

    e.preventDefault();

    form.validateFields((err, values) => {
      if (!err) {
        // const price = Number(values["price_value"]) * 100;

        const price = this.props.form.getFieldValue("price_value");

        const token_amount = this.props.form.getFieldValue(
          "token_amount_value"
        );

        const ether_price_rate = this.props.form.getFieldValue(
          "ether_price_rate"
        );

        const etherAmount = getEtherAmount(
          price,
          token_amount,
          ether_price_rate
        );

        // const ether_price_rate = Number(values['ether_price_rate'])*100;

        this.createOrderInContract(
          values["type_value"],
          values["metadata_value"],
          multiplyBy100(price),
          values["token_amount_value"],
          multiplyBy100(ether_price_rate),
          etherAmount
        );
      }
    });
  };

  handleClickReset = e => {
    e.preventDefault();
    this.props.form.resetFields();
  };

  handleBlur = async e => {
    const fieldsAreNotEmpty = () => {
      return (
        this.props.form.getFieldValue("price_value") > 0 &&
        this.props.form.getFieldValue("token_amount_value") > 0 &&
        this.props.form.getFieldValue("ether_price_rate") > 0
      );
    };

    if (e.target.value.length < 1) {
      return false;
    }

    if (fieldsAreNotEmpty()) {
      const price = this.props.form.getFieldValue("price_value");

      const token_amount = this.props.form.getFieldValue("token_amount_value");

      const ether_price_rate = this.props.form.getFieldValue(
        "ether_price_rate"
      );
      const etherAmount = getEtherAmount(price, token_amount, ether_price_rate);

      this.props.form.setFieldsValue({
        ["ether_amount_value"]: decreaseValueForBlockchain(etherAmount)
      });
    }
    return false;
  };

  createOrderInContract = (
    type_value,
    metadata_value,
    price_value,
    token_amount_value,
    ether_price_rate,
    ether_amount_value
  ) => {
    const {
      blockchain: { ioContractInstance }
    } = this.props;

    ioContractInstance.createOrder(
      type_value,
      metadata_value,
      price_value,
      token_amount_value,
      ether_price_rate,
      { value: ether_amount_value },
      (err, res) => {
        if (!err) {
          message.success("You successfully added new order to the contract");
          this.props.form.resetFields();
          return;
        }
        message.error("Ooops, something went wrong");
        return;
      }
    );
  };

  renderOptionsOrderTypes = () => {
    const {
      ordersList: { types }
    } = this.props;
    return types != null
      ? types.map((item, index) => {
          return (
            <Option value={item["ID"]} key={index}>
              {item["Name"]}
            </Option>
          );
        })
      : null;
  };

  checkPriceRate = (rule, value, callback) => {
    const {exchangeRate: { deviation_allowed_min, deviation_allowed_max }} = this.props;
    if (value) {
      if (Number(value) < Number(deviation_allowed_min) || Number(value) > Number(deviation_allowed_max)) {
        callback(`ETH exchange rate should be within range from ${deviation_allowed_min} to ${deviation_allowed_max}`);
      }
  
      if(!DIGIT_REG_EXP_WITH_DECIMALS.test(value)){
        callback("Exchange rate must be a number");
      }
    }
    callback();
  };

  render() {
    const {
      form: { getFieldDecorator },
      user: { blockchain_connection_type },
      blockchain: { isInstalled, isLoggedIn, allowance, isEnabled },
      exchangeRate: {
        converted_amount,
        deviation_allowed_min,
        deviation_allowed_max
      },
      title
    } = this.props;

    const blockChainNotSetuped =
      !isInstalled ||
      !isLoggedIn ||
      !allowance ||
      (!isEnabled && blockchain_connection_type);

    return (
      <React.Fragment>
        {title && <div className={"tables-container__title"}>{title}</div>}
      <div
        className={
          !!!blockchain_connection_type || blockChainNotSetuped
            ? "data-widget_blocked"
            : null
        }
      >
        {blockChainNotSetuped ? (
          <SetupBlockchainProvider type={"full"} />
        ) : null}
        {!!!blockchain_connection_type ? <ChooseBlockchainProvider /> : null}
        <Card>
          <Row>
            <Col>
              <Form onSubmit={e => this.handleSubmit(e)}>
                <FormItem label={"Select a type"}>
                  {getFieldDecorator("type_value", {
                    rules: [
                      { required: true, message: "Please select a type!" }
                    ]
                  })(
                    <Select
                      disabled={
                        !!!blockchain_connection_type || blockChainNotSetuped
                      }
                    >
                      {this.renderOptionsOrderTypes()}
                    </Select>
                  )}
                </FormItem>
                <FormItem label={"Metadata"}>
                  {getFieldDecorator("metadata_value", {
                    validateTrigger: "onBlur",
                    rules: [
                      {
                        required: true,
                        pattern: SPECSYMBOLS_NOT_ALLOWED,
                        message: "Please input metadata"
                      }
                    ]
                  })(
                    <Input
                      disabled={
                        !!!blockchain_connection_type || blockChainNotSetuped
                      }
                    />
                  )}
                </FormItem>
                <FormItem label={"Price"}>
                  {getFieldDecorator("price_value", {
                    validateTrigger: "onBlur",
                    rules: [
                      {
                        required: true,
                        message: "Please type price value",
                        pattern: DIGIT_REG_EXP_WITH_DECIMALS
                      }
                    ]
                  })(
                    <Input
                      placeholder="0"
                      name={"price_value"}
                      onBlur={e => this.handleBlur(e)}
                      disabled={
                        !!!blockchain_connection_type || blockChainNotSetuped
                      }
                    />
                  )}
                </FormItem>
                <FormItem label={"Token amount"}>
                  {getFieldDecorator("token_amount_value", {
                    validateTrigger: "onBlur",
                    rules: [
                      {
                        required: true,
                        message: "Please type token amount",
                        pattern: DIGIT_REG_EXP
                      }
                    ]
                  })(
                    <Input
                      placeholder="0"
                      name={"token_amount_value"}
                      onBlur={e => this.handleBlur(e)}
                      disabled={
                        !!!blockchain_connection_type || blockChainNotSetuped
                      }
                    />
                  )}
                </FormItem>
                <FormItem label={`Ethereum exchange rate | ${converted_amount}`}>
                  {getFieldDecorator("ether_price_rate", {
                    rules: [
                      { message: "Please type ethereum exchange rate", required: true },
                      {
                        validator: this.checkPriceRate,
                        validateTrigger: "onBlur",
                      }
                    ]
                  })(
                    <Input placeholder="0" onBlur={e => this.handleBlur(e)} />
                  )}
                </FormItem>
                <FormItem label={"Ethereum amount"} className="card_row">
                  {getFieldDecorator("ether_amount_value", {
                    rules: [
                      {
                        required: true,
                        message: "Please type ether amount",
                        pattern: DIGIT_REG_EXP_WITH_DECIMALS
                      }
                    ]
                  })(
                    <Input
                      name="ether_amount_value"
                      placeholder="0"
                      disabled={
                        !!!blockchain_connection_type || blockChainNotSetuped
                      }
                    />
                  )}
                </FormItem>
                <Row gutter={16} className="card_row">
                  <Col span={12}>
                    <Button
                      type="primary"
                      htmlType="submit"
                      block
                      disabled={
                        !!!blockchain_connection_type || blockChainNotSetuped
                      }
                    >
                      Create
                    </Button>
                  </Col>

                  <Col span={12}>
                    <Button
                      type="danger"
                      block
                      onClick={e => this.handleClickReset(e)}
                      disabled={
                        !!!blockchain_connection_type || blockChainNotSetuped
                      }
                    >
                      Decline
                    </Button>
                  </Col>
                </Row>
              </Form>
            </Col>
          </Row>
        </Card>
      </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    user: state.user,
    blockchain: state.blockchain,
    ordersList: state.ordersList,
    exchangeRate: state.exchangeRate.exchangeRate
  };
};

const mapDispatchToProps = dispatch => {
  return {
    get: (type, body, param) => {
      dispatch(get(type, body, param));
    }
  }
};

const CreateOrderComponent = Form.create()(CreateOrderDataWidget);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CreateOrderComponent);
