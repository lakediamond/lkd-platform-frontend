import React from "react";
import { connect } from "react-redux";
import {
  postPhoneNumber,
  postCode,
  // getPhoneInfo,
  resendPhoneNumber,
  deletePhoneNumber,
  get
} from "redux/actions";

import {
  Button,
  Col,
  Form,
  Input,
  Row,
  Card,
  Select,
  Icon,
  Tooltip
} from "antd";

import { CountDown } from "ant-design-pro";
import { parsePhoneNumberFromString, AsYouType } from "libphonenumber-js/max";

import { DIGIT_REG_EXP, GET_PHONE_INFO } from "redux/constants";

const Option = Select.Option;
const FormItem = Form.Item;

//TODO handle error with too many tries

class PhoneDataWidget extends React.Component {
  constructor(props) {
    super(props);
    this.phoneInput = React.createRef();
    this.countriesSelectInput = React.createRef();
    //TODO for now it's not needed
    this.sendNumberButton = React.createRef();
  }

  state = {
    countryCode: null,
    phoneNumber: null
  };

  initialState = this.state;

  componentDidMount() {
    this.props.get(GET_PHONE_INFO);
    // this.props.getPhoneInfo();
  }

  //TODO will improve it
  componentDidUpdate(prevProps, prevState, snapshot) {
    // if (prevState.country_code !== this.state.countryCode &&) {
    //   this.phoneInput.current.inputElement.focus();
    // }
    // const {
    //   phone: { phoneInfo }
    // } = this.props;
    // if(prevProps.phone.phoneInfo !== phoneInfo && !!phoneInfo && prevProps.phone.phoneInfo === null){
    //
    // }
  }

  fetchPhoneNumber = e => {
    e.preventDefault();
    // this.setState(this.initialState);
    const country_code = this.props.form.getFieldValue("prefix").split("|")[0];
    // const tel_code = this.props.form.getFieldValue("prefix").split("|")[1];
    // const phone_number = `+${tel_code}${this.props.form.getFieldValue(
    //   "phone_number"
    // )}`;
    const { phoneNumber } = this.state;
    const data = {
      phone: phoneNumber.replace(/\s/g, ""),
      country_code: country_code
    };
    this.props.postPhoneNumber(data);
    // this.setState(this.initialState);
  };

  fetchCode = e => {
    e.preventDefault();
    const code = this.props.form.getFieldValue("code_value");
    const data = { code: `${code}` };
    this.props.postCode(data);
  };

  handleChange = e => {
    const { countryCode } = this.state;

    if (e.target.selectionStart < countryCode.length) {
      e.preventDefault();
      e.target.selectionStart = e.target.value.length + 1;
    }

    if (e.target.value.length === 0) {
      e.target.value = countryCode;
    }

    const phone = parsePhoneNumberFromString(e.target.value);
    if (phone) {
      if (phone.isValid()) {
        e.target.blur();
      }
    }
    let value = new AsYouType().input(e.target.value);
    this.setState({ phoneNumber: value });
  };

  handleSelect = e => {
    this.setState({ countryCode: null, phoneNumber: null });
    this.setState({
      countryCode: `+${e.split("|")[1]}`,
      phoneNumber: `+${e.split("|")[1]}`
    });
  };

  handleSelectFocus = e => {
    document
      .querySelector(".countries-select-prefix")
      .classList.remove("ant-input_phone_failed");
    document
      .querySelector(".ant-input")
      .classList.remove("ant-input_phone_failed");
    document
      .querySelector(".countries-select-prefix")
      .classList.remove("ant-input_phone_success");
    document
      .querySelector(".ant-input")
      .classList.remove("ant-input_phone_success");
  };

  handleChangeSelect = e => {
    setTimeout(() => {
      this.phoneInput.current.focus();
    }, 300);
  };

  handleFocusPhone = e => {
    // e.target.classList.remove("ant-input_phone_failed");
    // const { countryCode } = this.state;
    // if (this.prefixInput.current) {
    //   const width = this.prefixInput.current.clientWidth;
    //   e.target.style.paddingLeft = `${width + 6}px`;
    // }
    // if(!!!countryCode){
    //   e.target.classList.remove("ant-input_phone_failed");
    //   document
    //     .querySelector(".countries-select-prefix")
    //     .classList.remove("ant-input_phone_success");
    // }
  };

  handleBlur = e => {
    const { countryCode } = this.state;
    const phone = parsePhoneNumberFromString(e.target.value);

    const setError = () => {
      e.target.classList.remove("ant-input_phone_success");
      e.target.classList.add("ant-input_phone_failed");
      document
        .querySelector(".countries-select-prefix")
        .classList.remove("ant-input_phone_success");
      document
        .querySelector(".countries-select-prefix")
        .classList.add("ant-input_phone_failed");
      this.setState({ phoneIsValid: false });
    };

    const setSuccess = () => {
      e.target.classList.remove("ant-input_phone_failed");
      e.target.classList.add("ant-input_phone_success");
      document
        .querySelector(".countries-select-prefix")
        .classList.remove("ant-input_phone_failed");
      document
        .querySelector(".countries-select-prefix")
        .classList.add("ant-input_phone_success");
      this.setState({ phoneIsValid: true });
    };

    if (!!!phone) {
      setError();
      return false;
    }

    if (phone) {
      phone.isValid() ? setSuccess() : setError();
    }
  };

  handleClickInput = e => {
    const { countryCode } = this.state;
    if (e.target.selectionStart < countryCode.length) {
      e.target.selectionStart = e.target.value.length;
    }
  };

  handleKeyPress = e => {
    const { countryCode } = this.state;

    if (
      e.target.value.length < countryCode.length ||
      e.target.selectionStart < countryCode.length
    ) {
      e.preventDefault();
      e.target.value = countryCode;
      e.target.selectionStart = e.target.value.length;
    }
  };

  changePhoneNumber = () => {
    const { deletePhoneNumber } = this.props;
    this.setState({
      countryCode: null,
      phoneNumber: null,
      phoneIsValid: false
    });
    this.phoneInput.current.value = null;
    deletePhoneNumber();
  };

  resendPhoneNumber = e => {
    e.preventDefault();
    this.setState(this.initialState);

    const {
      phone: {
        phoneInfo: { content_items }
      }
    } = this.props;

    const value = {
      phone: content_items[1]["value"],
      country_code: content_items[0]["value"]
    };

    this.props.resendPhoneNumber(value);
  };

  renderCountriesCollection = () => {
    const {
      phone: { phoneInfo }
    } = this.props;

    const countriesCollection = [];

    phoneInfo
      ? phoneInfo["content_items"][0]["data"].map(item => {
          countriesCollection.push(
            <Option
              value={`${item["country_code"]}|${item["phone_code"]}|${
                item["country"]
              }`}
              key={item["id"]}
            >
              {`${item["country"]}`}
            </Option>
          );
        })
      : null;

    return countriesCollection;
  };

  countriesSelect = () => {
    const {
      form: { getFieldDecorator },
      phone: { phoneInfo, phoneInfo: { status } },
    } = this.props;

    if (!phoneInfo || !!phoneInfo["content_items"][0]["value"]) {
      return null;
    }
    
    return getFieldDecorator("prefix", {
      initialValue: "Country code"
    })(
      <Select
        showSearch
        style={{ minWidth: 160, maxWidth: 160, overflow: "hidden" }}
        disabled={status === "VERIFIED"}
        onSelect={e => this.handleSelect(e)}
        className={"countries-select-prefix"}
        ref={this.countriesSelectInput}
        onFocus={e => this.handleSelectFocus(e)}
        onChange={e => this.handleChangeSelect(e)}
        // onChange{() => this.handleChangeCountry()}
      >
        {this.renderCountriesCollection()}
      </Select>
    );
  };

  renderPhoneInput = () => {
    const {
      phone: { phoneInfo, phoneInfo: { status } },
    } = this.props;

    if (!phoneInfo) {
      return null;
    }

    const { countryCode } = this.state;

    const renderValue = () => {
      if (!!phoneInfo["content_items"][1]["value"]) {
        return `${phoneInfo["content_items"][1]["value"]}`;
      }

      return this.state.phoneNumber;
    };

    return (
      <div className={"phone_input_group card_row"}>
        {this.countriesSelect()}
        <input
          className={
            !!phoneInfo["content_items"][1]["value"]
              ? "ant-input ant-input_filled ant-input-disabled"
              : "ant-input ant-input_phone"
          }
          disabled={!!!countryCode || !!phoneInfo["content_items"][1]["value"]}
          onChange={e => this.handleChange(e)}
          onFocus={e => this.handleFocusPhone(e)}
          defaultValue={renderValue()}
          value={this.state.phoneNumber}
          ref={this.phoneInput}
          onBlur={e => this.handleBlur(e)}
          onClick={e => this.handleClickInput(e)}
          onKeyDown={e => this.handleKeyPress(e)}
        />
      </div>
    );
  };

  renderPhoneNumberButton = () => {
    const {
      phone: {
        phoneInfo: { content_items }
      },
      form: { getFieldValue },
      fetch: { state }
    } = this.props;

    const { phoneIsValid } = this.state;

    const enabled = content_items[1]["enabled"];
    const enabled_at = content_items[1]["enabled_at"];
    const value = content_items[1]["value"];
    const status = content_items[1]["status"];

    if (enabled && !value && status !== "VERIFIED") {
      return (
        <Button
          onClick={e => this.fetchPhoneNumber(e)}
          disabled={!phoneIsValid}
          type={"primary"}
          ref={this.sendNumberButton}
        >
          {!state ? (
            "Send code to my phone (as a text message)"
          ) : (
            <Icon type="sync" spin style={{ color: "fff" }} />
          )}
        </Button>
      );
    }

    if (enabled && value && status !== "VERIFIED") {
      return (
        <Col>
          <Button
            onClick={e => this.resendPhoneNumber(e)}
            disabled={!enabled}
            type={"primary"}
            className={"card_row"}
          >
            {!state ? (
              "Resend text message with code"
            ) : (
              <Icon type="sync" spin style={{ color: "fff" }} />
            )}
          </Button>
          <Button
            onClick={() => this.changePhoneNumber()}
            disabled={!enabled}
            type={"danger"}
          >
            {!state ? (
              "Change phone number"
            ) : (
              <Icon type="sync" spin style={{ color: "fff" }} />
            )}
          </Button>
        </Col>
      );
    }

    if (
      !enabled &&
      value &&
      status !== "VERIFIED" &&
      enabled_at !== "0001-01-01T00:00:00Z"
    ) {
      return (
        <React.Fragment>
          Resend at{" "}
          <CountDown
            target={enabled_at}
            onEnd={() => this.props.get(GET_PHONE_INFO)}
          />
        </React.Fragment>
      );
    }
  };

  renderSendCodeFragment = () => {
    const {
      phone: {
        phoneInfo: { content_items }
      },
      form: { getFieldDecorator },
      fetch: { state }
    } = this.props;

    const enabled = content_items[1]["enabled"];
    const enabled_at = content_items[1]["enabled_at"];
    const value = content_items[1]["value"];
    const status = content_items[1]["status"];

    if (
      !enabled &&
      value &&
      status !== "VERIFIED" &&
      enabled_at !== "0001-01-01T00:00:00Z"
    ) {
      return (
        <React.Fragment>
          <Row>
            <Col lg={12} xl={12} className={"card_row"}>
              <FormItem label={"Please enter the code received on your phone"}>
                {getFieldDecorator("code_value", {
                  validateTrigger: "onBlur",
                  rules: [
                    {
                      required: true,
                      message: "Please type code from SMS"
                    }
                  ]
                })(
                  <Input
                    placeholder="******"
                    name={"price_value"}
                    maxLength={6}
                  />
                )}
              </FormItem>
            </Col>
          </Row>
          <Row>
            <Col lg={8} xl={12}>
              <FormItem>
                <Button onClick={e => this.fetchCode(e)} type={"primary"}>
                  {!state ? (
                    "Submit code"
                  ) : (
                    <Icon type="sync" spin style={{ color: "fff" }} />
                  )}
                </Button>
              </FormItem>
            </Col>
          </Row>
        </React.Fragment>
      );
    }

    if (status === "VERIFIED") {
      return (
        <React.Fragment>
          <Row gutter={24}>
            <Col offset={18}>
              <span className={"confirmed"}>
                <Icon type="check-circle" theme="filled" /> Your information was
                received!
              </span>
            </Col>
          </Row>
        </React.Fragment>
      );
    }

    return null;
  };

  render() {
    const {
      phone: { phoneInfo }
    } = this.props;

    return phoneInfo ? (
      <Card title={"Please validate your telephone number"}>
        <Form>
          <Row gutter={8} type="flex" align="start">
            <Col span={12}>{this.renderPhoneInput()}</Col>
            <Col span={8}>{this.renderPhoneNumberButton()}</Col>
          </Row>
          {this.renderSendCodeFragment()}
        </Form>
      </Card>
    ) : null;
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    phone: state.phone,
    fetch: state.fetch
  };
};

const mapDispatchToProps = dispatch => {
  return {
    postPhoneNumber: value => {
      dispatch(postPhoneNumber(value));
    },
    postCode: value => {
      dispatch(postCode(value));
    },
    // getPhoneInfo: () => {
    //   dispatch(getPhoneInfo());
    // },
    get: (type) => {
      dispatch(get(type));
    },
    resendPhoneNumber: value => {
      dispatch(resendPhoneNumber(value));
    },
    deletePhoneNumber: () => {
      dispatch(deletePhoneNumber());
    }
  };
};

const phoneComponent = Form.create()(PhoneDataWidget);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(phoneComponent);
