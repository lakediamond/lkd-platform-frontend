import React from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { Button, Icon, Upload, message, Row, Col, Skeleton } from "antd";
import {
  GET_GENERIC_UPLOAD_IMAGE_URL,
  CHECK_POSSIBILITY_TO_UPLOAD_PHOTO,
  GET_IDENTITY_SCANS,
  GET_TIERS_INFO
} from "redux/constants";

import { updateIdentityScansStatus, get } from "redux/actions";
import { Request } from "services";
import { Spinner } from "components";
import { TIERS_INFO_PATH } from "../../../../redux/constants";

class UploadDataWidget extends React.Component {
  static propTypes = {
    type: PropTypes.string.isRequired
  };

  state = {
    file: null,
    isUploaded: false,
    error: null
  };

  componentDidMount() {
    this.interval = [];
  }

  async componentDidUpdate(prevProps, prevState, snapshot) {
    const {
      type,
      identityScans,
      updateIdentityScansStatus,
      openedTab
    } = this.props;

    const fileName = `/${type}`;

    const status = identityScans.content_items[openedTab]["status"];
    let identityScansUploaded = identityScans.content_items.filter(
      item => item.status === "VALID" || item.status === "PROCESSED"
    );

    if (identityScansUploaded.length === 3) {
      this.props.get(GET_TIERS_INFO);
      return false;
    }

    if (!!!status || status === "INVALID") {
      const isPhotoExists = await Request(
        CHECK_POSSIBILITY_TO_UPLOAD_PHOTO,
        null,
        fileName
      );

      if (!isPhotoExists) {
        message.error("Network error");
        // console.log("ERROR networks!!!");
        // throw new Error("error");
      }

      if (isPhotoExists.status === 304 && status === "UPLOADED") {
        return updateIdentityScansStatus(openedTab, "UPLOADED");
      }

      // if (isPhotoExists.status === 204) {
      //   return true;
      // }
    }

    if (status === "UPLOADED" && this.interval.length < 2) {
      this.interval.push(
        setInterval(() => {
          this.props.get(GET_IDENTITY_SCANS);
        }, 7000)
      );
    }

    if (status === "UPLOADED" && prevProps.openedTab !== openedTab) {
      return this.props.get(GET_IDENTITY_SCANS);
    }

    if (status !== "UPLOADED" || prevProps.openedTab != openedTab) {
      this.interval.map(item => clearInterval(item));
      this.interval = [];
    }
  }

  componentWillUnmount() {
    this.interval.map(item => clearInterval(item));
    this.interval = [];
  }

  beforeUpload = async file => {
    const { type, updateIdentityScansStatus, order } = this.props;
    const filePath = `/${type}`;

    const rowFile = file.slice(0, file.size, file.type);
    const rowExt = file.name.split(".");
    const ext = rowExt[rowExt.length - 1];

    const isLt2M = file.size / 1024 / 1024 < 2;

    if (!isLt2M) {
      message.error("Image must be less than 2MB");
      this.setState({ error: true });
    }

    const newFile = new File([rowFile], `${type}.${ext}`, {
      type: rowFile.type
    });

    const pathForNewFileCreated = await Request(
      GET_GENERIC_UPLOAD_IMAGE_URL,
      newFile.name,
      filePath
    );

    if (!pathForNewFileCreated) {
      message.error("Network error");
    }

    if (pathForNewFileCreated.status === 200) {
      pathForNewFileCreated
        .json()
        .then(url => this.setState({ url: url["url"], file: newFile }));
    }

    return await false;
  };

  handleChange = () => {
    return false;
  };

  handleUpload = async () => {
    const { updateIdentityScansStatus, openedTab } = this.props;
    const { file } = this.state;

    this.setState({
      uploading: true
    });

    const config = {
      method: "PUT",
      headers: new Headers({ "Content-type": `${file.type}` }),
      body: file,
      mode: "cors"
    };

    const data = await fetch(this.state.url, config);

    if (!data) {
      message.error("This is a network error");
    }

    if (data.status === 200) {
      message.success("Your image successfully upload");
      updateIdentityScansStatus(openedTab, "UPLOADED");
    }

    this.setState({
      uploading: false,
      file: null
    });
  };

  handleRemove = () => {
    this.setState({ isUploaded: false, file: null, error: null });
  };

  renderUploadComponent = () => {
    const { file } = this.state;
    const { order, identityScans, openedTab } = this.props;
    const status = identityScans
      ? identityScans.content_items[openedTab]["status"]
      : "";

    if (status === "") {
      return (
        <Upload
          accept=".jpg, .png, .jpeg"
          name="avatar"
          listType="picture-card"
          beforeUpload={this.beforeUpload}
          onChange={this.handleChange}
          className="card_row"
          showUploadList={true}
          onRemove={() => this.handleRemove()}
        >
          {file ? null : <Icon type="plus" />}
        </Upload>
      );
    }

    if (status == "INVALID") {
      return (
        <Upload
          accept=".jpg, .png, .jpeg"
          name="avatar"
          listType="picture-card"
          beforeUpload={this.beforeUpload}
          onChange={this.handleChange}
          className="card_row"
          showUploadList={true}
          onRemove={() => this.handleRemove()}
        >
          {file ? null : <Icon type="plus" />}
        </Upload>
      );
    }

    return null;
  };

  renderTextStatus = () => {
    const { openedTab, identityScans, get } = this.props;
    const status = identityScans
      ? identityScans.content_items[openedTab]["status"]
      : null;

    if (status === "UPLOADED") {
      return (
        <React.Fragment>
          <div className={"card_row"}>
            Photo has been uploaded, please wait while we check it...
          </div>
          <Spinner type={"in_block"} />
        </React.Fragment>
      );
    }

    if (status === "INVALID") {
      return (
        <React.Fragment>
          <div className={"card_row"}>
            Your previous image was corrupted.Please try again
          </div>
        </React.Fragment>
      );
    }

    if (status === "VALID" || status === "PROCESSED") {
      return (
        <React.Fragment>
          <span className={"confirmed"}>
            <Icon type="check-circle" theme="filled" /> Upload successful!
          </span>
        </React.Fragment>
      );
    }

    return null;
  };

  renderButton = () => {
    const { uploading, file, error } = this.state;
    const { openedTab, identityScans } = this.props;
    const status = identityScans["content_items"][openedTab]["status"];

    if (status === "" || status === "INVALID") {
      return (
        <Row>
          <Col>
            <Button
              type="primary"
              onClick={this.handleUpload}
              disabled={!file || !!error}
              className="card_row"
            >
              {uploading ? "Uploading" : "Start Upload"}
            </Button>
          </Col>
        </Row>
      );
    }

    return null;
  };

  render() {
    const { openedTab, identityScans } = this.props;
    const status = identityScans
      ? identityScans.content_items[openedTab]["status"]
      : null;

    if (status === null) {
      return (
        <React.Fragment>
          <Skeleton active />
        </React.Fragment>
      );
    }

    return (
      <React.Fragment>
        <Row>
          <Col>
            {this.renderTextStatus()}
            {this.renderUploadComponent()}
          </Col>
        </Row>
        {this.renderButton()}
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    identityScans: state.identityScans
  };
};

const mapDispatchToProps = dispatch => {
  return {
    updateIdentityScansStatus: (order, value) => {
      dispatch(updateIdentityScansStatus(order, value));
    },
    get: type => {
      dispatch(get(type));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UploadDataWidget);
