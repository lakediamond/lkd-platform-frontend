import React from "react";
import { Tabs, Card, Popover, Tooltip } from "antd";
import { UploadDataWidget } from "components";
import { get } from "redux/actions";
import { GET_IDENTITY_SCANS } from "redux/constants";
import { connect } from "react-redux";

const TabPane = Tabs.TabPane;


//TODO added specific open tab prop

class DocumentDataWidget extends React.Component {
  state={
    openedTab: 0
  };
  
  componentDidMount() {
    this.props.get(GET_IDENTITY_SCANS);
  };
  
  handleChange = (e) => {
    this.setState({openedTab: e})
  };
  
  render() {
    // const { identityScans } = this.props;
    
    // const status = identityScans ? identityScans['content_items'].map(item => item['data']) : null;
    
    // statuses:  null UPLOADED INVALID VALID PROCESSED
    
    return (
      <Card title="Upload your identity document photos">
        <Tabs defaultActiveKey="0" onChange={e => this.handleChange(e)}>
          <TabPane
            tab={
              <Tooltip title={`"Passport cover" or "Identity card" picture side`}>
                ID Front
              </Tooltip>
            }
            key="0"
          >
            <UploadDataWidget type={"front_image"} order={0} openedTab={this.state.openedTab}/>
          </TabPane>
          <TabPane
            tab={
              <Tooltip title={`"Passport picture side" or "Identity card backside"`}>
                ID Back
              </Tooltip>
            }
            key="1"
          >
            <UploadDataWidget type={"back_image"} order={1}  openedTab={this.state.openedTab}/>
          </TabPane>
          <TabPane
            tab={
              <Tooltip
                title={
                  "A picture of you, holding your ID and a paper with today's date"
                }
              >
                Selfie
              </Tooltip>
            }
            key="2"
          >
            <UploadDataWidget type={"face_image"} order={2} openedTab={this.state.openedTab}/>
          </TabPane>
        </Tabs>
      </Card>
    );
  }
};


const mapStateToProps = (state, ownProps) => {
  return {
    identityScans: state.identityScans,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    get: (type) => {
      dispatch(get(type));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DocumentDataWidget);