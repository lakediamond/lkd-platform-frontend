import React from "react";
import {
  Card,
  Row,
  Col,
  Divider,
  Form,
  Select,
  Input,
  Radio,
  Button,
  Icon
} from "antd";

import { connect } from "react-redux";
import { postCommonInfo, get } from "redux/actions";
import { GET_COMMON_INFO } from "redux/constants";

const Option = Select.Option;
const FormItem = Form.Item;
// const RadioGroup = Radio.Group;

class CommonInfoDataWidget extends React.Component {
  state = {
    contribution: "ether",
    fields: [],
    id: null
  };

  componentDidMount() {
    this.props.get(GET_COMMON_INFO);

    setTimeout(() => {
      const element = document.querySelector(".ant-select-search__field");
      if (!!element) {
        element.setAttribute("autocomplete", "justforbreakautocomplete");
      }
    }, 300);
  }

  componentDidUpdate(prevProps, prevState, prevContext) {
    const {
      commonInfo: { commonInfo }
    } = this.props;

    if (prevProps.commonInfo.commonInfo !== commonInfo && commonInfo) {
      const { id } = commonInfo;
      const value = commonInfo.content_items[5]["value"];
      this.setState({ contribution: value, id: id });
    }
  }

  // radioChange = e => {
  //   this.setState({ contribution: e.target.value });
  // };

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields(async (err, values) => {
      if (!err) {
        values["id"] = this.state.id;
        values["reg_address_country"] = values["reg_address_country"].split(
          "|"
        )[0];
        values["contribution_option"] = "ether";
        this.props.postCommonInfo(values);
      }
    });
  };

  handleDecline = e => {
    e.preventDefault();
    this.props.form.resetFields();
  };

  renderCountriesSelect = () => {
    const {
      commonInfo: { commonInfo },
      form: { getFieldDecorator }
    } = this.props;

    if (!commonInfo) {
      return null;
    }

    const status = commonInfo ? commonInfo["status"] : null;
    const countriesOptions = [];

    commonInfo
      ? commonInfo.content_items[0]["data"].map(item => {
          countriesOptions.push(
            <Option
              value={`${item["country_code"]}|${item["country"]}`}
              key={item["id"]}
            >
              {`${item["country"]}`}
            </Option>
          );
        })
      : null;

    const code = commonInfo.content_items[0]["code"];
    const value = commonInfo.content_items[0]["value"];

    const countryValue = !!value
      ? commonInfo.content_items[0]["data"].filter(
          item => item["country_code"] == value
        )[0]["country"]
      : null;

    return (
      <FormItem label="Country">
        {getFieldDecorator(code, {
          validateTrigger: "onBlur",
          initialValue: countryValue,
          rules: [
            {
              required: true,
              message: "Please select country"
            }
          ]
        })(
          <Select showSearch disabled={status === "CUSTOMER_CONFIRMED"}>
            {countriesOptions}
          </Select>
        )}
      </FormItem>
    );
  };

  renderZipCodeInput = () => {
    const {
      commonInfo: { commonInfo },
      form: { getFieldDecorator }
    } = this.props;

    if (!commonInfo) {
      return null;
    }

    const status = commonInfo ? commonInfo["status"] : null;
    const code = commonInfo.content_items[1]["code"];
    const value = commonInfo.content_items[1]["value"];

    return (
      <FormItem label="Postal code">
        {getFieldDecorator(code, {
          validateTrigger: "onBlur",
          initialValue: value,
          rules: [
            {
              required: true,
              message: "Please input your postal code"
            }
          ]
        })(<Input disabled={status === "CUSTOMER_CONFIRMED"} />)}
      </FormItem>
    );
  };

  renderRegAddressCity = () => {
    const {
      commonInfo: { commonInfo },
      form: { getFieldDecorator }
    } = this.props;

    if (!commonInfo) {
      return null;
    }

    const status = commonInfo ? commonInfo["status"] : null;
    const code = commonInfo.content_items[2]["code"];
    const value = commonInfo.content_items[2]["value"];

    return (
      <FormItem label="City">
        {getFieldDecorator(code, {
          validateTrigger: "onBlur",
          initialValue: value,
          rules: [
            {
              required: true,
              message: "Please input your city"
            }
          ]
        })(<Input disabled={status === "CUSTOMER_CONFIRMED"} />)}
      </FormItem>
    );
  };

  renderRegAddressCityDetails = () => {
    const {
      commonInfo: { commonInfo },
      form: { getFieldDecorator }
    } = this.props;

    if (!commonInfo) {
      return null;
    }

    const status = commonInfo ? commonInfo["status"] : null;
    const code = commonInfo.content_items[3]["code"];
    const value = commonInfo.content_items[3]["value"];

    return (
      <FormItem label="Address line 1">
        {getFieldDecorator(code, {
          validateTrigger: "onBlur",
          initialValue: value,
          rules: [
            {
              required: true,
              message: "Please input your address line"
            }
          ]
        })(<Input disabled={status === "CUSTOMER_CONFIRMED"} />)}
      </FormItem>
    );
  };

  renderRegAddressCityExt = () => {
    const {
      commonInfo: { commonInfo },
      form: { getFieldDecorator }
    } = this.props;

    if (!commonInfo) {
      return null;
    }

    const status = commonInfo ? commonInfo["status"] : null;
    const code = commonInfo.content_items[4]["code"];
    const value = commonInfo.content_items[4]["value"];

    return (
      <FormItem label="Address line 2">
        {getFieldDecorator(code, {
          validateTrigger: "onBlur",
          initialValue: value,
          rules: [
            {
              required: false,
              message: "Please input your address line"
            }
          ]
        })(<Input disabled={status === "CUSTOMER_CONFIRMED"} />)}
      </FormItem>
    );
  };

  // renderContributionOptions = () => {
  //   const {
  //     commonInfo: {commonInfo},
  //     form: { getFieldDecorator }
  //   } = this.props;
  //
  //   if (!commonInfo) {
  //     return null;
  //   }
  //
  //   const status = commonInfo ? commonInfo["status"] : null;
  //   const code = commonInfo.content_items[5]["code"];
  //   const value = commonInfo.content_items[5]["value"];
  //
  //   return (
  //     <FormItem>
  //       {getFieldDecorator(code, {
  //         initialValue: value,
  //         rules: [
  //           {
  //             required: true,
  //             message: "Please choose your contribution plan"
  //           }
  //         ]
  //       })(
  //         <RadioGroup onChange={e => this.radioChange(e)}>
  //           <Radio disabled={status === "CUSTOMER_CONFIRMED"} value="ether">
  //             In Ethereum
  //           </Radio>
  //           <Radio disabled={status === "CUSTOMER_CONFIRMED"} value="fiat">
  //             In Fiat
  //           </Radio>
  //         </RadioGroup>
  //       )}
  //     </FormItem>
  //   );
  // };

  // renderBankNameInput = () => {
  //   const {
  //     commonInfo: {commonInfo},
  //     form: { getFieldDecorator }
  //   } = this.props;
  //
  //   if (!commonInfo) {
  //     return null;
  //   }
  //
  //   const status = commonInfo ? commonInfo["status"] : null;
  //   const code = commonInfo.content_items[6]["code"];
  //   const value = commonInfo.content_items[6]["value"];
  //
  //   return (
  //     <FormItem label="Bank Name">
  //       {getFieldDecorator(code, {
  //         validateTrigger: "onBlur",
  //         initialValue: value,
  //         rules: [
  //           {
  //             required: true,
  //             message: "Please input your bank name"
  //           }
  //         ]
  //       })(<Input disabled={status === "CUSTOMER_CONFIRMED"} />)}
  //     </FormItem>
  //   );
  // };
  //
  // renderBankAddress = () => {
  //   const {
  //     commonInfo: {commonInfo},
  //     form: { getFieldDecorator }
  //   } = this.props;
  //
  //   if (!commonInfo) {
  //     return null;
  //   }
  //
  //   const status = commonInfo ? commonInfo["status"] : null;
  //   const code = commonInfo.content_items[8]["code"];
  //   const value = commonInfo.content_items[8]["value"];
  //
  //   return (
  //     <FormItem label="Bank address">
  //       {getFieldDecorator(code, {
  //         validateTrigger: "onBlur",
  //         initialValue: value,
  //         rules: [
  //           {
  //             required: true,
  //             message: "Please input bank address"
  //           }
  //         ]
  //       })(<Input disabled={status === "CUSTOMER_CONFIRMED"} />)}
  //     </FormItem>
  //   );
  // };
  //
  // renderIBANInput = () => {
  //   const {
  //     commonInfo: {commonInfo},
  //     form: { getFieldDecorator }
  //   } = this.props;
  //
  //   if (!commonInfo) {
  //     return null;
  //   }
  //
  //   const status = commonInfo ? commonInfo["status"] : null;
  //   const code = commonInfo.content_items[7]["code"];
  //   const value = commonInfo.content_items[7]["value"];
  //
  //   return (
  //     <FormItem label="Contributor IBAN">
  //       {getFieldDecorator(code, {
  //         validateTrigger: "onBlur",
  //         initialValue: value,
  //         rules: [
  //           {
  //             required: (this.state.contribution === "fiat"),
  //             message: "Please input IBAN"
  //           }
  //         ]
  //       })(<Input disabled={status === "CUSTOMER_CONFIRMED"} />)}
  //     </FormItem>
  //   );
  // };

  renderButtonGroup = () => {
    const {
      commonInfo: { commonInfo },
      fetch: { state }
    } = this.props;

    const status = commonInfo ? commonInfo["status"] : null;

    if (!commonInfo) {
      return null;
    }

    if (status === "CUSTOMER_CONFIRMED") {
      return (
        <React.Fragment>
          <Row gutter={24}>
            <Col offset={18}>
              <span className={"confirmed"}>
                <Icon type="check-circle" theme="filled" /> Your information was
                received!
              </span>
            </Col>
          </Row>
        </React.Fragment>
      );
    }

    return (
      <React.Fragment>
        {/*<Divider />*/}
        <Row gutter={24}>
          <Col lg={{ offset: 17 }} xl={{ offset: 18 }}>
            <Row>
              <Col span={12}>
                {!state ? (
                  <Button type="danger" onClick={e => this.handleDecline(e)}>
                    Decline
                  </Button>
                ) : null}
              </Col>
              <Col span={12}>
                <Button type="primary" htmlType={"submit"}>
                  {!state ? (
                    "Approve"
                  ) : (
                    <Icon type="sync" spin style={{ color: "fff" }} />
                  )}
                </Button>
              </Col>
            </Row>
          </Col>
        </Row>
      </React.Fragment>
    );
  };

  render() {
    return (
      <Card
        title={"Please provide information on your actual physical address"}
      >
        <Form onSubmit={e => this.handleSubmit(e)}>
          <Row className="card_row">
            <div className="step_title">Address</div>
            <Row gutter={24}>
              <Col span={12}>{this.renderCountriesSelect()}</Col>
              <Col span={12}>{this.renderZipCodeInput()}</Col>
            </Row>
            <Row gutter={24}>
              <Col span={12}>{this.renderRegAddressCity()}</Col>
              <Col span={12}>{this.renderRegAddressCityDetails()}</Col>
            </Row>
            <Row gutter={24}>
              <Col span={12} offset={12}>
                {this.renderRegAddressCityExt()}
              </Col>
            </Row>
          </Row>
          {/*<Divider />*/}
          {/*<Row className="card_row">*/}
          {/*<Col>*/}
          {/*<div className="step_title">Source of funds</div>*/}
          {/*</Col>*/}
          {/*<Row>{this.renderContributionOptions()}</Row>*/}
          {/*{this.state.contribution === "fiat" ? (*/}
          {/*<React.Fragment>*/}
          {/*<Row gutter={24}>*/}
          {/*<Col span={12}>{this.renderBankNameInput()}</Col>*/}
          {/*<Col span={12}>{this.renderBankAddress()}</Col>*/}
          {/*</Row>*/}
          {/*<Row gutter={24}>*/}
          {/*<Col span={12}>{this.renderIBANInput()}</Col>*/}
          {/*</Row>*/}
          {/*</React.Fragment>*/}
          {/*) : null}*/}
          {/*</Row>*/}
          {this.renderButtonGroup()}
        </Form>
      </Card>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    commonInfo: state.commonInfo,
    fetch: state.fetch
  };
};

const mapDispatchToProps = dispatch => {
  return {
    postCommonInfo: value => {
      dispatch(postCommonInfo(value));
    },
    // getCommonInfo: () => {
    //   dispatch(getCommonInfo());
    // },
    get: type => {
      dispatch(get(type));
    }
  };
};

const commonInfoComponent = Form.create()(CommonInfoDataWidget);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(commonInfoComponent);
