import React from "react";
import HeaderLinks from "./HeaderLinks";
import { Layout } from "antd";
import { Wallet } from "components";

const HeaderComp = ({ ...props }) => {
  const {
    history: {
      location: { pathname }
    }
  } = props;
  
  const { Header } = Layout;

  // const showCurrentLocation =() => {
  //   return props.routes.map((prop, key) => {
  //     if (prop.path === pathname) {
  //       return prop.navbarName;
  //     }
  //     return null;
  //   });
  // };

  return pathname !== "/consent" && pathname !== "/auth" ? (
    <Header>
      <Wallet />
      <HeaderLinks />
    </Header>
  ) : null;
  
  // return (
  //   <Header>
  //     <Wallet />
  //   </Header>
  // );
};

export default HeaderComp;
