import React from "react";
import logo from "assets/img/ld_logo.png";

const Title = () => {
  return (
    <h2 className={"main_title"}>
      <img src={logo} alt={"Lake Diamonds logo"} />
      <span>Lake Diamond</span>
    </h2>
  )
};

export default Title;
