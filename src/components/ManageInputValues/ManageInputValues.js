import React from "react";
import { Icon, Row, Col } from "antd";
import style from "./index.css";

const ManageInputValues = props => {
  const handleIncrease = () => {
    const { actions } = props;
    actions[0]();
  };

  const handleDecrease = () => {
    const { actions } = props;
    actions[1]();
  };
  
  const { disabled } = props;
  
  return (
    <div className={disabled ? "manageInput disabled" : "manageInput"}>
      <Row className={"manageInput__control"} onClick={() => handleIncrease()}>
        <Icon type="up" />
      </Row>
      <Row className={"manageInput__control"} onClick={() => handleDecrease()}>
        <Icon type="down" />
      </Row>
      {props.currency ? (
        <div className={"manageInput__currency"}>{props.currency}</div>
      ) : null}
    </div>
  );
};

export default ManageInputValues;
