import React from "react";
import { NavLink } from "react-router-dom";
import { connect } from "react-redux";
import { Tabs, Row, Col, Icon, Select } from "antd";
import { ProposalsTable, OrdersTable } from "components";
import style from "./index.css";

const TabPane = Tabs.TabPane;

const TablesContainer = props => {
  const {
    type,
    title,
    table,
    action,
    proposalsList: {
      all: { records: allRecords, total_record: allRecordsLength },
      user: { records: userRecords, total_record: userRecordsLength }
    },
    owner
  } = props;
  
  
  const renderTable = (table, type) => {
    return (
      <Tabs defaultActiveKey="0" className={"ant-tabs_centered"}>
        <TabPane
          tab={type === "short" ? `My Active ${table}` : `My ${table}`}
          key="0"
        >
          {table === "Proposals" ? (
            <ProposalsTable type={type} collection={userRecords} userTable={true} totalRecord={userRecordsLength}/>
          ) : (
            <OrdersTable />
          )}
        </TabPane>
        <TabPane
          tab={type === "short" ? `Last Completed ${table}` : `All ${table}`}
          key="1"
        >
          {table === "Proposals" ? (
            <ProposalsTable type={type} collection={allRecords} userTable={false} totalRecord={allRecordsLength} />
          ) : (
            <OrdersTable />
          )}
        </TabPane>
      </Tabs>
    );
  };

  return (
    <div
      className={`${"tables-container"} ${
        type === "short" ? "tables-container_short" : null
      }`}
    >
      {title && <div className={"tables-container__title"}>{props.title}</div>}
      {renderTable(table, type)}
      {type === "short" && (
        <Row
          type="flex"
          justify="space-between"
          className={"tables-container__manage"}
        >
          <Col span={"12"} className={"tables-container__link"}>
            <NavLink to={`/${table.toLowerCase()}`}>
              View all <Icon type="right" />
            </NavLink>
          </Col>
          <Col className={"tables-container__update"} onClick={() => action()}>
            <Icon type="sync" />
            <span>Refresh table</span>
          </Col>
        </Row>
      )}
    </div>
  );
};

const mapStateToProps = (state, ownProps) => {
  return {
    proposalsList: state.proposalsList,
    user: state.user
  };
};

export default connect(
  mapStateToProps,
  null
)(TablesContainer);


//TODO removed this component. There is no Orders Table