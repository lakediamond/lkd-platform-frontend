import React from "react";
import { Table, Tag, Divider, Card, Button } from "antd";
import { createAddressLink } from "services";


class SubOwnersTable extends React.Component {
  state = {
    selectedSubOwnersToDelete: [],
    loading: false
  };
  
  handleClickRemoveSubOwners = () => {
    const dataCollection = this.state.selectedSubOwnersToDelete.map(item => item.eth_address);
    this.props.action(
      dataCollection
    );
  };
  
  selectRow = record => {
    const selectedSubOwnersToDelete = [...this.state.selectedSubOwnersToDelete];
    if (selectedSubOwnersToDelete.indexOf(record.eth_address) >= 0) {
      selectedSubOwnersToDelete.splice(selectedSubOwnersToDelete.indexOf(record.eth_address), 1);
    } else {
      selectedSubOwnersToDelete.push(record.eth_address);
    }
    this.setState({ selectedSubOwnersToDelete });
  };
  
  onSelectedRowKeysChange = (selectedRowKeys, selectedSubOwnersToDelete) => {
    this.setState({ selectedSubOwnersToDelete });
  };

  render() {
    const { loading, selectedSubOwnersToDelete } = this.state;
    const { isContractOwner, blocked } = this.props;
    const rowSelection = {
      selectedSubOwnersToDelete,
      onChange: this.onSelectedRowKeysChange
    };
    const { collection = null } = this.props;
    const hasSelected = selectedSubOwnersToDelete.length > 0;
  
    const tableColumns =  [
      { title: "First Name", dataIndex: "first_name", key: "first_name" },
      { title: "Last Name", dataIndex: "last_name", key: "last_name" },
      { title: "Email", dataIndex: "email", key: "email" },
      {
        title: "ETH Address", key: "eth_address", render: text => {
          return (
            <a
              href={createAddressLink(text["eth_address"])}
            >
              {text["eth_address"]}
            </a>
          );
        }
      }
    ];
  
    const setRowClassName = (record) => {
      const { selectedSubOwnersToDelete } = this.state;
      return selectedSubOwnersToDelete.map(item => item === record.eth_address ? 'ant-table-row_selected' : '');
    };
    
    return (
      <Card title={"Sub Owners"}>
        <Table
          className={"table_small card_row table_selectable"}
          rowClassName={setRowClassName}
          rowSelection={isContractOwner ? rowSelection : null}
          columns={tableColumns}
          dataSource={collection}
          pagination={false}
          onRow={record => ({
            onClick: () => {
              this.selectRow(record);
            }
          })}
        />
        <Button
          type="danger"
          onClick={this.handleClickRemoveSubOwners}
          disabled={!hasSelected}
          loading={loading}
        >
          Delete selected admins
        </Button>
      </Card>
    );
  }
}

export default SubOwnersTable;
