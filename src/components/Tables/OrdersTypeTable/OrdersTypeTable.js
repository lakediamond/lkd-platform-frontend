import React from "react";
import { Table, Card, Input, Button } from "antd";

const OrdersTypeTable = props => {
  const handleClickSubmit = e => {
    const parent = e.target.parentNode;
    const parentGrand = parent.parentNode;
    const parentNeighbor = parentGrand.previousSibling;
    const id = parentNeighbor.childNodes[1].textContent;
    const orderType = e.target.previousSibling.value;
    const previousValue = e.target.previousSibling.getAttribute(
      "aria-valuetext"
    );
    if (orderType === previousValue) {
      return false;
    }
    props.action(id, orderType);
  };

  const handleClickDecline = e => {
    const siblingInput = e.target.previousSibling.previousSibling;
    const defaultValue = siblingInput.getAttribute("aria-valuetext");
    siblingInput.value = defaultValue;
  };

  const { collection } = props;

  collection ? collection.sort((a, b) => a["ID"] - b["ID"]) : null;

  const divStyle = {
    display: "flex",
    alignItems: "center"
  };

  const buttonAcceptStyle = {
    margin: "0 10px"
  };

  const ordersType = [
    {
      title: "id",
      dataIndex: "ID",
      key: "ID",
      render: text => <span>{text}</span>
    },
    {
      title: "Description",
      dataIndex: "Name",
      key: "Name",
      render: text => (
        <div style={divStyle}>
          <Input
            type="text"
            defaultValue={text}
            aria-valuetext={text}
            ref="manageOrder"
          />
          <Button
            type="primary"
            shape="circle"
            icon="check"
            style={buttonAcceptStyle}
            onClick={e => handleClickSubmit(e)}
            size="small"
            disabled={props.blocked}
          />
          <Button
            type="danger"
            shape="circle"
            icon="close"
            onClick={e => handleClickDecline(e)}
            size="small"
            disabled={props.blocked}
          />
        </div>
      )
    }
  ];

  return (
    <Card title={"Orders Type"}>
      <Table columns={ordersType} dataSource={collection} pagination={false} />
    </Card>
  );
};

export default OrdersTypeTable;
