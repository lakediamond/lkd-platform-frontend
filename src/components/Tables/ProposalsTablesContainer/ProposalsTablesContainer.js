import React from "react";
import { Tabs, Card, Popover, Tooltip } from "antd";
import { ProposalsTable } from "components";
import { connect } from "react-redux";

const TabPane = Tabs.TabPane;

const ProposalsTablesContainer = (props) => {
  
  const { collection } = props;
  return (
    <React.Fragment>
      {/*<Tabs defaultActiveKey="0" className={"ant-tabs_centered"}>*/}
        {/*<TabPane*/}
          {/*tab={"Last Completed Proposals"}*/}
          {/*key="0"*/}
        {/*>*/}
          <ProposalsTable
            collection={collection["all"]}
            title={"All Proposals"}
          />
        {/*</TabPane>*/}
        {/*<TabPane*/}
          {/*tab={"My open proposals"}*/}
          {/*key="1"*/}
        {/*>*/}
          {/*<ProposalsTable*/}
            {/*collection={collection['user']}*/}
            {/*title={"My Proposals"}*/}
            {/*type={"user"}*/}
          {/*/>*/}
          {/**/}
        {/*</TabPane>*/}
      {/*</Tabs>*/}
    </React.Fragment>
  )
}

export default ProposalsTablesContainer;