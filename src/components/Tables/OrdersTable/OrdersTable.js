import React from "react";
import { NavLink } from "react-router-dom";
import { connect } from "react-redux";
import {
  formatData,
  createAddressLink,
  createTXLink,
  divideValueFor100,
  decreaseValueForBlockchainUI
} from "services";
import { Table, Card, Row, Col, Icon, Tooltip } from "antd";
// import { get } from "redux/actions";
// import { GET_PROPOSALS_ALL, GET_PROPOSALS_USER } from "redux/constants";

class OrdersTable extends React.Component {
  //  /user/proposal?page=2&limit=10&field=id&sort=asc
  param = null;

  sorters = {
    pagination: null,
    column: null,
    order: null
  };

  initialSorters = JSON.parse(JSON.stringify(this.sorters));

  handleChange = (pagination, filters, sorter) => {
    this.param = `page=${pagination.current}&field=${
      sorter.column ? sorter.column.dataIndex : ""
    }&sort=${sorter.order ? sorter.order.slice(0, 3) : ""}`;

    this.sorters = {
      pagination: pagination.current ? pagination.current : null,
      column: sorter.column ? sorter.column.dataIndex : null,
      order: sorter.order ? sorter.order : null
    };

    this.props.action(this.param);
  };

  handleResetTable = () => {
    this.param = null;
    this.sorters = this.initialSorters;
    return this.props.action();
  };

  handleRefreshTable = () => {
    return this.props.action(this.param);
  };

  expandedRowRender = (record, index) => {
    const { collection } = this.props;

    const investments = collection.map(item => {
      if (!!item["investments"]) {
        return item["investments"];
      }
      return false;
    });

    const columns = [
      {
        title: "Proposal ID",
        dataIndex: "proposal_id",
        key: `proposal_id`,
        render: (text, record, index) => {
          return (
            <a
              target="_blank"
              rel="noopener noreferrer"
              className={"ant-table__link ant-table__account"}
              href={createTXLink(record["tx_hash"])}
            >
              {text}
            </a>
          );
        }
      },
      {
        title: "Owner address",
        dataIndex: "eth_account",
        key: "eth_account",
        render: (text, record, index) => {
          return (
            <Tooltip
              placement="topLeft"
              title={text}
              overlayClassName={"ant-table__tooltip"}
            >
              <span className={"ant-table__account"}>{text}</span>
            </Tooltip>
          );
        }
      },
      {
        title: "Amount matched (LKD)",
        key: "tokens_invested",
        render: (text, record, index) => {
          return <span>{text["tokens_invested"]}</span>;
        }
      },
      {
        title: "Price per token (CHF)",
        key: "price",
        render: (text, record, index) => {
          return <span>{divideValueFor100(text["price"])}</span>;
        }
      },
      {
        title: "ETH distributed",
        key: "eth_received",
        render: (text, record, index) => {
          return (
            <span>{decreaseValueForBlockchainUI(text["eth_received"])}</span>
          );
        }
      },
      {
        title: "Created on",
        key: "created_on",
        dataIndex: "created_on",
        render: (text, record, index) => {
          return <span>{formatData(text)}</span>;
        }
      }
    ];

    return (
      <React.Fragment>
        <Table
          className="table_expanded"
          columns={columns}
          dataSource={investments[index]}
          pagination={false}
        />
      </React.Fragment>
    );
  };

  render() {
    const { collection, account, title, type, action, totalRecord } = this.props;

    const tableColumns = [
      {
        title: "ID",
        dataIndex: "id",
        key: "id",
        sorter: type === "full" ? () => ({}) : null,
        sortOrder: this.sorters.column === "id" && this.sorters.order,
        render: (text, record, index) => {
          return (
            <a
              target="_blank"
              rel="noopener noreferrer"
              className={"ant-table__link"}
              href={createTXLink(record["tx_hash"])}
            >
              {text}
            </a>
          );
        }
      },
      {
        title: "Type",
        dataIndex: "type",
        key: "type",
        sorter: type === "full" ? () => ({}) : null,
        sortOrder: this.sorters.column === "type" && this.sorters.order
      },
      {
        title: "Metadata",
        dataIndex: "metadata",
        key: "metadata",
        sorter: type === "full" ? () => ({}) : null,
        sortOrder: this.sorters.column === "metadata" && this.sorters.order
      },
      {
        title: "Growth time (LKD)",
        key: "tokens_amount",
        dataIndex: "tokens_amount",
        render: (text, record, index) => {
          return <span>{text}</span>;
        },
        sorter: type === "full" ? () => ({}) : null,
        sortOrder: this.sorters.column === "tokens_amount" && this.sorters.order
      },
      {
        title: "Price per token (CHF)",
        key: "token_price",
        dataIndex: "token_price",
        render: (text, record, index) => {
          return <span>{divideValueFor100(text)}</span>;
        },
        sorter: type === "full" ? () => ({}) : null,
        sortOrder: this.sorters.column === "token_price" && this.sorters.order
      },
      {
        title: "Order price (ETH)",
        dataIndex: "eth_amount",
        render: (text, record, index) => {
          return (
            <span>
              {decreaseValueForBlockchainUI(Number(text))}
            </span>
          );
        },
        sorter: type === "full" ? () => ({}) : null,
        sortOrder: this.sorters.column === "eth_amount" && this.sorters.order
      },
      {
        title: "Created on",
        key: "created_on",
        dataIndex: "created_on",
        render: (text, record, index) => {
          return <span>{formatData(text)}</span>;
        },
        sorter: type === "full" ? () => ({}) : null,
        sortOrder: this.sorters.column === "created_on" && this.sorters.order
      },
      {
        title: "Filled %",
        key: "filled_rate",
        dataIndex: "filled_rate",
        render: (text, record, index) => {
          return <span>{text}</span>;
        },
        sorter: type === "full" ? () => ({}) : null,
        sortOrder: this.sorters.column === "filled_rate" && this.sorters.order
      }
    ];

    return (
      <React.Fragment>
        {title && <div className={"tables-container__title"}>{title}</div>}
        {type === "full" && (
          <Row
            type="flex"
            justify="space-between"
            className={"tables-container__manage tables-container__manage_full"}
          >
            <Col className={"tables-container__update"}>
              <Icon type="sync" />
              <span onClick={this.handleRefreshTable}>Refresh table</span>
            </Col>
            <Col className={"tables-container__update"}>
              <span onClick={this.handleResetTable}>Reset table</span>
            </Col>
          </Row>
        )}
        <Table
          className={"table_small"}
          dataSource={collection}
          columns={tableColumns}
          pagination={
            type !== "short"
              ? {
                total: totalRecord
              }
              : false
          }
          onChange={this.handleChange}
          expandedRowRender={this.expandedRowRender}
        />
        {type === "short" && (
          <Row
            type="flex"
            justify="space-between"
            className={"tables-container__manage"}
          >
            <Col span={"12"} className={"tables-container__link"}>
              <NavLink to={`/orders`}>
                View all <Icon type="right" />
              </NavLink>
            </Col>
            <Col
              className={"tables-container__update"}
              onClick={() => action()}
            >
              <Icon type="sync" />
              <span>Refresh table</span>
            </Col>
          </Row>
        )}
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    proposalsList: state.proposalsList,
    user: state.user,
    blockchain: state.blockchain
  };
};

export default connect(
  mapStateToProps,
  null
)(OrdersTable);
