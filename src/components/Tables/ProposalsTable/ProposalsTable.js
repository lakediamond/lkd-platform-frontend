import React from "react";
import { Table, Button, Row, Col, Icon, Select, Tooltip } from "antd";
import {
  formatData,
  createAddressLink,
  createTXLink,
  divideValueFor100,
  decreaseValueForBlockchainUI
} from "services";
import { connect } from "react-redux";
import { get } from "redux/actions";
import { GET_PROPOSALS_ALL, GET_PROPOSALS_USER } from "redux/constants";
import { SetupBlockchainProvider } from "components";

class ProposalsTable extends React.Component {
  param = "";
  
  sorters = {
    pagination: null,
    column: null,
    order: null
  };
  
  initialSorters = JSON.parse(JSON.stringify(this.sorters));
  filteredInfo = null;

  renderOptionsOwners = () => {
    const {
      proposalsList: { owners }
    } = this.props;
    
    return owners.map((item, index) => {
      return {
        text: `${item}`,
        value: `${item}`
      };
    });
  };

  withdrawProposal = id => {
    const {
      blockchain: { ioContractInstance }
    } = this.props;

    ioContractInstance.withdrawProposal(id, (err, res) => {
      if (!err) {
        return;
      }
      return;
    });
  };

  handleChange = (pagination, filters, sorter) => {
    const {
      userTable,
      user: { eth_address }
    } = this.props;

    const { owner_address = null } = filters;

    const filter = !!owner_address
      ? owner_address
          .map((item, index) => {
            return `owner_address=${item}`;
          })
          .join("&")
      : "";

    const param = `page=${pagination.current}&field=${
      sorter.column ? sorter.column.dataIndex : null
    }&sort=${sorter.order ? sorter.order.slice(0, 3) : null}&${filter}`;
  
    this.sorters = {
      pagination: pagination.current ? pagination.current : null,
      column: sorter.column ? sorter.column.dataIndex : null,
      order: sorter.order  ? sorter.order  : null
    };
    this.filteredInfo = filters;

    const userParam = `owner_address=${eth_address}&${param}`;

    if (userTable) {
      this.param = userParam;
      return this.props.get(GET_PROPOSALS_USER, null, userParam);
    } else {
      this.param = param;
      return this.props.get(GET_PROPOSALS_ALL, null, param);
    }
  };

  handleClickSubmit = e => {
    this.withdrawProposal(e);
  };

  handleRefreshTable = () => {
    const {
      userTable,
      user: { eth_address },
      get
    } = this.props;

    return userTable
      ? get(
          GET_PROPOSALS_USER,
          null,
          `${this.param}&owner_address=${eth_address}`
        )
      : get(GET_PROPOSALS_ALL, null, this.param);
    // get(GET_PROPOSALS_ALL);
    // get(GET_PROPOSALS_USER, null, `owner_address=${eth_address}`);
  };

  handleResetTable = () => {
    const {
      userTable,
      user: { eth_address },
      get
    } = this.props;
    
    this.param = null;
    this.sorters = this.initialSorters;
    this.filteredInfo = null;

    return userTable
      ? get(GET_PROPOSALS_USER, null, `owner_address=${eth_address}`)
      : get(GET_PROPOSALS_ALL, null, null);
  };

  expandedRowRender = (record, index) => {
    const { collection } = this.props;

    const investments = collection.map(item => {
      if (!!item["investments"]) {
        return item["investments"];
      }
      return false;
    });

    const columns = [
      {
        title: "Order ID",
        dataIndex: "order_id",
        key: "order_id",
        render: (text, record, index) => {
          return (
            <a
              target="_blank"
              rel="noopener noreferrer"
              className={"ant-table__link"}
              href={createTXLink(record["investment_tx"])}
            >
              {text}
            </a>
          );
        }
      },
      {
        title: "Amount matched (LKD)",
        key: "tokens_invested",
        dataIndex: "tokens_invested"
      },
      {
        title: "ETH Distributed",
        dataIndex: "ether_invested",
        key: "ether_invested",
        render: (text, record, index) => {
          return <span>{decreaseValueForBlockchainUI(text)}</span>;
        }
      },
      {
        title: "Created On",
        dataIndex: "created_on",
        key: "created_on",
        render: (text, record, index) => {
          return <span>{formatData(text)}</span>;
        }
      }
    ];

    return (
      <Table
        className="table_expanded"
        columns={columns}
        dataSource={investments[index]}
        pagination={false}
        // expandedRowKeys={["0"]}
        // expandedRowKeys={this.state.expandedRowKeys}
        // onExpand={() => console.log("sasasas")}
        // onExpandedRowsChange={() => console.log("sdssdsdds")}
      />
    );
  };

  render() {
    const {
      type,
      collection,
      userTable,
      totalRecord,
      blockchain: { isEnabled }
    } = this.props;

    // this.renderOptionsOwners();

    const tableColumns = [
      {
        title: "ID",
        dataIndex: "id",
        key: `id`,
        width: 60,
        render: (text, record, index) => {
          return (
            <a
              target="_blank"
              rel="noopener noreferrer"
              className={"ant-table__link"}
              href={createTXLink(record["tx_hash"])}
            >
              {text}
            </a>
          );
        },
        sorter: type === "full" ?  () => ({}) : null,
        sortOrder: this.sorters.column === 'id' && this.sorters.order
      },
      {
        title: "Owner address",
        dataIndex: "owner_address",
        key: `owner_address`,
        width: 70,
        filters: !userTable ? this.renderOptionsOwners() : null,
        filterMultiple: true,
        filteredValue: this.filteredInfo ? this.filteredInfo.owners_address : null,
        render: (text, record, index) => {
          return (
            <Tooltip
              placement="topLeft"
              title={text}
              overlayClassName={"ant-table__tooltip"}
            >
              <span className={"ant-table__account"}>
                {text}
              </span>
            </Tooltip>
          );
        }
      },
      {
        title: "Remaining Amount (LKD)",
        dataIndex: "remaining_tokens_amount",
        key: "remaining_tokens_amount",
        width: 125,
        sorter: type === "full" ?  () => ({}) : null,
        sortOrder: this.sorters.column === 'remaining_tokens_amount' && this.sorters.order
      },
      {
        title: "Initial Amount (LKD)",
        dataIndex: "original_tokens_amount",
        key: "original_tokens_amount",
        width: 126,
        sorter: type === "full" ?  () => ({}) : null,
        sortOrder: this.sorters.column === 'original_tokens_amount' && this.sorters.order
      },
      {
        title: "Price per token (CHF)",
        key: `price`,
        dataIndex: "price",
        width: 126,
        render: (text, record, index) => {
          return <span>{divideValueFor100(text)}</span>;
        },
        sorter: type === "full" ?  () => ({}) : null,
        sortOrder: this.sorters.column === 'price' && this.sorters.order
      },
      {
        title: "Created on",
        key: `created_on`,
        dataIndex: "created_on",
        render: (text, record, index) => {
          return <span>{formatData(text)}</span>;
        },
        width: 148,
        sorter: type === "full" ?  () => ({}) : null,
        sortOrder: this.sorters.column === 'created_on' && this.sorters.order
      },
      {
        title: "Status",
        key: "status",
        dataIndex: "status",
        render: (text, record, index) => {
          if (userTable && text == "active" && !!isEnabled) {
            return (
              <span
                className={"ant-table__link"}
                onClick={e => this.handleClickSubmit(record["id"])}
              >
                <span className={`ant-table__status_cancel`}>&#9679;</span>{" "}
                Cancel
              </span>
            );
          }

          if (userTable && text == "active" && !!!isEnabled) {
            return (
              <span>
                <span className={`ant-table__status_setup`}>&#9679;</span>{" "}
                <SetupBlockchainProvider type={"short"} />
              </span>
            );
          }
          return (
            <span>
              <span className={`ant-table__status_${text}`}>&#9679;</span>{" "}
              {text.charAt(0).toUpperCase() + text.slice(1)}
            </span>
          );
        },
        width: 120,
        sorter: type === "full" ?  () => ({}) : null,
        sortOrder: this.sorters.column === 'status' && this.sorters.order
      },
      {
        title: "Canceled on",
        key: "withdrawn_on",
        dataIndex: "withdrawn_on",
        render: (text, record, index) => {
          return !!text ? <span>{formatData(text)}</span> : "";
        },
        width: 148,
        sorter: type === "full" ?  () => ({}) : null,
        sortOrder: this.sorters.column === 'withdrawn_on' && this.sorters.order
      },
      {
        title: "Cancelation Tx",
        key: "cancellation_tx_hash",
        dataIndex: "cancellation_tx_hash",
        width: 100,
        render: (text, record, index) => {
          return (
            <a
              target="_blank"
              rel="noopener noreferrer"
              className={"ant-table__account ant-table__link"}
              href={createTXLink(text)}
            >
              {text}
            </a>
          );
        },
        sorter: type === "full" ?  () => ({}) : null,
        sortOrder: this.sorters.column === 'cancellation_tx_hash' && this.sorters.order
      }
    ];

    const renderColumns = () => {
      if (type === "short") {
        tableColumns.splice(6, 3);
      }
      return tableColumns;
    };

    return (
      <React.Fragment>
        {type === "full" && (
          <Row
            type="flex"
            justify="space-between"
            className={"tables-container__manage tables-container__manage_full"}
          >
            <Col className={"tables-container__update"}>
              <Icon type="sync" />
              <span onClick={this.handleRefreshTable}>Refresh table</span>
            </Col>
            <Col className={"tables-container__update"}>
              <span onClick={() => this.handleResetTable()}>Reset table</span>
            </Col>
          </Row>
        )}
        <Table
          dataSource={collection}
          pagination={
            type !== "short"
              ? {
                  total: totalRecord
                }
              : false
          }
          onChange={this.handleChange}
          columns={renderColumns()}
          expandedRowRender={this.expandedRowRender}
        />
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    proposalsList: state.proposalsList,
    user: state.user,
    blockchain: state.blockchain
  };
};

const mapDispatchToProps = dispatch => {
  return {
    get: (type, body, param) => {
      dispatch(get(type, body, param));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProposalsTable);
