import React from "react";
import PropTypes from "prop-types";
import { Card, Row } from "antd";
import { CountDown } from "ant-design-pro";

const FinishMessage = props => {
  const { success, message, method } = props;
  const targetTime = new Date().getTime() + 10000;
  const title = success ? "Congratulations" : "Error";
  const text = success ? "You Successfully" : "We are sorry but you didn't";

  return (
    <Card title={title}>
      <Row className={"text_row"}>
        {`${text} ${message}.`}
        {`You will be redirect to the login page in`} <CountDown target={targetTime} onEnd={() => method()} />
      </Row>
      <Row>
        Or you can go to <a href={"/auth"}>login manually</a>
      </Row>
    </Card>
  );
};

// FinishMessage.propTypes = {
//   success: PropTypes.boolean.isRequired,
//   message: PropTypes.string.isRequired,
//   method: PropTypes.fn.isRequired,
// };

export default FinishMessage;
