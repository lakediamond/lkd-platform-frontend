import React from "react";
import { Card, Row, Col, Button, Form, Select, Modal } from "antd";
import { connect } from "react-redux";
import { setBlockchainProvider, closeSelectProvider } from "redux/actions";

const { Option, OptGroup } = Select;
const FormItem = Form.Item;

class SelectBlockchainProvider extends React.Component {
  submitForm = e => {
    console.log(this.props);
    const {
      setBlockchainProvider,
      form,
      user: { blockchain_connection_type },
      closeSelectProvider
    } = this.props;
    e.preventDefault();
    form.validateFields((err, values) => {
      if (!err) {
        if (
          blockchain_connection_type != values["blockchain_connection_type"]
        ) {
          setBlockchainProvider(values);
        }
        closeSelectProvider();
      }
    });
  };

  render() {
    const {
      form: { getFieldDecorator },
      closeSelectProvider,
      blockchain: { selectProviderOpened }
    } = this.props;

    return (
      <Modal
        visible={selectProviderOpened}
        onCancel={closeSelectProvider}
        closable={true}
        footer={null}
        className={"modal"}
      >
        <React.Fragment>
          <Row>
            <Col>
              <Form onSubmit={e => this.submitForm(e)}>
                <Row className={"card_row"}>
                  <FormItem>
                    {getFieldDecorator("blockchain_connection_type", {
                      validateTrigger: "onBlur",
                      rules: [
                        {
                          required: true,
                          message: "Please choose your blockchain provider"
                        }
                      ]
                    })(
                      <Select placeholder={"Blockchain provider"}>
                        <Option key={"Metamask_key"} value={"METAMASK"}>
                          Metamask
                        </Option>
                        <Option key={"Ledger_key"} value={"LEDGER"} disabled>
                          Ledger
                        </Option>
                      </Select>
                    )}
                  </FormItem>
                </Row>
                <Row gutter={16}>
                  <Col span={12}>
                    <Button htmlType={"submit"} type={"primary"} block>Submit</Button>
                  </Col>
                  <Col span={12}>
                    <Button type={"danger"} onClick={closeSelectProvider} block>
                      Decline
                    </Button>
                  </Col>
                </Row>
              </Form>
            </Col>
          </Row>
        </React.Fragment>
      </Modal>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    user: state.user,
    blockchain: state.blockchain
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setBlockchainProvider: value => {
      dispatch(setBlockchainProvider(value));
    },
    closeSelectProvider: () => {
      dispatch(closeSelectProvider());
    }
  };
};

const blockChainProvider = Form.create()(SelectBlockchainProvider);
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(blockChainProvider);
