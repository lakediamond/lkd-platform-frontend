import React from "react";
import PropTypes from "prop-types";
import { Switch, Route, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import {
  HeaderComp,
  Sidebar,
  Spinner,
  BlockchainProviderSettings,
  SelectBlockchainProvider
} from "components";
import appRoutes from "routes/app.jsx";
import { Login, Register, Auth, Consent, Final, Recovery } from "views";
import { Layout, Alert, notification } from "antd";
import {
  showAlert,
  loginUser,
  closeAlert,
  isMetamaskLoggedIn,
  enableMetamask,
  createContractInstance,
  signInUser,
  getETHBalance,
  getTokenBalance,
  userAchievedKyc
} from "redux/actions/index";

import { startAuth } from "services";

const switchRoutes = (
  <Switch>
    {appRoutes.map((prop, key) => {
      if (prop.redirect)
        return <Redirect from={prop.path} to={prop.to} key={key} />;
      return (
        <Route exact path={prop.path} component={prop.component} key={key} />
      );
    })}
  </Switch>
);

class App extends React.Component {
  state = {};

  componentDidMount() {
    const {
      location: { pathname },
      signInUser
    } = this.props;

    this.intervals = [];

    const loginPath = pathname === "/login";
    const registerPath = pathname === "/register";
    const authPath = pathname === "/auth";
    const consentPath = pathname === "/consent";
    const recoveryPath = pathname === "/user/access_recovery_activation";
    const finalPath = pathname === "/";

    if (
      !loginPath &&
      !registerPath &&
      !authPath &&
      !consentPath &&
      !finalPath &&
      !recoveryPath
    ) {
      signInUser();
    }
  }

  componentWillUnmount() {
    this.intervals.map(item => clearInterval(item));
    this.intervals = [];
  }

  componentDidUpdate(prevProps, prevState) {
    const {
      blockchain: { metaMaskAccount, ioContractInstance },
      createContractInstance,
      location: { pathname },
      user: { eth_address, token_expired, kyc_summary: { achieved } },
      tierInfo,
      userAchievedKyc
    } = this.props;

    if (this.refs.mainPanel) {
      this.refs.mainPanel.scrollTop = 0;
    }

    if (
      prevProps.blockchain.metaMaskAccount !== metaMaskAccount &&
      !!metaMaskAccount &&
      !!eth_address
    ) {
      this.checkCurrentAccount(metaMaskAccount);
      if (this.intervals.length < 2) {
        this.intervals.push(
          setInterval(() => this.isAccountChanged(metaMaskAccount), 1000)
        );
      }
    }

    if (
      !!metaMaskAccount &&
      !!!prevProps.blockchain.ioContractInstance &&
      !!!ioContractInstance
    ) {
      createContractInstance(metaMaskAccount);
    }

    if (prevProps.user.token_expired !== token_expired) {
      this.renderUserTokenExpired();
    }

    //TODO need to be improved
    if (prevProps.user.kyc_summary.achieved === null && !achieved) {
      if (tierInfo.tierInfo) {
        // const succeedTiers = tierInfo.tierInfo[0].steps.filter(
        //   item =>
        //     item["status"] === "CUSTOMER_CONFIRMED" ||
        //     item["status"] === "VERIFIED"
        // );
        // if (succeedTiers.length === 3) {
        //   userAchievedKyc();
        // }
        if(tierInfo.tierInfo.status === "GRANTED"){
          userAchievedKyc();
        }
      }
    }
  }

  renderUserTokenExpired = () => {
    notification.open({
      message: `Your session has expired. You need to login again`,
      description: `We will automatically refresh your page after five seconds or you can close this notification to do it now`,
      duration: 0,
      onClose: () => {
        startAuth();
      }
    });
    setTimeout(() => {
      startAuth();
    }, 5000);
  };

  render() {
    const { Content } = Layout;
    const {
      alertMessage: { open_alert, alert_message },
      location: { pathname },
      blockchain: { isLoggedIn },
      closeSetupProvider,
      ...rest
    } = this.props;

    const {
      user: { in_progress, token_expired }
    } = rest;

    const loginPath = pathname === "/login";
    const registerPath = pathname === "/register";
    const authPath = pathname === "/auth";
    const consentPath = pathname === "/consent";
    const recoveryPath = pathname === "/user/access_recovery_activation";
    const finalPath = pathname === "/";

    const renderOAUTHViews = pathname => {
      switch (pathname) {
        case "/login":
          return <Login history={rest.history} />;
        case "/register":
          return <Register history={rest.history} />;
        case "/auth":
          return <Auth history={rest.history} />;
        case "/consent":
          return <Consent history={rest.history} />;
        case "/":
          return <Final history={rest.history} />;
        case "/user/access_recovery_activation":
          return <Recovery history={rest.history} />;
        default:
          return null;
      }
    };

    if (
      loginPath ||
      registerPath ||
      authPath ||
      consentPath ||
      finalPath ||
      recoveryPath
    ) {
      return <React.Fragment>{renderOAUTHViews(pathname)}</React.Fragment>;
    }

    if (in_progress || open_alert === null) {
      return <Spinner />;
    }

    const blockchainIsNeededPath = pathname => {
      return (
        pathname === "/proposals" ||
        pathname === "/wallet" ||
        pathname === "/management" ||
        pathname === "/orders"
      );
    };

    return (
      <React.Fragment>
        {open_alert ? (
          <Alert
            message="Uncorrect metamask account"
            description={alert_message}
            type="error"
          />
        ) : null}
        <BlockchainProviderSettings />
        <SelectBlockchainProvider />
        <Layout style={{ minHeight: "100vh" }}>
          <Sidebar
            routes={appRoutes}
            linksDisabled={this.state.modal_open}
            {...rest}
          />
          <Layout
            className={[
              isLoggedIn === false && blockchainIsNeededPath(pathname)
                ? "metamask_not_logged_in"
                : null,
              token_expired ? "metamask_not_logged_in" : null
            ]}
          >
            <HeaderComp routes={appRoutes} {...rest} />
            <Content className={open_alert ? "layout_blocked" : null}>
              {switchRoutes}
            </Content>
          </Layout>
        </Layout>
      </React.Fragment>
    );
  }

  isAccountChanged(account) {
    if (window.web3.eth.accounts[0] !== account) {
      window.location.reload();
    }
  }

  checkCurrentAccount(account) {
    const {
      user: { eth_address },
      showAlert,
      closeAlert
    } = this.props;

    if (eth_address.toLowerCase() !== account.toLowerCase()) {
      showAlert(
        `Please choose Ethereum account address you specified during registration. Last four numbers are: ${eth_address.slice(
          eth_address.length - 4
        )}`
      );
    } else {
      closeAlert();
    }
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    alertMessage: state.alertMessage,
    blockchain: state.blockchain,
    user: state.user,
    tierInfo: state.tierInfo
  };
};

const mapDispatchToProps = dispatch => {
  return {
    showAlert: message => {
      dispatch(showAlert(message));
    },
    closeAlert: () => {
      dispatch(closeAlert());
    },
    isMetamaskLoggedIn: () => {
      dispatch(isMetamaskLoggedIn());
    },
    enableMetamask: () => {
      dispatch(enableMetamask());
    },
    createContractInstance: () => {
      dispatch(createContractInstance());
    },
    signInUser: () => {
      dispatch(signInUser());
    },
    getETHBalance: account => {
      return dispatch(getETHBalance(account));
    },
    getTokenBalance: account => {
      return dispatch(getTokenBalance(account));
    },
    // get: (type, body, param) => {
    //   dispatch(get(type, body, param));
    // }
    userAchievedKyc: ()=> {
      return dispatch(userAchievedKyc())
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
