import {
  GET_TIERS_INFO_SUCCESS,
  GET_TIERS_INFO_FAILED
} from "../constants";

const defaultState = {
  error: null,
  tierInfo: null,
};

export default (state = defaultState, action) => {
  switch (action.type) {
    case GET_TIERS_INFO_SUCCESS:
      return {...state, tierInfo: action.payload };
    case GET_TIERS_INFO_FAILED:
      return {...state, error: action.payload};
    default:
      return state;
  }
};