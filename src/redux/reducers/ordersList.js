import {
  // ORDERS_SOCKET_OPENED,
  // ORDERS_SOCKET_SUCCESS,
  // ORDERS_SOCKET_FAILED,
  // ORDERS_SOCKET_CLOSED,
  GET_ORDERS_SUCCESS,
  GET_ORDERS_FAILED,
  GET_ORDER_TYPES_SUCCESS,
  GET_ORDER_TYPES_FAILED
} from "redux/constants";

const defaultState = {
  error: null,
  orders: {
    records: null
  },
  types: null,
  // socket: null
};


const ordersList = (state = defaultState, action) => {
  switch (action.type) {
    // case ORDERS_SOCKET_OPENED:
    //   return {...state, socket: action.payload};
    case GET_ORDERS_SUCCESS:
      return {...state, orders: action.payload};
    case GET_ORDERS_FAILED:
      return {...state, error: action.payload};
    // case ORDERS_SOCKET_CLOSED:
    //   return {...state, socket: null };
    case GET_ORDER_TYPES_SUCCESS:
      return {...state, types: action.payload};
    case GET_ORDER_TYPES_FAILED:
      return {...state, orders: state.types, error: action.payload};
    default:
      return state;
  }
};

export default ordersList;

// if (JSON.stringify(action.payload) === JSON.stringify(state)) {
//   return state;
// }
