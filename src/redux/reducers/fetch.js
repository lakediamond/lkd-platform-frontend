import {
  FETCH_IN_PROGRESS,
  FETCH_IS_FINISHED
} from "../constants";

const defaultState = {
  state: false,
  entity: null
};

export default (state = defaultState, action) => {
  switch (action.type) {
    case FETCH_IN_PROGRESS:
      return {...state, state: true, entity: action.payload };
    case FETCH_IS_FINISHED:
      return defaultState;
    default:
      return defaultState;
  }
};