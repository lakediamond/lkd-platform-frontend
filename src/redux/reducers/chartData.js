import {
  GET_CHART_DATA_SUCCESS,
  GET_CHART_DATA_FAILED,
} from "../constants";

const defaultState = {};

export default (state = defaultState, action) => {
  switch (action.type) {
    case GET_CHART_DATA_SUCCESS:
      return action.payload;
    case GET_CHART_DATA_FAILED:
      return { ...state};
    default:
      return state;
  }
};
