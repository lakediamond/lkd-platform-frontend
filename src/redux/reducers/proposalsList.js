import {
  // PROPOSALS_USER_SOCKET_OPENED,
  // PROPOSALS_USER_SOCKET_SUCCESS,
  // PROPOSALS_USER_SOCKET_FAILED,
  // PROPOSALS_USER_SOCKET_CLOSED,
  // PROPOSALS_ALL_SOCKET_OPENED,
  // PROPOSALS_ALL_SOCKET_SUCCESS,
  // PROPOSALS_ALL_SOCKET_FAILED,
  // PROPOSALS_ALL_SOCKET_CLOSED,
  GET_PROPOSALS_ALL_SUCCESS,
  GET_PROPOSALS_ALL_FAILED,
  GET_PROPOSALS_USER_SUCCESS,
  GET_PROPOSALS_USER_FAILED,
  GET_PROPOSALS_OWNERS_SUCCESS,
  GET_PROPOSALS_OWNERS_FAILED,
} from "redux/constants";

const defaultState = {
  all: {
    records: null
  },
  user: {
    records: null
  },
  error: null,
  owners: []
  // socketAll: null,
  // socketUser: null,
};

const proposalsList = (state = defaultState, action) => {
    switch (action.type) {
      // case PROPOSALS_USER_SOCKET_OPENED:
      //       return {...state, socketUser: action.payload };
      case GET_PROPOSALS_USER_SUCCESS:
        return {...state, user: action.payload };
      case GET_PROPOSALS_USER_FAILED:
        return {...state, error: action.payload};
      // case PROPOSALS_USER_SOCKET_CLOSED:
      //   return {...state, socketUser: null };
      // case PROPOSALS_ALL_SOCKET_OPENED:
      //   return {...state, socketAll: action.payload };
      case GET_PROPOSALS_ALL_SUCCESS:
        return {...state, all: action.payload };
      case GET_PROPOSALS_ALL_FAILED:
        return {...state, error: action.payload};
      case GET_PROPOSALS_OWNERS_SUCCESS:
        return {...state, owners: action.payload['owners']};
      case GET_PROPOSALS_OWNERS_FAILED:
        return {...state };
      // case PROPOSALS_ALL_SOCKET_CLOSED:
      //   return {...state, socketAll: null };
      default:
        return state;
    }
};

export default proposalsList;
