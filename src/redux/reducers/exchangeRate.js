import {
  GET_ETH_EXCHANGE_RATE_SUCCESS,
  GET_ETH_EXCHANGE_RATE_FAILED,
  GET_ETH_EXCHANGE_RATE_WALLET_SUCCESS,
  GET_ETH_EXCHANGE_RATE_WALLET_FAILED
} from "../constants";

const defaultState = {
  exchangeRate: {
    converted_amount: null,
    currency: null,
    risk_included: null,
    risk_amount: null,
    deviation_allowed_min: null,
    deviation_allowed_max: null,
  },
  wallet_converted_amount: null
};

export default (state = defaultState, action) => {
  switch (action.type) {
    case GET_ETH_EXCHANGE_RATE_SUCCESS:
      return { ...state,
        exchangeRate: {
          currency: action.payload["currency"],
          risk_included: action.payload["risk_included"],
          risk_amount: action.payload["risk_amount"],
          deviation_allowed_min: action.payload["deviation_allowed_min"],
          deviation_allowed_max: action.payload["deviation_allowed_max"],
          converted_amount: action.payload["converted_amount"]}
      };
    case GET_ETH_EXCHANGE_RATE_FAILED:
      return { ...state};
    case GET_ETH_EXCHANGE_RATE_WALLET_SUCCESS:
      return {
        ...state,
        wallet_converted_amount: action.payload["converted_amount"]
      };
    case GET_ETH_EXCHANGE_RATE_WALLET_FAILED:
      return {...state};
    default:
      return state;
  }
};
