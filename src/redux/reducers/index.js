import { combineReducers } from 'redux';
import ordersList from './ordersList';
import proposalsList from './proposalsList';
import subOwnersList from "./subOwners";
import alertMessage from './alertMessage';
import user from './user';
import blockchain from './blockchain';
import phone from './phone';
import commonInfo from './commonInfo';
import tierInfo from "./kyc";
import fetch from "./fetch";
import exchangeRate from "./exchangeRate"
import chartData from "./chartData";
// import complianceReview from "./complianceReview";
import auth from "./auth";
import identityScans from "./identityScans";

const tokensiteApp = combineReducers({
    ordersList,
    proposalsList,
    subOwnersList,
    alertMessage,
    user,
    blockchain,
    phone,
    commonInfo,
    tierInfo,
    // complianceReview,
    exchangeRate,
    auth,
    fetch,
    identityScans,
    chartData
});

export default tokensiteApp;
