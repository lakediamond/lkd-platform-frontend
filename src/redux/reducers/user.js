import {
  LOGIN_USER_SUCCESS,
  LOGIN_USER_IN_PROGRESS,
  LOGIN_USER_FAILED,
  GET_INVESTMENT_PLAN_FAILED,
  GET_INVESTMENT_PLAN_SUCCESS,
  POST_INVESTMENT_PLAN_FAILED,
  POST_INVESTMENT_PLAN_SUCCESS,
  LOGOUT_USER_SUCCESS,
  LOGOUT_USER_FAILED,
  POST_CONSENT_IN_PROGRESS,
  POST_CONSENT_SUCCESS,
  POST_CONSENT_FAILED,
  GET_2FA_CODE_SUCCESS,
  GET_2FA_CODE_FAILED,
  ENABLE_2FA_SUCCESS,
  ENABLE_2FA_FAILED,
  DISABLE_2FA_SUCCESS,
  DISABLE_2FA_FAILED,
  USER_TOKEN_EXPIRED,
  POST_PROVIDER_SUCCESS,
  POST_PROVIDER_FAILED,
  USER_ACHIEVED_KYC
} from "../constants";

const defaultState = {
  email: "",
  id: "",
  eth_address: null,
  role: null,
  first_name: "",
  last_name: "",
  locked: "",
  birth_date: null,
  kyc_summary: {
    achieved: null,
    ongoing: null,
    upcoming: null
  },
  target_tier: null,
  investment_plans: null,
  request: false,
  in_progress: false,
  google_2fa_enabled: false,
  google_2fa_code: null,
  token_expired: false,
  blockchain_connection_type: null
};

const user = (state = defaultState, action) => {
  switch (action.type) {
    case LOGIN_USER_SUCCESS:
      return {
        ...state,
        email: action.payload["email"],
        id: action.payload["id"],
        eth_address: action.payload["eth_address"],
        role: action.payload["role"],
        first_name: action.payload["first_name"],
        birth_date: action.payload["birth_date"],
        last_name: action.payload["last_name"],
        locked: action.payload["locked"],
        kyc_summary: action.payload["kyc_summary"],
        target_tier: action.payload["target_tier"],
        google_2fa_enabled: action.payload["google_2fa_enabled"],
        blockchain_connection_type: action.payload["blockchain_connection_type"],
        in_progress: false
      };
    case LOGIN_USER_IN_PROGRESS:
      return {
        ...state,
        in_progress: true
      };
    case LOGIN_USER_FAILED:
      return {
        ...state,
        defaultState
      };
    case LOGOUT_USER_FAILED:
      return {state};
    case LOGOUT_USER_SUCCESS:
      return {
        ...state
      };
    case GET_INVESTMENT_PLAN_FAILED:
      return {
        ...state,
        investment_plans: null
      };
    case GET_INVESTMENT_PLAN_SUCCESS:
      return {
        ...state,
        investment_plans: action.payload
      };
    case POST_INVESTMENT_PLAN_SUCCESS:
      return {
        ...state,
        target_tier: action.payload
      };
    case POST_CONSENT_IN_PROGRESS:
      return {
        ...state,
        request: true
      };
    case POST_CONSENT_SUCCESS:
      return {
        ...state,
        request: false
      };
    case POST_CONSENT_FAILED:
      return {
        ...state,
        request: false
      };
    case GET_2FA_CODE_SUCCESS:
      return {
        ...state,
        google_2fa_code: action.payload['qr_code']
      };
    case GET_2FA_CODE_FAILED:
      return {
        ...state,
        google_2fa_code: null
      };
    case ENABLE_2FA_SUCCESS:
      return {
        ...state,
        google_2fa_enabled: true,
      };
    case ENABLE_2FA_FAILED:
      return {
        ...state,
        google_2fa_enabled: false,
      };
    case DISABLE_2FA_SUCCESS:
      return {
        ...state,
        google_2fa_enabled: false,
        google_2fa_code: null
      };
    case ENABLE_2FA_FAILED:
      return {
        ...state,
      };
    case USER_TOKEN_EXPIRED:
      return {
        ...state,
        token_expired: true
      };
    case POST_PROVIDER_SUCCESS:
      return {
        ...state,
        blockchain_connection_type: action.payload
      };
    case POST_PROVIDER_FAILED:
      return {
        ...state,
      };
      //todo need to improve
    case USER_ACHIEVED_KYC:
      return {
        ...state,
        kyc_summary: {
          achieved: true,
        }
      };
    default:
      return state;
  }
};

export default user;
