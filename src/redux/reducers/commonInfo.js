import {
  POST_COMMON_INFO_SUCCESS,
  POST_COMMON_INFO_FAILED,
  GET_COMMON_INFO_FAILED,
  GET_COMMON_INFO_SUCCESS,
} from "../constants";

const defaultState = {
  error: null,
  infoPosted: false,
  request: false,
  commonInfo: null
};

export default (state = defaultState, action) => {
  switch (action.type) {
    case POST_COMMON_INFO_SUCCESS:
      return {...state, infoPosted: true };
    case POST_COMMON_INFO_FAILED:
      return {
        ...state,
        request: false,
        error: {
          code: action.payload["code"],
          message: action.payload["message"]
        }
      };
    case GET_COMMON_INFO_FAILED:
      return defaultState;
    case GET_COMMON_INFO_SUCCESS:
      return {...state, commonInfo: action.payload};
    default:
      return state;
  }
};