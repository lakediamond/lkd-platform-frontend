import {
  METAMASK_INSTALLED,
  METAMASK_NOT_INSTALLED,
  METAMASK_USER_LOGGED_IN_SUCCESS,
  METAMASK_USER_LOGGED_IN_FAILED,
  METAMASK_ENABLED,
  METAMASK_NOT_ENABLED,
  USER_IS_CONTRACT_OWNER,
  USER_NOT_CONTRACT_OWNER,
  ETH_BALANCE_RECEIVE_SUCCESS,
  ETH_BALANCE_RECEIVE_FAILED,
  TOKEN_BALANCE_RECEIVE_SUCCESS,
  TOKEN_BALANCE_RECEIVE_FAILED,
  CONTRACT_INSTANCE_CREATED,
  ALLOWANCE_RECEIVED,
  ALLOWANCE_FAILED,
  OPEN_SETUP_PROVIDER,
  CLOSE_SETUP_PROVIDER,
  OPEN_SELECT_PROVIDER,
  CLOSE_SELECT_PROVIDER,
  SET_ESTIMATION_ETHEREUM_PRICE,
  RESET_ESTIMATION_ETHEREUM_PRICE,
  GET_ALLOWED_TOKEN_AMOUNT_SUCCESS,
  GET_ALLOWED_TOKEN_AMOUNT_FAILED
} from "../constants";

const defaultState = {
  isInstalled: null,
  isLoggedIn: null,
  isEnabled: null,
  allowance: null,
  metaMaskAccount: null,
  isContractOwner: null,
  tokenContractInstance: null,
  ioContractInstance: null,
  storageContractInstance: null,
  selectProviderOpened: false,
  setupProviderOpened: false,
  ethBalance: null,
  tokenBalance: null,
  estimationEthereumPrice: null,
  allowedTokensAmount: null
};

export default (state = defaultState, action) => {
  switch (action.type) {
    case METAMASK_INSTALLED:
      return { ...state, isInstalled: action.payload };
    case METAMASK_NOT_INSTALLED:
      return { ...state, isInstalled: false };
    case METAMASK_USER_LOGGED_IN_FAILED:
      return { ...state, isLoggedIn: false };
    case METAMASK_USER_LOGGED_IN_SUCCESS:
      return { ...state, isLoggedIn: true };
    case METAMASK_ENABLED:
      return {
        ...state,
        isEnabled: action.payload["isEnabled"],
        metaMaskAccount: action.payload["account"],
        isLoggedIn: true
      };
    case USER_IS_CONTRACT_OWNER:
      return { ...state, isContractOwner: true };
    case USER_NOT_CONTRACT_OWNER:
      return { ...state, isContractOwner: false };
    case METAMASK_NOT_ENABLED:
      return { ...state, isEnabled: false, metaMaskAccount: "" };
    case ETH_BALANCE_RECEIVE_SUCCESS:
      return { ...state, ethBalance: action.payload };
    case ETH_BALANCE_RECEIVE_FAILED:
      return { ...state, ethBalance: 0 };
    case TOKEN_BALANCE_RECEIVE_SUCCESS:
      return { ...state, tokenBalance: action.payload };
    case TOKEN_BALANCE_RECEIVE_FAILED:
      return { ...state, tokenBalance: 0 };
    case CONTRACT_INSTANCE_CREATED:
      return {
        ...state,
        tokenContractInstance: action.payload["tokenContractInstance"],
        ioContractInstance: action.payload["ioContractInstance"],
        storageContractInstance: action.payload["storageContractInstance"]
      };
    case ALLOWANCE_RECEIVED:
      return { ...state, allowance: action.payload };
    case ALLOWANCE_FAILED:
      return { ...state, allowance: action.payload };
    case OPEN_SELECT_PROVIDER:
      return { ...state, selectProviderOpened: true };
    case CLOSE_SELECT_PROVIDER:
      return { ...state, selectProviderOpened: false };
    case OPEN_SETUP_PROVIDER:
      return { ...state, setupProviderOpened: true };
    case CLOSE_SETUP_PROVIDER:
      return { ...state, setupProviderOpened: false };
    case SET_ESTIMATION_ETHEREUM_PRICE:
      return { ...state, estimationEthereumPrice: action.payload };
    case RESET_ESTIMATION_ETHEREUM_PRICE:
      return { ...state, estimationEthereumPrice: action.payload };
    case GET_ALLOWED_TOKEN_AMOUNT_SUCCESS:
      return { ...state, allowedTokensAmount: action.payload };
    case GET_ALLOWED_TOKEN_AMOUNT_FAILED:
      return { ...state};
    default:
      return state;
  }
};
