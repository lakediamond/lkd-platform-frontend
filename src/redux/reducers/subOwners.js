import {
  GET_SUB_OWNERS_SUCCESS,
  GET_SUB_OWNERS_FAILED
} from "redux/constants";

const defaultState = [];

export default (state = defaultState, action) => {
  switch (action.type) {
    case GET_SUB_OWNERS_SUCCESS:
      if (JSON.stringify(action.payload) === JSON.stringify(state)) {
        return state;
      }
      return action.payload;
    case GET_SUB_OWNERS_FAILED:
      return state;
    default:
      return state;
  }
};

