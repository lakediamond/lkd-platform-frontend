import {
  POST_PHONE_SUCCESS,
  POST_PHONE_FAILED,
  POST_SMS_CODE_SUCCESS,
  POST_SMS_CODE_FAILED,
  GET_PHONE_INFO_FAILED,
  GET_PHONE_INFO_SUCCESS,
  RESEND_PHONE_NUMBER_FAILED,
  RESEND_PHONE_NUMBER_SUCCESS,
  FETCH_IN_PROGRESS
} from "../constants";

const defaultState = {
  phoneInfo: null,
  request: false,
  error: null
};

const phone = (state = defaultState, action) => {
  switch (action.type) {
    case POST_PHONE_SUCCESS:
      return { ...state, request: false };
    case POST_PHONE_FAILED:
      return { ...state, request: false };
    case RESEND_PHONE_NUMBER_SUCCESS:
      return { ...state, request: false };
    case RESEND_PHONE_NUMBER_FAILED:
      return {
        ...state,
        request: false,
        error: {
          code: action.payload["code"],
          message: action.payload["message"]
        }
      };
    case POST_SMS_CODE_SUCCESS:
      return { ...state, request: false };
    case POST_SMS_CODE_FAILED:
      return {
        ...state,
        request: false,
        error: {
          code: action.payload["code"],
          message: action.payload["message"]
        }
      };
    case GET_PHONE_INFO_FAILED:
      return { ...state, phoneInfo: null, request: false };
    case GET_PHONE_INFO_SUCCESS:
      return { ...state, phoneInfo: action.payload, request: false };
    case FETCH_IN_PROGRESS:
      return { ...state, request: true };
    default:
      return state;
  }
};

export default phone;
