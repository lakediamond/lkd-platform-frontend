import { Request, startAuth } from "services";
import {
  GET_USER_PROFILE_DATA,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_IN_PROGRESS,
  LOGIN_USER_FAILED,
  LOGOUT_USER,
  LOGOUT_USER_SUCCESS,
  LOGOUT_USER_FAILED,
  POST_CONSENT,
  POST_CONSENT_IN_PROGRESS,
  POST_CONSENT_SUCCESS,
  POST_CONSENT_FAILED,
  CONSENT_ERROR_MESSAGE,
  ENABLE_2FA,
  GET_2FA_CODE,
  GET_2FA_CODE_SUCCESS,
  GET_2FA_CODE_FAILED,
  ENABLE_2FA_SUCCESS,
  ENABLE_2FA_FAILED,
  DISABLE_2FA_SUCCESS,
  DISABLE_2FA_FAILED,
  GET_2FA_ERROR_MESSAGE,
  DISABLE_2FA_ERROR_MESSAGE,
  ENABLE_2FA_ERROR_MESSAGE,
  DISABLE_2FA,
  POST_INVESTMENT_PLAN,
  POST_INVESTMENT_PLAN_FAILED,
  POST_INVESTMENT_PLAN_SUCCESS,
  POST_PROVIDER,
  POST_PROVIDER_SUCCESS,
  POST_PROVIDER_FAILED,
  USER_ACHIEVED_KYC
} from "../constants";

import { message } from "antd";

import { fetchInProgress, fetchIsFinished } from "./index";

const loginUserSuccess = payload => {
  return { type: LOGIN_USER_SUCCESS, payload };
};

const loginUserInProgress = () => {
  return { type: LOGIN_USER_IN_PROGRESS };
};

const loginUserFailed = () => {
  return { type: LOGIN_USER_FAILED };
};

const logoutUserSuccess = () => {
  return { type: LOGOUT_USER_SUCCESS };
};

const logoutUserFailed = () => {
  return { type: LOGOUT_USER_FAILED };
};

const investmentPlanPostFailed = () => {
  return { type: POST_INVESTMENT_PLAN_FAILED, };
};

const investmentPlanPostSuccess = (payload) => {
  return { type: POST_INVESTMENT_PLAN_SUCCESS, payload };
};

const postConsentInProgress = () => {
  return { type: POST_CONSENT_IN_PROGRESS };
};

const postConsentFailed = () => {
  return { type: POST_CONSENT_FAILED };
};

const postConsentSuccess = () => {
  return { type: POST_CONSENT_SUCCESS };
};

const enable2FAFailed = () => {
  return { type: ENABLE_2FA_FAILED };
};

const enable2FASuccess = () => {
  return { type: ENABLE_2FA_SUCCESS };
};

const disable2FAFailed = () => {
  return { type: DISABLE_2FA_FAILED };
};

const disable2FASuccess = () => {
  return { type: DISABLE_2FA_SUCCESS };
};

const get2FACodeFailed = () => {
  return { type: GET_2FA_CODE_FAILED };
};

const get2FACodeSuccess = payload => {
  return { type: GET_2FA_CODE_SUCCESS, payload };
};

const setProviderSuccess = payload => {
  return { type: POST_PROVIDER_SUCCESS, payload };
};

const setProviderFailed = () => {
  return { type: POST_PROVIDER_FAILED }
};





export const signInUser = () => {
  return async dispatch => {
    dispatch(loginUserInProgress());
    const data = await Request(GET_USER_PROFILE_DATA);
    if (!data) {
      dispatch(loginUserFailed());
      startAuth();
      return false;
    }
    if (data.status === 200) {
      data.json().then(res => {
        return dispatch(loginUserSuccess(res));
      });
      return true;
    }

    dispatch(loginUserFailed());
    startAuth();
  };
};

export const logoutUser = () => {
  return async dispatch => {
    const data = await Request(LOGOUT_USER);
    if (!data) {
      dispatch(logoutUserFailed());
      return false;
    }
    if (data.status === 200) {
      dispatch(logoutUserSuccess());
      window.location.reload();
      return true;
    }

    dispatch(logoutUserFailed());
    return false;
  };
};

export const setInvestmentPlan = data => {
  // const code = data.split("|")[0];
  // const description = data.split("|")[1];
  const body = { "investment_plan": Number(data) };
  return async dispatch => {
    dispatch(fetchInProgress("set_investment_plan"));
    const investmentPlan = await Request(POST_INVESTMENT_PLAN, body);
    
    if(!investmentPlan){
      dispatch(investmentPlanPostFailed());
      dispatch(fetchIsFinished());
      return false;
    }

    if(investmentPlan.status === 200){
      // const payload = { code: code, description: description };
      dispatch(investmentPlanPostSuccess(data));
      dispatch(fetchIsFinished());
      return true;
    }
    
    
    dispatch(investmentPlanPostFailed());
    dispatch(fetchIsFinished());
  };
  
};

export const confirmConsent = data => {
  return async dispatch => {
    dispatch(postConsentInProgress());
    const consent = await Request(POST_CONSENT, data);
    if (!consent) {
      dispatch(postConsentFailed());
      message.error(CONSENT_ERROR_MESSAGE);
      return false;
    }
    if (consent.status === 200) {
      dispatch(postConsentSuccess());
      const headers = consent.headers;
      const location = headers.get("Location");
      window.location.href = location;
      return true;
    }

    dispatch(postConsentFailed());
    message.error(CONSENT_ERROR_MESSAGE);
  };
};

export const get2FAQRCode = () => {
  return async dispatch => {
    dispatch(fetchInProgress("get_2FA_code"));
    const code = await Request(GET_2FA_CODE);
    if (!code) {
      dispatch(get2FACodeFailed());
      message.error(GET_2FA_ERROR_MESSAGE);
      return false;
    }
    if (code.status === 200) {
      code.json().then(payload => {
        dispatch(get2FACodeSuccess(payload));
      });
      return true;
      dispatch(fetchIsFinished());
    }
    dispatch(postConsentFailed());
    dispatch(fetchIsFinished());
    message.error(CONSENT_ERROR_MESSAGE);
  };
};

export const enable2FA = value => {
  return async dispatch => {
    dispatch(fetchInProgress("enable_2FA"));
    const code = await Request(ENABLE_2FA, value);
    if (!code) {
      dispatch(enable2FAFailed());
      dispatch(fetchIsFinished());
      message.error(ENABLE_2FA_ERROR_MESSAGE);
      return false;
    }

    if (code.status === 200) {
      dispatch(enable2FASuccess());
      dispatch(fetchIsFinished());
      // window.location.reload();
      return true;
    }
  
    if(code.status !== 200){
      code.json().then(res => {
        message.error(res.error_message.map(item => item).join())
      });
    }

    dispatch(enable2FAFailed());
    dispatch(fetchIsFinished());
    // message.error(ENABLE_2FA_ERROR_MESSAGE);
  };
};

export const disable2FA = value => {
  return async dispatch => {
    dispatch(fetchInProgress("disable_2FA"));
    const code = await Request(DISABLE_2FA, value);
    if (!code) {
      dispatch(disable2FAFailed());
      dispatch(fetchIsFinished());
      message.error(DISABLE_2FA_ERROR_MESSAGE);
      return false;
    }

    if (code.status === 200) {
      dispatch(disable2FASuccess());
      dispatch(fetchIsFinished());
      return true;
    }

    dispatch(disable2FAFailed());
    dispatch(fetchIsFinished());
    message.error(DISABLE_2FA_ERROR_MESSAGE);
  };
};


export const setBlockchainProvider = value => {
  return async dispatch => {
    dispatch(fetchInProgress("set_blockchain_provider"));
    const data = await Request(POST_PROVIDER, value);
    if (!data) {
      dispatch(setProviderFailed());
      dispatch(fetchIsFinished());
      message.error("Network error");
      return false;
    }
    
    if (data.status === 200) {
      dispatch(setProviderSuccess(value["blockchain_connection_type"]));
      dispatch(fetchIsFinished());
      return true;
    }
  
    if(data.status !== 200){
      data.json().then(res => {
        message.error(res.error_message.map(item => item).join())
      });
    }
  
    dispatch(setProviderFailed());
    dispatch(fetchIsFinished());
    
  };
}

export const userAchievedKyc = () => {
  return { type: USER_ACHIEVED_KYC };
}
