import { Request } from "services";
import { message } from "antd";
import {
  GET_COMMON_INFO,
  POST_COMMON_INFO,
  POST_COMMON_INFO_SUCCESS,
  POST_COMMON_INFO_FAILED,
  CREATED_SUCCESS_MESSAGE,
  GET_TIERS_INFO
} from "../constants";

import { get, fetchInProgress, fetchIsFinished } from "./index";

const commonInfoPostFailed = (payload) => {
  return { type: POST_COMMON_INFO_FAILED, payload };
};

const commonInfoPostSuccess = () => {
  return { type: POST_COMMON_INFO_SUCCESS };
};

export const postCommonInfo = value => {
  return async dispatch => {
    dispatch(fetchInProgress("common_info"));
    const info = await Request(POST_COMMON_INFO, value);
    if (!info) {
      dispatch(fetchIsFinished());
      dispatch(commonInfoPostFailed());
      return false;
    }

    if (info.status === 200) {
      dispatch(fetchIsFinished());
      dispatch(commonInfoPostSuccess());
      dispatch(get(GET_COMMON_INFO));
      dispatch(get(GET_TIERS_INFO));
      message.success(CREATED_SUCCESS_MESSAGE);
      return true;
    }
  
    if(info.status !== 200){
      info.json().then(res => {
          const payload = { code: info.status, message: res.error_message.map(item => item).join() };
          message.error(res.error_message.map(item => item).join());
        dispatch(commonInfoPostFailed(payload));
        }
      )
    }

    dispatch(fetchIsFinished());
    
  };
};
