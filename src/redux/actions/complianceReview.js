// import { Request } from "services";
// import { message } from "antd";
// import {
//   GET_COMPLIANCE_REVIEW,
//   GET_COMPLIANCE_REVIEW_SUCCESS,
//   GET_COMPLIANCE_REVIEW_FAILED,
//   POST_COMPLIANCE_REVIEW,
//   POST_COMPLIANCE_REVIEW_SUCCESS,
//   POST_COMPLIANCE_REVIEW_FAILED,
//   CREATED_SUCCESS_MESSAGE
// } from "../constants";
//
// import { get, fetchInProgress, fetchIsFinished } from "./index";
//
// const postComplianceReviewFailed = () => {
//   return { type: POST_COMPLIANCE_REVIEW_FAILED };
// };
//
// const postComplianceReviewSuccess = () => {
//   return { type: POST_COMPLIANCE_REVIEW_SUCCESS };
// };
//
// const getComplianceReviewFailed = () => {
//   return { type: GET_COMPLIANCE_REVIEW_FAILED };
// };
//
// const getComplianceReviewSuccess = payload => {
//   return { type: GET_COMPLIANCE_REVIEW_SUCCESS, payload };
// };
//
// export const postComplianceReview = value => {
//   return async dispatch => {
//     dispatch(fetchInProgress("compliance_review"));
//     const review = await Request(POST_COMPLIANCE_REVIEW, value);
//
//     dispatch(fetchIsFinished());
//
//     if (!review) {
//       dispatch(postComplianceReviewFailed());
//       return false;
//     }
//
//     if (review.status === 200) {
//       dispatch(postComplianceReviewSuccess());
//       dispatch(getComplianceReview());
//       dispatch(get("GET_TIERS_INFO"));
//       message.success(CREATED_SUCCESS_MESSAGE);
//       return true;
//     }
//
//     dispatch(postComplianceReviewFailed());
//   };
// };
//
// export const getComplianceReview = () => {
//   return async dispatch => {
//     const data = await Request(GET_COMPLIANCE_REVIEW);
//     if (!data) {
//       dispatch(getComplianceReviewFailed());
//       return false;
//     }
//
//     if (data.status === 200) {
//       data
//         .json()
//         .then(payload => dispatch(getComplianceReviewSuccess(payload)));
//       return true;
//     }
//     dispatch(getComplianceReviewFailed());
//   };
// };
