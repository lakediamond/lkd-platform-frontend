const closeAlert = () => {
  return { type: "CLOSE_ALERT" };
};

const showAlert = message => {
  return dispatch => {
    dispatch({ type: "SHOW_ALERT", message });
  };
};

export { closeAlert, showAlert };
