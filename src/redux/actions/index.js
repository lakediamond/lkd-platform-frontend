import {
  postPhoneNumber,
  postCode,
  // getPhoneInfo,
  resendPhoneNumber,
  deletePhoneNumber
} from "./phone";
import { postCommonInfo } from "./commonInfo";
import { closeAlert, showAlert } from "./alert";
import {
  signInUser,
  setInvestmentPlan,
  logoutUser,
  confirmConsent,
  get2FAQRCode,
  enable2FA,
  disable2FA,
  setBlockchainProvider,
  userAchievedKyc
} from "./user";
import { get, webSocketData, user_token_expired } from "./get";
import {
  isMetamaskInstalled,
  enableMetamask,
  getETHBalance,
  getTokenBalance,
  createContractInstance,
  checkAllowance,
  isMetamaskLoggedIn,
  checkContractOwner,
  openSetupProvider,
  closeSetupProvider,
  openSelectProvider,
  closeSelectProvider,
  setEstimationEthereumPrice,
  resetEstimationEthereumPrice,
  getAllowedTokensAmount
} from "./blockchain";

// import { postInvestmentPlan } from "./kyc";
import { fetchInProgress, fetchIsFinished } from "./fetch";
import { getComplianceReview, postComplianceReview } from "./complianceReview";
import { checkChallenge, restorePassword, sendEmailForRestore, changePassword } from "./auth";
import { updateIdentityScansStatus } from "./identityScans"

export {
  closeAlert,
  showAlert,
  signInUser,
  logoutUser,
  confirmConsent,
  isMetamaskInstalled,
  isMetamaskLoggedIn,
  enableMetamask,
  getETHBalance,
  getTokenBalance,
  createContractInstance,
  checkContractOwner,
  checkAllowance,
  postPhoneNumber,
  resendPhoneNumber,
  postCode,
  setBlockchainProvider,
  postCommonInfo,
  setInvestmentPlan,
  fetchInProgress,
  fetchIsFinished,
  getComplianceReview,
  postComplianceReview,
  get2FAQRCode,
  enable2FA,
  disable2FA,
  checkChallenge,
  restorePassword,
  changePassword,
  sendEmailForRestore,
  get,
  user_token_expired,
  webSocketData,
  updateIdentityScansStatus,
  deletePhoneNumber,
  openSetupProvider,
  closeSetupProvider,
  openSelectProvider,
  closeSelectProvider,
  setEstimationEthereumPrice,
  resetEstimationEthereumPrice,
  getAllowedTokensAmount,
  userAchievedKyc
};
