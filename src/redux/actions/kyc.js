// import {
//   GET_TIERS_INFO_SUCCESS,
//   GET_TIERS_INFO_FAILED,
//   GET_TIERS_INFO,
//   POST_INVESTMENT_PLAN,
//   POST_INVESTMENT_PLAN_FAILED,
//   POST_INVESTMENT_PLAN_SUCCESS
// } from "../constants";
//
// import { Request } from "services";
//
// // const investmentPlanPostFailed = () => {
// //   return { type: POST_INVESTMENT_PLAN_FAILED, };
// // };
// //
// // const investmentPlanPostSuccess = () => {
// //   return { type: POST_INVESTMENT_PLAN_SUCCESS };
// // };
//
// export const postInvestmentPlan = (value) => {
//   return async dispatch => {
//     const investmentPlan = await Request(POST_INVESTMENT_PLAN, value);
//
//     if(!investmentPlan){
//       dispatch(investmentPlanPostFailed());
//       return false;
//     }
//
//     if(investmentPlan.status === 200){
//       dispatch(investmentPlanPostSuccess());
//     }
//
//     dispatch(investmentPlanPostFailed());
//   };
// };
