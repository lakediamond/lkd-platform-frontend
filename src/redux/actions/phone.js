import { Request } from "services";
import { message } from "antd";
import {
  POST_PHONE_NUMBER,
  DELETE_PHONE_NUMBER,
  DELETE_PHONE_NUMBER_SUCCESS,
  DELETE_PHONE_NUMBER_FAILED,
  POST_SMS_CODE,
  POST_PHONE_SUCCESS,
  POST_PHONE_FAILED,
  POST_SMS_CODE_SUCCESS,
  POST_SMS_CODE_FAILED,
  GET_PHONE_INFO,
  GET_PHONE_INFO_FAILED,
  GET_PHONE_INFO_SUCCESS,
  PHONE_CONFIRM_ALREADY_RECIEVED_MESSAGE,
  PHONE_VERIFIED_SUCCESS_MESSAGE,
  RESEND_PHONE_NUMBER,
  RESEND_PHONE_NUMBER_FAILED,
  RESEND_PHONE_NUMBER_SUCCESS,
} from "../constants";

import { get, fetchInProgress, fetchIsFinished } from "./index";

const phoneNumberPostFailed = () => {
  return { type: POST_PHONE_FAILED };
};

const phoneNumberPostSuccess = () => {
  return { type: POST_PHONE_SUCCESS };
};

const phoneNumberResendFailed = (payload) => {
  return { type: RESEND_PHONE_NUMBER_FAILED, payload };
};

const phoneNumberResendSuccess = () => {
  return { type: RESEND_PHONE_NUMBER_SUCCESS };
};

const smsCodePostFailed = (payload) => {
  return { type: POST_SMS_CODE_FAILED, payload };
};

const smsCodePostSuccess = () => {
  return { type: POST_SMS_CODE_SUCCESS };
};

const getPhoneInfoFailed = () => {
  return { type: GET_PHONE_INFO_FAILED };
};

const getPhoneInfoSuccess = (payload) => {
  return { type: GET_PHONE_INFO_SUCCESS, payload };
};

const deletePhoneNumberSuccess = () => {
  return { type: DELETE_PHONE_NUMBER_SUCCESS }
};

const deletePhoneNumberFailed = () => {
  return { type: DELETE_PHONE_NUMBER_FAILED }
};

export const postPhoneNumber = (value) => {
  return async dispatch => {
    dispatch(fetchInProgress("phone_number"));
    const phone = await Request(POST_PHONE_NUMBER, value);
    
    if(!phone){
      dispatch(fetchIsFinished());
      dispatch(phoneNumberPostFailed());
      return false;
    }
    
    if(phone.status === 201){
      dispatch(fetchIsFinished());
      dispatch(phoneNumberPostSuccess());
      dispatch(get("GET_PHONE_INFO"));
      // dispatch(getPhoneInfo());
      return true;
    }
  
    if(phone.status !== 200 || phone.status !== 201){
      dispatch(fetchIsFinished());
      dispatch(phoneNumberPostFailed());
      phone.json().then(res => {
          const payload = { code: phone.status, message: res.error_message.map(item => item).join() };
          message.error(res.error_message.map(item => item).join());
          dispatch(phoneNumberResendFailed(payload));
        }
      );
      // message.error(PHONE_CONFIRM_ALREADY_RECIEVED_MESSAGE);
      return true;
    }
    
    dispatch(fetchIsFinished());
    dispatch(phoneNumberPostFailed());
  };
};

export const resendPhoneNumber = (value) => {
  return async dispatch => {
    dispatch(fetchInProgress("phone_number"));
    const phone = await Request(RESEND_PHONE_NUMBER, value);
    if(!phone){
      dispatch(fetchIsFinished());
      dispatch(phoneNumberResendFailed());
      return false;
    }
    
    if(phone.status === 200){
      dispatch(fetchIsFinished());
      dispatch(phoneNumberResendSuccess());
      // dispatch(getPhoneInfo());
      dispatch(get("GET_PHONE_INFO"));
      return true;
    }
  
    if(phone.status !== 200){
      phone.json().then(res => {
          const payload = { code: phone.status, message: res.error_message.map(item => item).join() };
        message.error(res.error_message.map(item => item).join());
        dispatch(phoneNumberResendFailed(payload));
        }
      )
    }
    
    dispatch(fetchIsFinished());
  };
};

export const postCode = (value) => {
  return async dispatch => {
    dispatch(fetchInProgress("sms_code"));
    const code = await Request(POST_SMS_CODE, value);
    
    if(!code){
      dispatch(fetchIsFinished());
      dispatch(smsCodePostFailed());
      return false;
    }
    
    if(code.status === 200){
      dispatch(fetchIsFinished());
      dispatch(smsCodePostSuccess());
      // dispatch(getPhoneInfo());
      dispatch(get("GET_PHONE_INFO"));
      dispatch(get("GET_TIERS_INFO"));
      message.success(PHONE_VERIFIED_SUCCESS_MESSAGE);
      return true;
    }
    
    if(code.status !== 200){
      code.json().then(res => {
        const payload = { code: code.status, message: res.error_message.map(item => item).join() };
        message.error(res.error_message.map(item => item).join());
        dispatch(smsCodePostFailed(payload));
        }
      )
    }
    dispatch(fetchIsFinished());
    
  };
};

// export const getPhoneInfo = () => {
//   return async dispatch => {
//     const data = await Request(GET_PHONE_INFO);
//     if(!data){
//       dispatch(getPhoneInfoFailed());
//       return false;
//     }
//
//     if(data.status === 200){
//       data.json().then(payload => dispatch(getPhoneInfoSuccess(payload)));
//       return true;
//     }
//     dispatch(getPhoneInfoFailed());
//   };
// };


export const deletePhoneNumber = () => {
  return async dispatch => {
    const data = await Request(DELETE_PHONE_NUMBER);
    if(!data){
      dispatch(deletePhoneNumberFailed());
      return false;
    }
    
    if(data.status === 200){
      dispatch(deletePhoneNumberSuccess());
      dispatch(get("GET_PHONE_INFO"));
      // dispatch(getPhoneInfo());
      return true;
    }
    
    dispatch(deletePhoneNumberFailed());
  };
}

