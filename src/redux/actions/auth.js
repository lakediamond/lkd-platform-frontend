import { Request, startAuth } from "services";
import {
  RESTORE_PASSWORD_SUCCESS,
  RESTORE_PASSWORD_FAILED,
  CHECK_CHALLENGE_SUCCESS,
  CHECK_CHALLENGE_FAILED,
  RESTORE_PASSWORD,
  CHECK_CHALLENGE,
  POST_EMAIL_FOR_RESTORE,
  SEND_EMAIL_FOR_RESTORE_SUCCESS,
  SEND_EMAIL_FOR_RESTORE_FAILED,
  CHANGE_PASSWORD,
  CHANGE_PASSWORD_SUCCESS,
  CHANGE_PASSWORD_FAILED
} from "../constants";

import { message } from "antd";

import { fetchInProgress, fetchIsFinished } from "./index";

export const sendEmailForRestoreSuccess = () => {
  return { type: SEND_EMAIL_FOR_RESTORE_SUCCESS };
};

export const sendEmailForRestoreFailed = () => {
  return { type: SEND_EMAIL_FOR_RESTORE_FAILED };
};

export const checkChallengeSuccess = () => {
  return { type: CHECK_CHALLENGE_SUCCESS };
};

export const checkChallengeFailed = () => {
  return { type: CHECK_CHALLENGE_FAILED };
};

export const restorePasswordSuccess = () => {
  return { type: RESTORE_PASSWORD_SUCCESS };
};

export const restorePasswordFailed = () => {
  return { type: RESTORE_PASSWORD_FAILED }
};

export const changePasswordSuccess = () => {
  return { type: CHANGE_PASSWORD_SUCCESS };
};

export const changePasswordFailed = () => {
  return { type: CHANGE_PASSWORD_FAILED }
};

export const sendEmailForRestore = (value) => {
  return async dispatch => {
    dispatch(fetchInProgress("send_email_for_restore"));
    const email = await Request(POST_EMAIL_FOR_RESTORE, value);
    
    if (!email){
      dispatch(sendEmailForRestoreFailed())
    }
    
    if (email.status === 200) {
      dispatch(sendEmailForRestoreSuccess());
    }
    
    if(email.status !== 200){
      email.json().then(res => {
        message.error(res.error_message.map(item => item).join())
      });
    }
  
    dispatch(fetchIsFinished());
  }
};

export const checkChallenge = (value) => {
  return async dispatch => {
    dispatch(fetchInProgress("check_challenge"));
    
    if(!!!value) {
      dispatch(checkChallengeFailed());
      startAuth();
    }
    const challenge = await Request(CHECK_CHALLENGE, value);
    
    if (!challenge || challenge.status === 422){
      dispatch(checkChallengeFailed());
      startAuth();
    }
    
    if(challenge.status === 200){
      dispatch(checkChallengeSuccess())
    }
    
    dispatch(fetchIsFinished())
  }
};

export const restorePassword = (value) => {
  return async dispatch => {
    dispatch(fetchInProgress("restore_password"));
    const password = await Request(RESTORE_PASSWORD, value);
    if (!password){
      dispatch(restorePasswordFailed())
    }
    if (password.status === 200){
      dispatch(restorePasswordSuccess())
    }
    dispatch(fetchIsFinished())
  }
};

export const changePassword = (value) => {
  return async dispatch => {
    dispatch(fetchInProgress("change_password"));
    const password = await Request(CHANGE_PASSWORD, value);
    if (!password){
      message.error("Looks like it's a network error");
      dispatch(changePasswordFailed());
    }
    
    if (password.status !== 200){
      dispatch(changePasswordFailed());
      password.json().then(res => {
        message.error(res.error_message.map(item => item).join())
      });
    }
    
    if (password.status === 200){
      dispatch(changePasswordSuccess());
      message.success("Congratulations, you successfully changed your password");
    }
    
    dispatch(fetchIsFinished())
  }
};
