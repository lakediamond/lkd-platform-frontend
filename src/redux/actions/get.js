import { Request } from "services";
import {
  GET_ORDERS, USER_TOKEN_EXPIRED,
  // ORDERS_SOCKET,
  // WEBSOCKET_PATH,
  // PROPOSALS_USER_SOCKET,
  // PROPOSALS_ALL_SOCKET
} from "../constants";

import { store } from "index";
import { startAuth } from "services";

import proposalsList from "../reducers/proposalsList";

const get_success = (type, payload) => {
  return { type: type, payload };
};

const get_failed = (type, payload) => {
  return { type: type, payload };
};

export const user_token_expired = () => {
  return {type: USER_TOKEN_EXPIRED}
};

// const socket_closed = type => {
//   return { type: type };
// };
//
// const createSocket = (type, payload) => {
//   return { type: type, payload };
// };
//
// const socket_failed = (type, payload) => {
//   return { type: type, payload };
// };

export const get = (type, body = null, param = null) => {
  
  return async dispatch => {
    const data = await Request(type, body, param);
    if (!data) {
      const payload = { code: 503, message: "Service Unavailable" };
      dispatch(get_failed(`${type}_FAILED`, payload));
      return false;
    }

    if (data.status === 200) {
      data.json().then(payload => {
        dispatch(get_success(`${type}_SUCCESS`, payload));
      });
      return true;
    }

    if (data.status === 401) {
      data.json().then(response => {
        const payload = {
          code: data.status,
          message: response.error_message.map(item => item).join()
        };
        dispatch(get_failed(`${type}_FAILED`, payload));
        dispatch(user_token_expired());
        // startAuth();
      });
    }

    dispatch(get_failed(`${type}_FAILED`));
  };
};

// export const webSocketData = (type, action) => {
//
//   const getPath = type => {
//     switch (type) {
//       case ORDERS_SOCKET:
//         return "/admin/order";
//       case PROPOSALS_USER_SOCKET:
//         return "/user/proposal";
//       case PROPOSALS_ALL_SOCKET:
//         return "/user/proposal/all";
//       default:
//         return null;
//     }
//   };
//
//   return dispatch => {
//
//     dispatch(
//       createSocket(
//         `${type}_OPENED`,
//         new WebSocket(`${WEBSOCKET_PATH}${getPath(type)}`)
//       )
//     );
//
//     const getSocket = () => {
//       switch (type) {
//         case ORDERS_SOCKET:
//           return store.getState().ordersList.socket;
//         case PROPOSALS_ALL_SOCKET:
//           return store.getState().proposalsList.socketAll;
//         case PROPOSALS_USER_SOCKET:
//           return store.getState().proposalsList.socketUser;
//         default:
//           return null;
//       }
//     };
//
//     const socket = getSocket(type);
//
//     socket.onerror = function(error) {
//       console.log("-------error.message-------");
//       console.log(error.currentTarget.url);
//       console.log("-------error.message-------");
//       return dispatch(socket_failed(`${type}_CLOSED`, true));
//     };
//
//
//
//     if (action === "start") {
//       socket.onmessage = function(event) {
//         return dispatch(get_success(`${type}_SUCCESS`, JSON.parse(event.data)));
//       };
//     }
//
//
//
//     if (action === "finish") {
//       getSocket(type).close(1000);
//       return dispatch(socket_closed(`${type}_CLOSED`));
//     }
//
//   };
// };
