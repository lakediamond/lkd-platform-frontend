import {
  Dashboard,
  Orders,
  Proposals,
  Management,
  Consent,
  Auth,
  Final,
  Overview,
  Kyc,
  TwoFA,
  Recovery,
  Password
} from "views";

const appRoutes = [
  {
    path: "/dashboard",
    sidebarName: "Dashboard",
    navbarName: "Dashboard",
    icon: "dashboard",
    component: Dashboard
  },
  {
    path: "/orders",
    sidebarName: "Orders",
    navbarName: "Orders",
    icon: "line-chart",
    component: Orders
  },
  {
    path: "/proposals",
    sidebarName: "Proposals",
    navbarName: "Active Proposals",
    icon: "plus-square",
    component: Proposals
  },
  // {
  //   path: "/wallet",
  //   sidebarName: "Wallet",
  //   navbarName: "Wallet",
  //   icon: "wallet",
  //   component: Wallet
  // },
  {
    path: "/profile/overview",
    sidebarName: "Overview",
    navbarName: "Overview",
    icon: "setting",
    component: Overview
  },
  {
    path: "/profile/kyc",
    sidebarName: "KYC area",
    navbarName: "KYC area",
    icon: "setting",
    component: Kyc
  },
  {
    path: "/profile/kyc/tier/0",
    sidebarName: "KYC area",
    navbarName: "KYC area",
    icon: "setting",
    component: Kyc
  },
  // {
  //   path: "/profile/kyc/tier/1",
  //   sidebarName: "KYC area",
  //   navbarName: "KYC area",
  //   icon: "setting",
  //   component: Kyc
  // },
  // {
  //   path: "/profile/kyc/tier/2",
  //   sidebarName: "KYC area",
  //   navbarName: "KYC area",
  //   icon: "setting",
  //   component: Kyc
  // },
  // {
  //   path: "/profile/kyc/tier/3",
  //   sidebarName: "KYC area",
  //   navbarName: "KYC area",
  //   icon: "setting",
  //   component: Kyc
  // },
  {
    path: "/profile/security/password",
    sidebarName: "Password",
    navbarName: "Password",
    icon: "setting",
    component: Password
  },
  {
    path: "/profile/security/2FA",
    sidebarName: "2 factor authentication settings",
    navbarName: "2FA settings",
    icon: "setting",
    component: TwoFA
  },
  {
    path: "/management",
    sidebarName: "Management",
    navbarName: "Management",
    icon: "edit",
    component: Management
  },
  {
    path: "/consent",
    sidebarName: "Consent",
    navbarName: "Consent",
    icon: "setting",
    component: Consent
  },
  {
    path: "/auth",
    sidebarName: "auth",
    navbarName: "auth",
    icon: "",
    component: Auth
  },
  {
    path: "/",
    sidebarName: "final",
    navbarName: "final",
    icon: "",
    component: Final
  },
  {
    path: "/user/access_recovery_activation",
    sidebarName: " ",
    navbarName: " ",
    icon: "",
    component: Recovery
  },
  {
    redirect: true,
    path: "/profile",
    to: "/profile/overview",
    sidebarName: "Overview",
    navbarName: "Overview",
    icon: "setting",
    component: Overview
  }
];

export default appRoutes;
