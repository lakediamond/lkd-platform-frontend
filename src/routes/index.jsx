import App from "container/App.jsx";

const indexRoutes = [{ path: "/", component: App }];

export default indexRoutes;
